/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.GeneralTools.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Threading;

    using Codefarts.GeneralTools.Models;
    using Codefarts.GeneralTools.Utilities.Models;
#if !UNITY_WEBPLAYER
    using FileExtensionMethods = System.IO.File;
#endif
    /// <summary>
    /// Provides a class for generating texture atlases from a list of tiles.
    /// </summary>
    public class AtlasGenerator
    {
        #region Constants and Fields

        /// <summary>
        /// holds the size of the texture atlases to use.
        /// </summary>
        private readonly int size = 512;

        /// <summary>
        /// Holds a list of tile images to be processed.
        /// </summary>
        private readonly List<PixelsContainer> tileItems;

        /// <summary>
        /// Holds the current atlas index
        /// </summary>
        private int atlasIndex;

        /// <summary>
        /// Holds a reference to the current texture atlas being generated.
        /// </summary>
        private PixelsContainer atlasTex;

        /// <summary>
        /// Holds the current processing step.
        /// </summary>
        private int currentStep;

        /// <summary>
        /// Holds the template filename.
        /// </summary>
        private string fileName;

        /// <summary>
        /// Holds the current index to the tiles that is being processed.
        /// </summary>
        private int index;

        /// <summary>
        /// Used to determine if processing is currently running.
        /// </summary>
        private bool isRunning;

        /// <summary>
        /// Holds the position where the next tile will be placed in the current atlas.
        /// </summary>
        private Point2D pos;

        /// <summary>
        /// Holds the spacing value to use.
        /// </summary>
        private int spacing;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AtlasGenerator"/> class.
        /// </summary>
        /// <param name="size">
        /// The size of the atlas images.
        /// </param>
        /// <param name="spacing">
        /// The spacing between tiles.
        /// </param>
        /// <param name="fileName">
        /// The file name that will be used as a template. See Remarks.
        /// </param>
        /// <param name="sourceTileData">
        /// The source tile images.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If sourceTileData or filename is null or empty. 
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// If size is less than 256.
        /// </exception>
        /// <remarks>
        /// <para>
        /// The <see cref="FileName"/> argument is used as a template and the atlas index is appended to the end of it.
        /// </para>
        /// <para>
        /// "c:\testatlas.png" is saved as "c:\testatlas0.png" then "c:\testatlas1.png" etc.
        /// </para>
        /// </remarks>
        public AtlasGenerator(int size, int spacing, string fileName, IEnumerable<PixelsContainer> sourceTileData)
        {
            if (sourceTileData == null)
            {
                throw new ArgumentNullException("sourceTileData");
            }

            this.FileName = fileName.Trim();
            if (string.IsNullOrEmpty(this.FileName))
            {
                throw new ArgumentNullException("fileName");
            }

            if (size < 256)
            {
                throw new ArgumentOutOfRangeException("size");
            }

            this.size = size;
            this.tileItems = new List<PixelsContainer>();
            foreach (var item in sourceTileData)
            {
                this.tileItems.Add(item);
            }

            this.Spacing = spacing;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the current atlas count.
        /// </summary>
        public int AtlasCount
        {
            get
            {
                return this.atlasIndex + 1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether processing has completed.
        /// </summary>
        public bool Complete
        {
            get
            {
                return this.currentStep == 2;
            }
        }

        /// <summary>
        /// Gets or sets the filename template.
        /// </summary>
        public string FileName
        {
            get
            {
                return this.fileName;
            }

            set
            {
                if (this.isRunning)
                {
                    return;
                }

                this.fileName = value;
            }
        }

        /// <summary>
        /// Gets the completion percentage from 0 to 1.
        /// </summary>
        public float PercentageCompleted
        {
            get
            {
                return this.index / (float)this.tileItems.Count;
            }
        }

        /// <summary>
        /// Gets the total number of tiles that have been processed.
        /// </summary>
        public int ProcessedTiles
        {
            get
            {
                return this.index;
            }
        }

        /// <summary>
        /// Gets or sets the spacing between tiles.
        /// </summary>
        public int Spacing
        {
            get
            {
                return this.spacing;
            }

            set
            {
                if (this.isRunning)
                {
                    return;
                }

                this.spacing = value;
            }
        }

        /// <summary>
        /// Gets the total number of tiles that are to be processed.
        /// </summary>
        public int TileCount
        {
            get
            {
                return this.tileItems.Count;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Generates the texture atlas.
        /// </summary>
        /// <remarks>
        /// Processing takes place on another thread.
        /// </remarks>
        public void Run()
        {
            if (this.isRunning)
            {
                return;
            }

            this.isRunning = true;
            ThreadPool.QueueUserWorkItem(
                cb =>
                {
                    this.atlasTex = new PixelsContainer() { Width = this.size, Height = this.size };
                    this.pos = new Point2D(1 * this.Spacing, 1 * this.Spacing);
                    this.currentStep = 1;

                    foreach (var entry in this.tileItems)
                    {
                        // this.atlasTex.Draw(entry, this.pos.X, this.pos.Y);
                        this.TextureBlt(entry, this.atlasTex, this.pos.X, this.pos.Y);

                        // move to next tile pos
                        this.pos.X += entry.Width + this.Spacing;
                        if (this.pos.X + entry.Width > this.atlasTex.Width - 1)
                        {
                            this.pos.X = this.Spacing;
                            this.pos.Y += entry.Height + this.Spacing;

                            // detect if we have moved passed bottom of bitmap
                            if (this.pos.Y + entry.Height > this.atlasTex.Height - 1)
                            {
                                // save current atlas and start a new one
                                this.SaveOutTextureAtlas();

                                // start a new texture atlas bitmap
                                this.atlasTex = new PixelsContainer() { Width = this.size, Height = this.size };

                                this.pos = new Point2D(1 * this.Spacing, 1 * this.Spacing);
                            }
                        }

                        this.index++;
                    }

                    this.SaveOutTextureAtlas();

                    this.currentStep++;
                    this.isRunning = false;
                });
        }

        private void TextureBlt(PixelsContainer source, PixelsContainer destination, int x, int y)
        {
            for (var idy = 0; idy < source.Height; idy++)
            {
                for (var idx = 0; idx < source.Width; idx++)
                {
                    var positionY = y + idy;
                    var positionX = x + idx;
                    if (positionX < 0 || positionY < 0 || positionX > destination.Width - 1 || positionY > destination.Height - 1)
                    {
                        continue;
                    }
                    
                    var sourceColor = source.colors[(idy * source.Width) + idx];
                    destination.colors[(positionY * destination.Width) + positionX] = sourceColor;
                }
            }
        }

        #endregion

        #region Methods

        private void SaveToPPM(PixelsContainer bitmap, Stream stream)
        {
            // Use a streamwriter to write the text part of the encoding
            var writer = new StreamWriter(stream);
            writer.Write("P6" + " ");
            writer.Write(bitmap.Width + " " + bitmap.Height + " ");
            writer.Write("255" + " ");
            writer.Flush();

            // Switch to a binary writer to write the data
            var writerB = new BinaryWriter(stream);
            writerB.Seek(0, SeekOrigin.End);
            for (var x = 0; x < bitmap.Height; x++)
            {
                for (var y = 0; y < bitmap.Width; y++)
                {
                    var color = bitmap.colors[(y * bitmap.Width) + x];
                    writerB.Write(color.r);
                    writerB.Write(color.g);
                    writerB.Write(color.b);
                }
            }

            writerB.Flush();
        }

        /// <summary>
        /// Save the current texture atlas and increments the atlas count.
        /// </summary>
        private void SaveOutTextureAtlas()
        {
            var dirName = Path.GetDirectoryName(this.FileName);
            if (dirName == null)
            {
                throw new NullReferenceException("There is not directory specified in the FileName property.");
            }

            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }

            var file = Path.GetFileNameWithoutExtension(this.FileName);
            var outputName = Path.Combine(dirName, file + (this.atlasIndex == 0 ? string.Empty : this.atlasIndex.ToString(CultureInfo.InvariantCulture)) + ".png");

            using (var stream = new FileStream(outputName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                this.SaveToPPM(this.atlasTex, stream);
            }

            /*
            var data = new int[this.atlasTex.Width * this.atlasTex.Height];
            var index = 0;
            for (var idy = 0; idy < this.atlasTex.Height; idy++)
            {
                for (var idx = 0; idx < this.atlasTex.Width; idx++)
                {
                    var color = 0;

                    color = color | this.atlasTex[idx, idy].A << 24; // A
                    color = color | this.atlasTex[idx, idy].R << 16; // R
                    color = color | this.atlasTex[idx, idy].G << 8;  // G
                    color = color | this.atlasTex[idx, idy].B;       // B
                    data[index++] = color;
                }
            }

            var png = new Plupload.PngEncoder.PngEncoder(data, this.atlasTex.Width, this.atlasTex.Height, true, Plupload.PngEncoder.PngEncoder.FILTER_NONE, Plupload.PngEncoder.Deflater.BEST_COMPRESSION);
            var pngBytes = png.pngEncode();


            FileExtensionMethods.WriteAllBytes(outputName, pngBytes);
                       */
            //var png = new PngEncoder(this.atlasTex.Width, this.atlasTex.Height);
            //for (int idy = 0; idy < this.atlasTex.Height; idy++)
            //{
            //    for (int idx = 0; idx < this.atlasTex.Width; idx++)
            //    {
            //        Color col = this.atlasTex[idx, idy];
            //        png.SetPixel(idx, idy, col.R, col.G, col.B, col.A);
            //    }
            //}

            //png.Save(outputName);

            this.atlasIndex++;
        }

        #endregion
    }
}