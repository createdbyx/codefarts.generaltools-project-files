// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CaveMapGenerator.cs" company="Codefarts">
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
// This code is a modified version based on the work of Sebastian Lague via his youtube channel
// Youtube: https://www.youtube.com/watch?v=v7yyZZjF1z4&list=PLQBrCLoduX3_wq4orgVAI4ytLqyrug9Q3

namespace Codefarts.GeneralTools.Scripts
{
    using System;

    using UnityEngine;

    using Random = System.Random;

    /// <summary>
    /// The cave map generator.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public class CaveMapGenerator<T>
    {
        #region Fields

        /// <summary>
        ///     The height of the map.
        /// </summary>
        protected int height = 64;

        /// <summary>
        ///     Holds the map data.
        /// </summary>
        protected T[,] map;

        /// <summary>
        ///     The seed value for the map.
        /// </summary>
        protected string seed = string.Empty;

        /// <summary>
        ///     The number of smoothing passes to be applied.
        /// </summary>
        protected int smoothingPasses = 5;

        /// <summary>
        ///     The width of the map.
        /// </summary>
        protected int width = 128;

        /// <summary>
        ///     The random fill percent value to control the map fill density.
        /// </summary>
        private int randomFillPercent = 45;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CaveMapGenerator{T}"/> class. 
        /// </summary>
        /// <param name="width">
        /// The width of the map.
        /// </param>
        /// <param name="height">
        /// The height of the map.
        /// </param>
        public CaveMapGenerator(int width, int height)
            : this()
        {
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CaveMapGenerator{T}"/> class. 
        /// </summary>
        public CaveMapGenerator()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether the map will have a solid value around the edges of the map.
        /// </summary>
        public virtual bool EnsureSolidEdges { get; set; }

        /// <summary>
        ///     Gets or sets a callback function for a empty map value.
        /// </summary>
        public Func<T> GetEmptyValue { get; set; }

        /// <summary>
        ///     Gets or sets a callback function for a solid map value.
        /// </summary>
        public Func<T> GetSolidValue { get; set; }

        /// <summary>
        ///     Gets or sets the map height.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException" accessor="set">value</exception>
        /// <remarks>The default value is 64.</remarks>
        public virtual int Height
        {
            get
            {
                return this.height;
            }

            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value", "Must be equal to or greater then 1.");
                }

                this.height = value;
            }
        }

        /// <summary>
        ///     Gets or sets a callback function used to determine if a value is solid [true] or empty [false.
        /// </summary>
        public Func<T, bool> IsSolidValue { get; set; }

        /// <summary>
        ///     Gets or sets the map.
        /// </summary>
        public virtual T[,] Map
        {
            get
            {
                return this.map;
            }

            set
            {
                this.map = value;
            }
        }

        /// <summary>
        ///     The random fill percent used to control the map fill density.
        /// </summary>
        /// <remarks>The default value is 45.</remarks>
        public virtual int RandomFillPercent
        {
            get
            {
                return this.randomFillPercent;
            }

            set
            {
                this.randomFillPercent = value;
            }
        }

        /// <summary>
        ///     Gets or sets the seed value used to generate the map.
        /// </summary>
        /// <remarks>The default value is <see cref="string.Empty" />.</remarks>
        public virtual string Seed
        {
            get
            {
                return this.seed;
            }

            set
            {
                this.seed = value;
            }
        }

        /// <summary>
        ///     Gets or sets the number of smoothing passes to apply to the map.
        /// </summary>
        /// <remarks>The default value is 5.</remarks>
        public virtual int SmoothingPasses
        {
            get
            {
                return this.smoothingPasses;
            }

            set
            {
                this.smoothingPasses = value;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether to use a random seed.
        /// </summary>
        /// <remarks>The default value is false.</remarks>
        public virtual bool UseRandomSeed { get; set; }

        /// <summary>
        ///     Gets or sets the map width.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException" accessor="set">value</exception>
        /// <remarks>The default value is 128.</remarks>
        public virtual int Width
        {
            get
            {
                return this.width;
            }

            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value", "Must be equal to or greater then 1.");
                }

                this.width = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Generates the map.
        /// </summary>
        public virtual void GenerateMap()
        {
            this.map = new T[this.width, this.height];
            this.RandomFillMap();

            for (int i = 0; i < this.smoothingPasses; i++)
            {
                this.SmoothMap();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Fills map with random values.
        /// </summary>
        protected virtual void RandomFillMap()
        {
            // if we are using a random seed set a randome seed value
            if (this.UseRandomSeed)
            {
                this.seed = Time.time.ToString();
            }

            // get a random number generator
            var pseudoRandom = new Random(this.seed.GetHashCode());

            // build a random map 
            for (int x = 0; x < this.width; x++)
            {
                for (int y = 0; y < this.height; y++)
                {
                    if (this.EnsureSolidEdges && (x == 0 || x == this.width - 1 || y == 0 || y == this.height - 1))
                    {
                        this.Map[x, y] = this.GetSolidValue();
                    }
                    else
                    {
                        this.Map[x, y] = (pseudoRandom.Next(0, 100) < this.randomFillPercent) ? this.GetSolidValue() : this.GetEmptyValue();
                    }
                }
            }
        }

        /// <summary>
        /// Gets the surrounding wall count.
        /// </summary>
        /// <param name="gridX">
        /// The grid x.
        /// </param>
        /// <param name="gridY">
        /// The grid y.
        /// </param>
        /// <returns>
        /// The number of neighbors that are surrounding the grid location.
        /// </returns>
        private int GetSurroundingSolidCount(int gridX, int gridY)
        {
            int wallCount = 0;
            for (int neighborX = gridX - 1; neighborX <= gridX + 1; neighborX++)
            {
                for (int neighborY = gridY - 1; neighborY <= gridY + 1; neighborY++)
                {
                    if (neighborX >= 0 && neighborX < this.width && neighborY >= 0 && neighborY < this.height)
                    {
                        if (neighborX != gridX || neighborY != gridY)
                        {
                            wallCount += this.IsSolidValue(this.map[neighborX, neighborY]) ? 1 : 0;
                        }
                    }
                    else
                    {
                        wallCount++;
                    }
                }
            }

            return wallCount;
        }

        /// <summary>
        ///     Smoothes the map values.
        /// </summary>
        private void SmoothMap()
        {
            for (int x = 0; x < this.width; x++)
            {
                for (int y = 0; y < this.height; y++)
                {
                    // get surrounding solid count
                    int neighborWallTiles = this.GetSurroundingSolidCount(x, y);

                    if (neighborWallTiles > 4)
                    {
                        this.map[x, y] = this.GetSolidValue();
                    }
                    else if (neighborWallTiles < 4)
                    {
                        this.map[x, y] = this.GetEmptyValue();
                    }
                }
            }
        }

        #endregion
    }
}