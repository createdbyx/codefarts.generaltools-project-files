﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.GeneralTools
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using Codefarts.GeneralTools.Models;

    /// <summary>
    /// Provides a utility for synchronizing files and folders. 
    /// </summary>
    public class FileSyncUtility
    {
        /// <summary>
        /// Synchronizes files by copying them from there source locations into the projects destination locations.
        /// </summary>
        /// <param name="models">
        /// The sync models used to determine what to sync.
        /// </param>
        /// <param name="path">
        /// The destination path where files will be synced to.
        /// </param>
        /// <param name="codeFileExtensions">An array of code file extensions.</param>
        public static void Sync(IEnumerable<SyncModel> models, string path, string[] codeFileExtensions)
        {
            // Process each element in the table model
            foreach (var model in models)
            {
                // build destination path
                var destinationPath = Path.Combine(path, string.IsNullOrEmpty(model.Destination) ? string.Empty : model.Destination);

                switch (model.SyncType)
                {
                    case CodeSyncType.File:
                        // skip it if not specified
                        if (string.IsNullOrEmpty(model.Source))
                        {
                            continue;
                        }

                        // ensure source file exists
                        if (File.Exists(model.Source))
                        {
                            FilteredFileCopy(model.Source, Path.Combine(path, model.Destination), model.Filters, codeFileExtensions);
                        }

                        break;

                    case CodeSyncType.Folder:
                        // ensure source directory exists
                        if (string.IsNullOrEmpty(model.Source) || !Directory.Exists(model.Source))
                        {
                            continue;
                        }

                        // copy directory structure
                        var filterModel = model.Filters;
                        CopyDirectories(
                            model.Source,
                            destinationPath,
                            true,
                            true,
                            (source, destination, progress) =>
                            {
                                // we will handle file copying our selves so perform a filtered file copy and return false
                                // to prevent the CopyDirectories method from copying the file.
                                FilteredFileCopy(source, destination, filterModel, codeFileExtensions);
                                return false;
                            });
                        break;

                    case CodeSyncType.Project:
                        // ensure source file exists
                        if (string.IsNullOrEmpty(model.Source))
                        {
                            // continue;
                        }

                        break;
                }
            }
        }

        /// <summary>
        /// Copies a directory structure to the destination.
        /// </summary>
        /// <param name="source">The directory structure to be copied.</param>
        /// <param name="destination">The destination where the directory structure will be copied to.</param>
        /// <param name="copySubDirectories">true to copy all sub directories.</param>
        /// <param name="overwriteFiles">true if the destination files can be overwritten; otherwise, false.</param>
        /// <param name="callback">Provides a callback function for reporting progress. </param>
        /// <remarks><p>The callback invoked just before a file copy occurs providing a way of being notified.</p>
        /// <p>The callback parameter order is source file, destination file, progress.</p>
        /// <p>If the callback is specified it should return true to allow the file copy to occur.</p> 
        /// <p>The progress parameter reports progress from 0 to 100. Values to the left of the decimal represent folder copy progress and values to the
        /// right of the decimal from 0.000 to 0.99 represent the current file copy progress for the folder that is being copied.</p>
        /// <p>To get the current file copy progress as a value from 0 to 100 use the formula fileProgress = progress - 100 * 100.</p></remarks>
        public static void CopyDirectories(string source, string destination, bool copySubDirectories, bool overwriteFiles, Func<string, string, float, bool> callback)
        {
            // ensure source folder exists
            if (!Directory.Exists(source))
            {
                throw new DirectoryNotFoundException("The path specified in source is invalid (for example, it is on an unmapped drive).");
            }

            // create destination folder
            Directory.CreateDirectory(destination);

            // get all files in source and copy them to destination folder
            var files = Directory.GetFiles(source);
            var progress = 0f; // used to report the progress from 0 to 100

            // set up action to copy files
            var fileProcessor = new Action<float, string[], string>((folderProgress, filesToCopy, folder) =>
            {
                // copy files
                for (var i = 0; i < filesToCopy.Length; i++)
                {
                    // get file
                    var file = filesToCopy[i];

                    // set default result
                    var result = true;

                    // build destination filename
                    var fileName = Path.GetFileName(file);
                    if (fileName == null) // should never happen
                    {
                        return;
                    }

                    fileName = Path.Combine(folder, fileName);

                    // check if callback specified
                    if (callback != null)
                    {
                        // store result from callback
                        result = callback(file, fileName, progress);
                    }

                    // if result is true we are allowed to copy the file
                    if (result)
                    {
                        File.Copy(file, fileName, overwriteFiles);
                    }

                    // (folder progress * 100) + file progress
                    progress = folderProgress + ((float)i / filesToCopy.Length);
                }
            });

            // copy initial files
            fileProcessor(0, files, destination);

            // check to copy sub directories
            if (!copySubDirectories)
            {
                return;
            }

#if UNITY_WEBPLAYER
            // get the folder tree for the source folder
            var folders = Directory.GetDirectories(source, "*.*");
            var folderIndex = 0;

            // NOTE: This code may or may not be subject to infinite recursion via symbolic links.
            // https://en.wikipedia.org/wiki/NTFS_symbolic_link
            while (folderIndex < folders.Length)
            {
                var subFolders = Directory.GetDirectories(folders[folderIndex], "*.*");
                if (subFolders.Length > 0)
                {
                    Array.Resize(ref folders, folders.Length + subFolders.Length);
                    Array.Copy(subFolders, 0, folders, folders.Length - subFolders.Length, subFolders.Length);
                }

                folderIndex++;
            }
#else
            // get the folder tree for the source folder
            var folders = Directory.GetDirectories(source, "*.*", SearchOption.AllDirectories);
#endif

            // process each sub folder
            for (var index = 0; index < folders.Length; index++)
            {
                // get folder and increment index
                var folder = folders[index];

                // get files
                files = Directory.GetFiles(folder);

                // crop source root from destination and build destination folder path
                folder = folder.Remove(0, source.Length);
                folder = Path.Combine(destination, folder);

                // create destination folder
                Directory.CreateDirectory(folder);

                // process file copying
                fileProcessor((index / folders.Length) * 100, files, folder);
            }
        }

        /// <summary>
        /// Copies a file to the destination if it passes all filtering rules.
        /// </summary>
        /// <param name="source">The source file to be copied.</param>
        /// <param name="destinationPath">The destination path where the source file will be copied to.</param>
        /// <param name="filters">The list of filter conditions to be satisfied before file copy will be allowed.</param>
        /// <param name="codeFileExtensions">An array of code file extensions.</param>
        public static void FilteredFileCopy(string source, string destinationPath, IEnumerable<FilterModel> filters, string[] codeFileExtensions)
        {
            if (destinationPath == null)
            {
                throw new ArgumentNullException("destinationPath");
            }

            // if there are filters apply them
            StringBuilder fileData = null;
            if (filters != null)
            {
                // check filtering rules
                foreach (var filter in filters)
                {
                    var extensionMatch = Path.GetExtension(source) == filter.Extension;
                    var extensionSpecified = !string.IsNullOrEmpty(filter.Extension);
                    if (extensionSpecified && extensionMatch && !filter.Allow)
                    {
                        // matched extension but not allowing so return
                        return;
                    }

                    var index = Array.IndexOf(codeFileExtensions, filter.Extension);
                    if (index != -1)
                    {
                        if (fileData == null)
                        {
                            using (var stream = new StreamReader(source))
                            {
                                //fileData = new StringBuilder(File.ReadAllText(source));
                                fileData = new StringBuilder(stream.ReadToEnd());
                            }
                        }

                        fileData.Replace(filter.Search, filter.Replace);
                    }
                }
            }

            // check if the source has a file name
            var file = Path.GetFileName(source);
            if (file == null)
            {
                // no file name so skip entry
                return;
            }

            // create destination folder
            Directory.CreateDirectory(destinationPath);
            var destinationFileName = Path.Combine(destinationPath, file);

            if (fileData == null)
            {
                // copy the file and overwrite if already exists
                File.Copy(source, destinationFileName, true);
            }
            else
            {
                using (var stream = new StreamWriter(file))
                {
                    stream.Write(fileData.ToString());
                    //File.WriteAllText(destinationFileName, fileData.ToString());
                }
            }
        }

        private byte[] ReadBytes(string file)
        {
            using (var stream = File.Open(file, FileMode.Open))
            {
                // Read the source file into a byte array. 
                var bytes = new byte[stream.Length];
                var numBytesToRead = (int)stream.Length;
                var numBytesRead = 0;
                while (numBytesToRead > 0)
                {
                    // Read may return anything from 0 to numBytesToRead. 
                    var n = stream.Read(bytes, numBytesRead, numBytesToRead);

                    // Break when the end of the file is reached. 
                    if (n == 0)
                    {
                        break;
                    }

                    numBytesRead += n;
                    numBytesToRead -= n;
                }

                return bytes;
            }
        }
    }
}
