namespace Codefarts.GeneralTools.Code
{
    using System;

    public class SelectionArgs : EventArgs
    {
        public SelectionArgs(SelectionAction action, int index, object[] oldItems, object[] newItems)
        {
            this.Action = action;
            this.Index = index;
            this.OldItems = oldItems;
            this.NewItems = newItems;
        }

        public SelectionAction Action { get; set; }

        public int Index { get; set; }

        public object[] OldItems { get; set; }

        public object[] NewItems { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectionArgs"/> class.
        /// </summary>
        public SelectionArgs()
        {   
        }
    }
}