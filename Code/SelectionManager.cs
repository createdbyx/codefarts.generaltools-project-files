// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.Code
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public class SelectionManager
    {
        private static SelectionManager singleton;

        public event EventHandler<SelectionArgs> Status;
        SelectionArgs args = new SelectionArgs();

        protected virtual void OnStatus(SelectionAction action, int index, object[] oldItems, object[] newItems)
        {
            var handler = this.Status;
            if (handler != null)
            {
                this.args.Action = action;
                this.args.Index = index;
                this.args.OldItems = oldItems;
                this.args.NewItems = newItems;
                handler(this, this.args);
            }
        }

        private static object lockObject = new object();

        private List<object> selections = new List<object>();

        public object[] GetSelections()
        {
            var items = new object[this.selections.Count];
            this.selections.CopyTo(items, 0);
            return items;
        }

        public T[] GetSelections<T>()
        {
            var items = new T[this.selections.Count];
            var count = 0;
            lock (this.selections)
            {
                foreach (var selection in this.selections)
                {
                    if (selection is T)
                    {
                        items[count++] = (T)selection;
                    }
                }
            }

            Array.Resize(ref items, count);
            return items;
        }

        public IEnumerator GetEnumerator()
        {
            return this.selections.GetEnumerator();
        }

        public object this[int index]
        {
            get
            {
                return this.selections[index];
            }

            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value can not be null.");
                }

                var oldItem = this.selections[index];
                var changed = value != oldItem;
                this.selections[index] = value;
                if (changed)
                {
                    this.OnStatus(SelectionAction.Changed, index, new[] { oldItem }, new[] { value });
                }
            }
        }

        public void Clear()
        {
            var oldItems = this.GetSelections();
            this.selections.Clear();
            this.OnStatus(SelectionAction.Cleared, -1, oldItems, null);
        }

        public void Remove(object item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item can not be null.");
            }

            var index = this.selections.IndexOf(item);
            if (index != -1)
            {
                this.selections.Remove(item);
                this.OnStatus(SelectionAction.Removed, index, new[] { item }, null);
            }
        }

        public void RemoveAt(int index)
        {
            var oldItem = this.selections[index];
            this.selections.RemoveAt(index);
            this.OnStatus(SelectionAction.Removed, index, new[] { oldItem }, null);
        }

        public void Add(object item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item can not be null.");
            }

            this.selections.Add(item);
            this.OnStatus(SelectionAction.Added, this.selections.Count - 1, null, new[] { item });
        }

        public void Add(IEnumerable<object> items)
        {
            foreach (var item in items)
            {
                if (item == null)
                {
                    continue;
                    //throw new ArgumentNullException("item can not be null.");
                }

                this.selections.Add(item);
                this.OnStatus(SelectionAction.Added, this.selections.Count - 1, null, new[] { item });
            }
        }

        public bool HasSelections
        {
            get
            {
                return this.selections.Count > 0;
            }
        }

        public static SelectionManager Instance
        {
            get
            {
                if (singleton == null)
                {
                    lock (lockObject)
                    {
                        singleton = new SelectionManager();
                    }
                }

                return singleton;
            }
        }

        public int Count
        {
            get
            {
                return this.selections.Count;
            }
        }
    }
}