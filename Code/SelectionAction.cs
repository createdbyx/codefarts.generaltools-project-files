namespace Codefarts.GeneralTools.Code
{
    public enum SelectionAction
    {
        Added,
        Removed,
        Changed,
        Cleared
    }
}