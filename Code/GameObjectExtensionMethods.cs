﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.Code
{
    using UnityEngine;

    /// <summary>
    /// Provides extension methods for <see cref="GameObject"/>.
    /// </summary>
    public static class GameObjectExtensionMethods
    {
        /// <summary>
        /// Converts the mesh into to a xml string.
        /// </summary>
        /// <param name="gameObject">The game object to get the mesh information from.</param>
        /// <param name="children">if set to <c>true</c> all child objects will also be included.</param>
        /// <returns>A string containing the xml data.</returns>
        public static string ToMeshXml(this GameObject gameObject, bool children)
        {
            var meshFilter = gameObject.GetComponent<MeshFilter>();
            if (meshFilter == null)
            {
                return string.Empty;
            }

            //// var mesh = meshFilter.mesh;

            return string.Empty;
        }
    }
}
