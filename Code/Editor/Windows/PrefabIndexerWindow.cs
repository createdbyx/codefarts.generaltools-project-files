namespace Codefarts.GeneralTools.Editor.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Codefarts.GeneralTools.Editor.Controls;
    using Codefarts.GeneralTools.Models;
    using Codefarts.Localization;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides a tool window for constructing a asset containing metedata to a collection of project assets.
    /// </summary>
    public class PrefabIndexerWindow : EditorWindow
    {
        /// <summary>
        /// The output folder used to specify the name of the generated asset.
        /// </summary>
        private SelectOutputFolderControl outputFolder;

        /// <summary>
        /// The results scroll position.
        /// </summary>
        private Vector2 resultsScroll;

        /// <summary>
        /// The result list for the results tree.
        /// </summary>
        private List<TreeViewNode> result = new List<TreeViewNode>();

        /// <summary>
        /// The scroll position for the build options.
        /// </summary>
        private Vector2 optionsScroll;

        /// <summary>
        /// The expanded label
        /// </summary>
        private GUIStyle expandedLabel;

        /// <summary>
        /// The include all assets flag.
        /// </summary>
        private bool includeAllAssets;

        /// <summary>
        /// The include folders flag.
        /// </summary>
        private bool includeFolders;

        /// <summary>
        /// The currently selected tree node.
        /// </summary>
        private TreeViewNode selectedNode;

        /// <summary>
        /// The excluded file extensions.
        /// </summary>
        private string excludeExtensions = string.Empty;

        /// <summary>
        /// The include resource folders flag.
        /// </summary>
        private bool includeResourceFolders;

        /// <summary>
        /// The file icon used to indicate files in the treeview.
        /// </summary>
        private Texture2D fileIcon;

        /// <summary>
        /// The folder icon used to indicate folders in the treeview.
        /// </summary>
        private Texture2D folderIcon;

        /// <summary> 
        /// The open folder icon used to indicate open folders in the treeview.
        /// </summary>
        private Texture2D openFolderIcon;

        /// <summary>
        /// Holds a collection of file paths to trim when building.
        /// </summary>
        /// <remarks>
        /// Each line is a separate path.
        /// </remarks>
        private string trimPaths = string.Empty;

        /// <summary>
        /// The show file filters flag.
        /// </summary>
        private bool showFileFilters;

        /// <summary>
        /// The file filter scroll position.
        /// </summary>
        private Vector2 fileFilterScroll;

        /// <summary>
        /// The file filter list containing file extensions and how many assets have that extension.
        /// </summary>
        private Dictionary<string, int> fileFilterList = new Dictionary<string, int>();

        /// <summary>
        /// Provides a tree node for the results tree view.
        /// </summary>
        public class TreeViewNode
        {
            /// <summary>
            /// Gets or sets the child nodes.
            /// </summary>
            public List<TreeViewNode> Nodes { get; set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="TreeViewNode"/> class.
            /// </summary>
            public TreeViewNode()
            {
                // make sure nodes has a value
                this.Nodes = new List<TreeViewNode>();
            }

            /// <summary>
            /// Gets or sets the value for the node.
            /// </summary>                          
            public string Value { get; set; }

            /// <summary>
            /// Gets or sets the node name.
            /// </summary>             
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this node is a file.
            /// </summary>
            /// <value>
            ///   <c>true</c> if this node is a file; otherwise, <c>false</c>.
            /// </value>
            public bool IsFile { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this node is expanded.
            /// </summary>
            /// <value>
            /// <c>true</c> if this node is expanded; otherwise, <c>false</c>.
            /// </value>
            public bool IsExpanded { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this node is checked.
            /// </summary>
            /// <value>
            /// <c>true</c> if this node is checked; otherwise, <c>false</c>.
            /// </value>
            public bool IsChecked { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this node is selected.
            /// </summary>
            /// <value>
            /// <c>true</c> if this node is selected; otherwise, <c>false</c>.
            /// </value>
            public bool IsSelected { get; set; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PrefabIndexerWindow"/> class.
        /// </summary>
        public PrefabIndexerWindow()
        {
            // create output folder control
            this.outputFolder = new SelectOutputFolderControl();
        }

        /// <summary>
        /// Setups the variables needed for drawing the ui.
        /// </summary>
        public void SetupVariables()
        {
            // if expanded label style is null create it
            if (this.expandedLabel == null)
            {
                this.expandedLabel = new GUIStyle(GUI.skin.label);
                this.expandedLabel.normal.textColor = Color.blue;
            }

            // ensure tree node icons are created
            if (this.fileIcon == null)
            {
                this.fileIcon = Resources.Load<Texture2D>("Codefarts.Unity/Codefarts.GeneralTools/file");
            }


            if (this.folderIcon == null)
            {
                this.folderIcon = Resources.Load<Texture2D>("Codefarts.Unity/Codefarts.GeneralTools/folder");
            }


            if (this.openFolderIcon == null)
            {
                this.openFolderIcon = Resources.Load<Texture2D>("Codefarts.Unity/Codefarts.GeneralTools/openfolder");
            }
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            // ensure variables are setup
            this.SetupVariables();
            
            GUILayout.BeginVertical();
            
            // draw top botton bar
            this.DrawTopControls();

            // draw results and optinos
            GUILayout.BeginHorizontal();
            this.DrawResults();
            this.DrawOptions();
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
        }

        /// <summary>
        /// Draws the options.
        /// </summary>
        private void DrawOptions()
        {
            var local = LocalizationManager.Instance;

            // options side bar
            this.optionsScroll = GUILayout.BeginScrollView(this.optionsScroll, false, false, GUILayout.MaxWidth(350));
            GUILayout.BeginVertical();

            // include all assets oven ones outside the assets folder
            this.includeAllAssets = GUILayout.Toggle(this.includeAllAssets, local.Get("All"));

            // include empty folders
            this.includeFolders = GUILayout.Toggle(this.includeFolders, local.Get("EmptyFolders"));

            // include resource folders
            this.includeResourceFolders = GUILayout.Toggle(this.includeResourceFolders, local.Get("ResourceFolders"));

            // exclude file extensions
            GUILayout.Label(local.Get("ExcludeFileExtensions"));
            GUILayout.BeginHorizontal();
            this.excludeExtensions = GUILayout.TextField(this.excludeExtensions);
            this.showFileFilters = GUILayout.Toggle(this.showFileFilters, local.Get("Filter"), GUI.skin.button, GUILayout.ExpandWidth(false));
            GUILayout.EndHorizontal();

            // check and uncheck buttons to immediately filter the index
            GUILayout.BeginHorizontal();  // filter buttons

            if (GUILayout.Button(local.Get("Check")))
            {
                this.ApplyFilter(this.excludeExtensions, true);
            }

            if (GUILayout.Button(local.Get("Uncheck")))
            {
                this.ApplyFilter(this.excludeExtensions, false);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();    // filter buttons

            // draw file extension filter list
            this.DrawFileFilterList();

            // draw removal path 
            GUILayout.Label(local.Get("TrimPaths"));
            this.trimPaths = GUILayout.TextArea(this.trimPaths, int.MaxValue);
            if (GUILayout.Button(local.Get("AddSelected")) && this.selectedNode != null)
            {
                this.trimPaths += "\r\n" + (this.selectedNode.IsFile ? Path.GetDirectoryName(this.selectedNode.Value) : this.selectedNode.Value);
            }

            // draw the output folder for the generated asset
            this.outputFolder.Draw();

            //  GUILayout.BeginHorizontal(); // build & generate buttons

            // draw the build button to build the asset list
            if (GUILayout.Button(local.Get("Generate")))
            {
                this.GenerateIndex();
            }

            GUILayout.FlexibleSpace();

            // draw the build button to build the asset list
            var buildAsset = GUILayout.Button(local.Get("Build"));

            // GUILayout.EndHorizontal();   // build & generate buttons

            GUILayout.EndVertical();
            GUILayout.EndScrollView();

            if (buildAsset)
            {
                this.BuildAsset();
            }
        }

        private void DrawFileFilterList()
        {
            if (this.showFileFilters)
            {
                this.fileFilterScroll = GUILayout.BeginScrollView(this.fileFilterScroll, false, false, GUILayout.MaxHeight(200));
                GUILayout.BeginVertical();
                var keys = this.fileFilterList.Keys.OrderBy(x => x).ToArray();
                foreach (var extension in keys)
                {
                    this.DrawFileExtensionFilter(extension);
                }

                GUILayout.EndVertical();
                GUILayout.EndScrollView();
            }
        }

        private void DrawFileExtensionFilter(string extension)
        {
            if (GUILayout.Button(string.Format("{0} - {1}", this.fileFilterList[extension], extension)))
            {   
                this.excludeExtensions += "," + extension; 
                var list = new List<TreeViewNode>(this.result);
                while (list.Count > 0)
                {
                    var node = list[0];
                    list.RemoveAt(0);

                    if (node.IsFile && Path.GetExtension(node.Value).Equals(extension, StringComparison.InvariantCultureIgnoreCase))
                    {
                        node.IsChecked = false;
                    }

                    if (node.Nodes.Count > 0)
                    {
                        list.AddRange(node.Nodes);
                    }
                }
            }
        }

        private void ApplyFilter(string filter, bool value)
        {
            if (string.IsNullOrEmpty(filter))
            {
                return;
            }

            var fileExtensionParts = filter.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (fileExtensionParts.Length == 0)
            {
                return;
            }

            var list = new List<TreeViewNode>(this.result);
            while (list.Count > 0)
            {
                var node = list[0];
                list.RemoveAt(0);

                if (node.IsFile)
                {
                    var extension = Path.GetExtension(node.Value);
                    if (fileExtensionParts.Any(x => x.Trim().Equals(extension, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        node.IsChecked = value;
                    }
                }

                if (node.Nodes.Count > 0)
                {
                    list.AddRange(node.Nodes);
                }
            }
        }

        private void BuildAsset()
        {
            try
            {
                if (string.IsNullOrEmpty(this.outputFolder.OutputPath))
                {
                    return;
                }

                this.outputFolder.SetOutputPath(this.outputFolder.OutputPath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));
                var filePath = Path.ChangeExtension(Path.Combine("Assets", this.outputFolder.OutputPath), ".asset");
                if (string.IsNullOrEmpty(filePath))
                {
                    return;
                }

                var modelList = new List<AssetPathModel>();
                var list = new List<TreeViewNode>(this.result);
                while (list.Count > 0)
                {
                    var node = list[0];
                    list.RemoveAt(0);

                    if (node.IsFile && node.IsChecked)
                    {
                        var reference = AssetDatabase.LoadAssetAtPath(node.Value, typeof(UnityEngine.Object));
                        var parts = node.Value.Split(new[] { Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);
                        var assetType = parts.All(x => !x.Equals("Resources", StringComparison.InvariantCultureIgnoreCase))
                                            ? "Asset"
                                            : "Resource";
                        var model = ScriptableObject.CreateInstance<AssetPathModel>();
                        model.Path = node.Value;
                        model.Reference = reference;
                        model.Type = assetType;
                        modelList.Add(model);
                    }

                    if (node.Nodes.Count > 0)
                    {
                        list.AddRange(node.Nodes);
                    }
                }

                var content = ScriptableObject.CreateInstance<AssetPathCollectionModel>();

                content.Models = modelList.ToArray();

                AssetDatabase.CreateAsset(content, filePath);
                foreach (var model in modelList)
                {
                    AssetDatabase.AddObjectToAsset(model, filePath);
                }

                AssetDatabase.SaveAssets();
                AssetDatabase.ImportAsset(filePath);
                // EditorUtility.FocusProjectWindow();
                Selection.activeObject = content;
            }
            catch (Exception ex)
            {
                EditorUtility.DisplayDialog("Error", ex.ToString(), "Close");
            }
        }

        private void DrawNode(Rect rect, TreeViewNode node, int depth = 0)
        {
            // GUILayout.BeginHorizontal();
            //GUILayout.Space(8 * depth);

            //if (node.IsFile && !string.IsNullOrEmpty(Path.GetDirectoryName(node.Value)))
            // {
            //  GUILayout.Label(node.IsExpanded ? this.plusTexture : this.minusTexture, GUILayout.ExpandWidth(false));
            // }

            var content = new GUIContent(node.Name, this.fileIcon);
            var calcHeight = GUI.skin.label.CalcHeight(GUIContent.none, 1);
            var calcWidth = GUI.skin.toggle.CalcSize(GUIContent.none).x;
            // var maxHeight = GUILayout.MaxHeight(calcHeight);

            var value = GUI.Toggle(new Rect(8 * depth, rect.y, calcWidth, calcHeight), node.IsChecked, string.Empty);
            if (value != node.IsChecked)
            {
                node.IsChecked = value;
            }

            if (!node.IsFile)
            {
                var selectedText = node.Nodes.Count > 0 && !node.IsExpanded ? " (Children: " + node.Nodes.Count + ")" : string.Empty;
                //var content = new GUIContent(node.Name + selectedText, node.IsExpanded ? this.openFolderIcon : this.folderIcon);
                //var maxHeight = GUILayout.MaxHeight(GUI.skin.label.CalcHeight(GUIContent.none, 1));
                content.text += selectedText;
                content.image = node.IsExpanded ? this.openFolderIcon : this.folderIcon;
                if (GUI.Button(new Rect(8 * depth + calcWidth, rect.y, rect.width, calcHeight), content, node.IsSelected ? this.expandedLabel : GUI.skin.label))
                {
                    node.IsExpanded = !node.IsExpanded;
                    if (this.selectedNode != null)
                    {
                        this.selectedNode.IsSelected = false;
                    }

                    node.IsSelected = true;
                    this.selectedNode = node;
                }
            }
            else
            {
                GUI.Label(new Rect(8 * depth + calcWidth, rect.y, rect.width, calcHeight), content);
            }

            // GUILayout.EndHorizontal();

            if (node.IsExpanded)
            {
                foreach (var child in node.Nodes)
                {
                    rect.y += calcHeight;
                    // rect.position += new Vector2(0,calcHeight);
                    this.DrawNode(rect, child, depth + 2);
                }
            }
        }

        private void DrawNode(TreeViewNode node, int depth = 0)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(8 * depth);

            //if (node.IsFile && !string.IsNullOrEmpty(Path.GetDirectoryName(node.Value)))
            // {
            //  GUILayout.Label(node.IsExpanded ? this.plusTexture : this.minusTexture, GUILayout.ExpandWidth(false));
            // }

            var value = GUILayout.Toggle(node.IsChecked, string.Empty, GUILayout.ExpandWidth(false));
            if (value != node.IsChecked)
            {
                node.IsChecked = value;
            }

            if (!node.IsFile)
            {
                var selectedText = node.Nodes.Count > 0 && !node.IsExpanded ? " (Children: " + node.Nodes.Count + ")" : string.Empty;
                var content = new GUIContent(node.Name + selectedText, node.IsExpanded ? this.openFolderIcon : this.folderIcon);
                var maxHeight = GUILayout.MaxHeight(GUI.skin.label.CalcHeight(GUIContent.none, 1));
                if (GUILayout.Button(content, node.IsSelected ? this.expandedLabel : GUI.skin.label, maxHeight))
                {
                    node.IsExpanded = !node.IsExpanded;
                    if (this.selectedNode != null)
                    {
                        this.selectedNode.IsSelected = false;
                    }

                    node.IsSelected = true;
                    this.selectedNode = node;
                }
            }
            else
            {
                var content = new GUIContent(node.Name, this.fileIcon);
                var maxHeight = GUILayout.MaxHeight(GUI.skin.label.CalcHeight(GUIContent.none, 1));
                GUILayout.Label(content, maxHeight);
            }

            GUILayout.EndHorizontal();

            if (node.IsExpanded)
            {
                foreach (var child in node.Nodes)
                {
                    this.DrawNode(child, depth + 2);
                }
            }
        }

        /// <summary>
        /// Draws the result controls.
        /// </summary>
        private void DrawResults()
        {
            this.resultsScroll = GUILayout.BeginScrollView(this.resultsScroll, false, false);
            GUILayout.BeginVertical();
            foreach (var node in this.result)
            {
                this.DrawNode(node);
            }

            GUILayout.EndVertical();
            GUILayout.EndScrollView();

            //var rect = GUILayoutUtility.GetRect(1, Screen.width, 1, Screen.height);
            //this.viewRect = rect;
            //this.resultsScroll = GUI.BeginScrollView(rect, this.resultsScroll, this.viewRect);// false, false);
            ////GUILayout.BeginVertical();
            //foreach (var node in this.result)
            //{
            //    this.DrawNode(rect, node);
            //}

            ////GUILayout.EndVertical();
            //GUI.EndScrollView();
        }

        /// <summary>
        /// Draws the top controls.
        /// </summary>
        private void DrawTopControls()
        {
            var local = LocalizationManager.Instance;
            GUILayout.BeginHorizontal();

            if (GUILayout.Button(local.Get("ExpandAll")))
            {
                var list = new List<TreeViewNode>(this.result);
                while (list.Count > 0)
                {
                    var node = list[0];
                    list.RemoveAt(0);
                    node.IsExpanded = true;
                    if (node.Nodes.Count > 0)
                    {
                        list.AddRange(node.Nodes);
                    }
                }
            }

            if (GUILayout.Button(local.Get("CollapseAll")))
            {
                var list = new List<TreeViewNode>(this.result);
                while (list.Count > 0)
                {
                    var node = list[0];
                    list.RemoveAt(0);
                    node.IsExpanded = false;
                    if (node.Nodes.Count > 0)
                    {
                        list.AddRange(node.Nodes);
                    }
                }
            }

            if (this.selectedNode != null)
            {
                if (GUILayout.Button(local.Get("CheckChildren")))
                {
                    this.selectedNode.IsChecked = true;
                    foreach (var node in this.selectedNode.Nodes)
                    {
                        node.IsChecked = true;
                    }
                }

                if (GUILayout.Button(local.Get("UncheckChildren")))
                {
                    this.selectedNode.IsChecked = false;
                    foreach (var node in this.selectedNode.Nodes)
                    {
                        node.IsChecked = false;
                    }
                }

                if (GUILayout.Button(local.Get("CheckAllSubChildren")))
                {
                    var list = new List<TreeViewNode>(this.selectedNode.Nodes);
                    this.selectedNode.IsChecked = true;
                    while (list.Count > 0)
                    {
                        var node = list[0];
                        list.RemoveAt(0);
                        node.IsChecked = true;
                        if (node.Nodes.Count > 0)
                        {
                            list.AddRange(node.Nodes);
                        }
                    }
                }

                if (GUILayout.Button(local.Get("UncheckAllSubChildren")))
                {
                    var list = new List<TreeViewNode>(this.selectedNode.Nodes);
                    this.selectedNode.IsChecked = false;
                    while (list.Count > 0)
                    {
                        var node = list[0];
                        list.RemoveAt(0);
                        node.IsChecked = false;
                        if (node.Nodes.Count > 0)
                        {
                            list.AddRange(node.Nodes);
                        }
                    }
                }
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private TreeViewNode CreateTreeNode(string[] parts, int index, string source)
        {
            var node = new TreeViewNode() { Name = parts[index] };
            node.Value = string.Join(Path.AltDirectorySeparatorChar.ToString(CultureInfo.InvariantCulture), parts, 0, index + 1);
            if (index == parts.Length - 1)
            {
                node.Name = Path.GetFileName(source);
            }

            node.IsFile = File.Exists(node.Value);
            if (node.IsFile)
            {
                var extension = Path.GetExtension(node.Value);
                if (this.fileFilterList.ContainsKey(extension))
                {
                    this.fileFilterList[extension] += 1;
                }
                else
                {
                    this.fileFilterList.Add(extension, 1);
                }
            }

            return node;
        }

        /// <summary>
        /// Builds the index.
        /// </summary>
        private void GenerateIndex()
        {
            // get all asset paths
            var paths = from path in AssetDatabase.GetAllAssetPaths()  // array of relative file paths starting with "assets"
                        let data = this.includeAllAssets ? path : (path.StartsWith("assets", StringComparison.InvariantCultureIgnoreCase) ? path : null)
                        where data != null && (this.includeFolders ? Directory.Exists(Path.GetDirectoryName(data)) : File.Exists(data))
                        select data;

            // filter out any paths that
            if (!this.includeResourceFolders)
            {
                paths = from path in paths
                        let parts = path.Split(new[] { Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries)
                        where parts.All(x => !x.Equals("Resources", StringComparison.InvariantCultureIgnoreCase))
                        select path;
            }

            var fileExtensionParts = this.excludeExtensions.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            paths = from path in paths
                    where File.Exists(path)
                    let extension = Path.GetExtension(path)
                    where !string.IsNullOrEmpty(extension) && fileExtensionParts.All(x => !x.Equals(extension, StringComparison.InvariantCultureIgnoreCase))
                    select path;

            var separator = new[] { Path.AltDirectorySeparatorChar.ToString(CultureInfo.InvariantCulture) };
            this.fileFilterList.Clear();
            var nodes = ToHierarchy<string, TreeViewNode, string>(
              paths.OrderBy(x => x),
              x => x.Split(separator, StringSplitOptions.RemoveEmptyEntries),
              (r, p) => string.CompareOrdinal(r.Name, p) == 0,
              r => r.Nodes,
              (r, c) => ((List<TreeViewNode>)r).Add(c),
              this.CreateTreeNode);

            this.result.Clear();
            this.result.AddRange(nodes);
            if (this.result.Count > 0)
            {
                if (this.selectedNode != null)
                {
                    this.selectedNode.IsSelected = false;
                }

                this.selectedNode = this.result[0];
                this.selectedNode.IsSelected = true;
            }
        }

        /// <summary>
        /// Provides unity menu item to open the window.
        /// </summary>
        [MenuItem("Window/Codefarts/General Utilities/Prefab Indexer")]
        private static void ShowWindow()
        {
            // get the window, show it, and hand it focus
            var local = LocalizationManager.Instance;
            GetWindow<PrefabIndexerWindow>(local.Get("PrefabIndexer")).Show();
        }

        /// <summary>
        /// Constructs a nested hierarchy of types from a flat list of source types.
        /// </summary>
        /// <typeparam name="TSource">The source type of the flat list that is to be converted.</typeparam>
        /// <typeparam name="TReturn">The type that will be returned.</typeparam>
        /// <typeparam name="TPart">The type of the art type.</typeparam>
        /// <param name="sourceItems">The source items to be converted.</param>
        /// <param name="getParts">A callback function that returns a array of <see cref="TPart"/>.</param>
        /// <param name="comparePart">The compare part callback.</param>
        /// <param name="getChildren">The get children callback.</param>
        /// <param name="addChild">The add child callback.</param>
        /// <param name="createItem">The create item callback.</param>
        /// <returns>Returns an collection of <see cref="TReturn"/> representing the hierarchy.</returns>
        /// <exception cref="Exception">A delegate callback throws an exception. </exception>
        private static IEnumerable<TReturn> ToHierarchy<TSource, TReturn, TPart>(
          IEnumerable<TSource> sourceItems,
        Func<TSource, TPart[]> getParts,
        Func<TReturn, TPart, bool> comparePart,
        Func<TReturn, IEnumerable<TReturn>> getChildren,
        Action<IEnumerable<TReturn>, TReturn> addChild,
        Func<TPart[], int, TSource, TReturn> createItem)
        {
            var treeModels = new List<TReturn>();
            foreach (var keyName in sourceItems)
            {
                IEnumerable<TReturn> items = treeModels;
                var parts = getParts(keyName);
                for (var partIndex = 0; partIndex < parts.Length; partIndex++)
                {
                    var node = items.FirstOrDefault(x => comparePart(x, parts[partIndex]));
                    if (node != null)
                    {
                        items = getChildren(node);
                        continue;
                    }

                    var model = createItem(parts, partIndex, keyName);
                    addChild(items, model);
                    items = getChildren(model);
                }
            }

            return treeModels;
        }
    }
}