// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.Editor.Windows
{
    using Codefarts.Localization;
    using UnityEditor;
    using UnityEngine;
    using System;
    using System.IO;
    using System.Linq;

    using Assets.Codefarts_Game.GeneralTools.Code.Editor.Common;
    using Codefarts.GeneralTools.Editor.Controls;


    /// <summary>
    /// Provides a tool windows for constructing a texture containing all of the colors of another texture.
    /// </summary>
    public class ColorPaletteWindow : EditorWindow
    {
        private SelectOutputFolderControl outputFolder;
        private TileSelectionControl preview;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorPaletteWindow"/> class.
        /// </summary>
        public ColorPaletteWindow()
        {
            this.outputFolder = new SelectOutputFolderControl();
            preview = new TileSelectionControl();
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            GUILayout.BeginVertical();
            this.DrawHeader();
            this.DrawPreview();
            GUILayout.EndVertical();
        }

        private void DrawPreview()
        {
            GUILayout.BeginVertical();
            this.preview.Draw();
            GUILayout.EndVertical();
        }

        private void DrawHeader()
        {
            var local = LocalizationManager.Instance;

            GUILayout.BeginHorizontal(GUILayout.Height(32));

            if (GUILayout.Button(local.Get("Create")))
            {
                this.CreatePalette();
            }

            GUILayout.BeginVertical();
            this.outputFolder.Draw();
            GUILayout.EndVertical();

            this.preview.TextureAsset = EditorGUILayout.ObjectField(this.preview.TextureAsset, typeof(Texture2D), true, GUILayout.Width(64), GUILayout.Height(64)) as Texture2D;

            GUILayout.EndHorizontal();
        }

        private void CreatePalette()
        {
            if (this.preview.TextureAsset == null)
            {
                return;
            }

            Texture2D readable;
            Texture tempTexture;
            if (!this.preview.TextureAsset.TryCreateReadableTexture(out tempTexture))
            {
                return;
            }

            readable = tempTexture as Texture2D;
            if (readable == null)
            {
                return;
            }

            var pixels = readable.GetPixels32();

            this.DeleteAsset(readable);

            // get unique colors
            pixels = pixels.Distinct().ToArray();
            var size = 1;
            while (pixels.Length > size * size)
            {
                size *= 2;
            }

            var colorCount = pixels.Length;
            var divider = 1;
            while (colorCount < size * (size / divider + 1))
            {
                divider++;
            }

            Array.Resize(ref pixels, size * (size / divider));
            var texture = new Texture2D(size, size, TextureFormat.ARGB32, false);
            texture.SetPixels32(pixels);

            // save texture
            var fileName = Path.Combine(this.outputFolder.OutputPath, "Assets/Palette.png");
            var index = 0;
            while (File.Exists(fileName))
            {
                fileName = Path.Combine(this.outputFolder.OutputPath, string.Format("Assets/Palette{0}.png", index++));
            }

            File.WriteAllBytes(fileName, texture.EncodeToPNG());
            AssetDatabase.ImportAsset(fileName);

            Debug.Log(string.Format("{0} colors detected. Saved palette texture to '{1}'.", colorCount, fileName));
        }

        private void DeleteAsset(Texture2D readable)
        {
            string file = null;
            try
            {
                file = AssetDatabase.GetAssetPath(readable);

                // // destroy the temp texture reference
                UnityEngine.Object.DestroyImmediate(readable, true);

                // try to ensure that the temp texture asset is removed
                AssetDatabase.DeleteAsset(file);
            }
            catch
            {
            }

            try
            {
                // if the temp texture file exists attempt to delete it
                if (File.Exists(file))
                {
                    // try to ensure that the temp texture asset is removed
                    AssetDatabase.DeleteAsset(file);
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Provides unity menu item to open the window.
        /// </summary>
        [MenuItem("Window/Codefarts/General Utilities/Color Palette from Texture")]
        private static void ShowWindow()
        {
            // get the window, show it, and hand it focus
            var local = LocalizationManager.Instance;
            GetWindow<ColorPaletteWindow>(local.Get("TextureToColorPalette")).Show();
        }
    }
}