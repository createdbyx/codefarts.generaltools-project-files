/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.GeneralTools.Editor.Windows
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Codefarts.GeneralTools.Editor.Controls;

    using UnityEditor;

    using UnityEngine;

    using Color = UnityEngine.Color;

    public class MeasurementTextureGenerator : EditorWindow
    {
        public class PreviewItem
        {
            public Texture2D Texture;

            public Texture2D Image;

            public string Name;

            public Vector2 Position;
        }

        private List<PreviewItem> previewItems = new List<PreviewItem>();
        int selectedSize = 512;
        bool chkMipMaps;
        Color fillColor = Color.red;
        Color borderColor = Color.white;
        Color dimensionTextColor = Color.white;
        Color textColor = Color.white;
        Color arrowColor = Color.white;
        Vector2 pnlPreview;
        string txtNames = string.Empty;
        bool chkDimensions;
        bool chkArrows;
        private int currentImage = -1;
        private Font dimensionFont;
        private Font cbLabelFont;
        private int labelFontSize = 8;
        private int dimensionFontSize = 8;
        private int borderSize;
        private int cbType;
        private Color gridColor = Color.white;
        private bool chkGrid;
        private int gridSizeX = 32;
        private int gridSizeY = 32;
        private Color angleColor = Color.white;
        private float startAngle;
        private int angleCount;
        private float angleStep = 15;
        private Color angleTextColor = Color.white;
        private bool chkAngleText;
        private bool chkAngleSymbol;
        private int angleTextOffset;
        private int angleTextDistance = 64;
        private int angleOriginX;
        private int angleOriginY;
        private int angleDistance = 5000;
        private Font cbAngleFont;
        private int angleFontSize = 8;
        private int vecArrowXOffset;
        private int vecArrowYOffset;
        private int vecDimensionXOffset;
        private int vecDimensionYOffset;

        private SelectOutputFolderControl outputFolder;

        private Vector2 scroll;

        private bool expander;

        private readonly int[] sizeItems = new[] { 32, 64, 128, 256, 512, 1024, 2048, 4096 };

        private readonly string[] displayedSizeOptions = new[] { "32", "64", "128", "256", "512", "1024", "2048", "4096" };

        /// <summary>
        /// Initializes a new instance of the <see cref="MeasurementTextureGenerator"/> class.
        /// </summary>
        public MeasurementTextureGenerator()
        {
            this.outputFolder = new SelectOutputFolderControl();
        }

        /// <summary>
        /// Draws a line on an <see cref="Texture2D"/>.
        /// </summary>          
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x1">
        /// The x position for the start of the line.
        /// </param>
        /// <param name="y1">
        /// The y position for the start of the line.
        /// </param>
        /// <param name="x2">
        /// The x position for the end of the line.
        /// </param>
        /// <param name="y2">
        /// The y position for the end of the line.
        /// </param>
        /// <param name="color">
        /// The color that the line will be drawn with.
        /// </param>                                              
        private void DrawLine(Texture2D image, int x1, int y1, int x2, int y2, Color color)
        {
            var pixels = image.GetPixels();

            // source (converted from C) -> http://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#C
            // unity stores texture data left to right bottom to top so corrent y values first
            y1 = image.height - 1 - y1;
            y2 = image.height - 1 - y2;

            int dx = Math.Abs(x2 - x1);
            int dy = Math.Abs(y2 - y1);
            int sx = x1 < x2 ? 1 : -1;
            int sy = y1 < y2 ? 1 : -1;
            int err = (dx > dy ? dx : -dy) / 2;
            int e2;

            while (true)
            {
                var index = (y1 * image.width) + x1;
                if (index >= 0 && index < pixels.Length)
                {
                    pixels[index] = color;
                }

                if (x1 == x2 && y1 == y2)
                {
                    break;
                }

                e2 = err;
                if (e2 > -dx)
                {
                    err -= dy;
                    x1 += sx;
                }

                if (e2 < dy)
                {
                    err += dx;
                    y1 += sy;
                }
            }

            image.SetPixels(pixels);
            image.Apply();
        }

        /// <summary>
        /// Draws a filled Ellipse.
        /// </summary>                 
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x left most position of the ellipse.
        /// </param>
        /// <param name="y">
        /// The y top most position of the ellipse.
        /// </param>
        /// <param name="width">
        /// The width of the ellipse.
        /// </param>
        /// <param name="height">
        /// The height of the ellipse.
        /// </param>
        /// <param name="color">
        /// The value to use.
        /// </param>               
        private void FillEllipse(Texture2D image, int x, int y, int width, int height, Color color)
        {
            var pixels = image.GetPixels();
            width = width / 2;
            height = height / 2;
            var centerX = x + width;
            var centerY = y + height;

            // source -> http://stackoverflow.com/questions/10322341/simple-algorithm-for-drawing-filled-ellipse-in-c-c
            for (var indexY = -height; indexY <= height; indexY++)
            {
                for (var indexX = -width; indexX <= width; indexX++)
                {
                    var dx = indexX / (double)width;
                    var dy = indexY / (double)height;
                    if ((dx * dx) + (dy * dy) <= 1)
                    {
                        var positionX = centerX + indexX;
                        var positionY = image.height - 1 - (centerY + indexY);
                        if (positionX >= 0 && positionX < image.width && positionY >= 0 && positionY < image.height)
                        {
                            var index = (positionY * image.width) + positionX;
                            pixels[index] = color;
                        }
                    }
                }
            }

            image.SetPixels(pixels);
            image.Apply();
        }

        /// <summary>
        /// Draws a Ellipse.
        /// </summary>
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x center position of the circle.
        /// </param>
        /// <param name="y">
        /// The y center position of the circle.
        /// </param>
        /// <param name="width">
        /// The width of the Ellipse.
        /// </param>
        /// <param name="height">
        /// The height of the Ellipse.
        /// </param>
        /// <param name="color">
        /// The value to use.
        /// </param>
        private void DrawEllipse(Texture2D image, int x, int y, int width, int height, Color color)
        {
            // source (converted from C++) -> http://www.dailyfreecode.com/Code/draw-ellipse-midpoint-ellipse-algorithm-714.aspx
            var radiusX = width / 2f;
            var radiusY = height / 2f;
            float aa = radiusX * radiusX;
            float bb = radiusY * radiusY;
            float aa2 = aa * 2;
            float bb2 = bb * 2;

            float positionX = 0;
            float positionY = radiusY;

            float fx = 0;
            float fy = aa2 * radiusY;

            float p = (int)(bb - (aa * radiusY) + (0.25 * aa) + 0.5);
            var centerX = x + radiusX;
            var centerY = y + radiusY;

            var pixels = image.GetPixels();

            var locationX = (int)(centerX + positionX);
            var locationY = image.height - 1 - (int)(centerY + positionY);
            if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
            {
                pixels[(locationY * image.width) + locationX] = color;
            }

            locationY = image.height - 1 - (int)(centerY - positionY);
            if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
            {
                pixels[(locationY * image.width) + locationX] = color;
            }

            locationX = (int)(centerX - positionX);
            if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
            {
                pixels[(locationY * image.width) + locationX] = color;
            }

            locationY = image.height - 1 - (int)(centerY + positionY);
            if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
            {
                pixels[(locationY * image.width) + locationX] = color;
            }

            while (fx < fy)
            {
                positionX++;
                fx += bb2;

                if (p < 0)
                {
                    p += fx + bb;
                }
                else
                {
                    positionY--;
                    fy -= aa2;
                    p += fx + bb - fy;
                }

                locationX = (int)(centerX + positionX);
                locationY = image.height - 1 - (int)(centerY + positionY);
                if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
                {
                    pixels[(locationY * image.width) + locationX] = color;
                }

                locationY = image.height - 1 - (int)(centerY - positionY);
                if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
                {
                    pixels[(locationY * image.width) + locationX] = color;
                }

                locationX = (int)(centerX - positionX);
                if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
                {
                    pixels[(locationY * image.width) + locationX] = color;
                }

                locationY = image.height - 1 - (int)(centerY + positionY);
                if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
                {
                    pixels[(locationY * image.width) + locationX] = color;
                }
            }

            p = (int)((bb * (positionX + 0.5) * (positionX + 0.5)) + (aa * (positionY - 1) * (positionY - 1)) - (aa * bb) + 0.5);

            while (positionY > 0)
            {
                positionY--;
                fy -= aa2;

                if (p >= 0)
                {
                    p += aa - fy;
                }
                else
                {
                    positionX++;
                    fx += bb2;
                    p += fx + aa - fy;
                }

                locationX = (int)(centerX + positionX);
                locationY = image.height - 1 - (int)(centerY + positionY);
                if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
                {
                    pixels[(locationY * image.width) + locationX] = color;
                }

                locationY = image.height - 1 - (int)(centerY - positionY);
                if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
                {
                    pixels[(locationY * image.width) + locationX] = color;
                }

                locationX = (int)(centerX - positionX);
                if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
                {
                    pixels[(locationY * image.width) + locationX] = color;
                }

                locationY = image.height - 1 - (int)(centerY + positionY);
                if (locationX >= 0 && locationX < image.width && locationY >= 0 && locationY < image.height)
                {
                    pixels[(locationY * image.width) + locationX] = color;
                }

                //  pixels[((image.height - 1 - (int)(centerY + positionY)) * image.width) + (int)(centerX + positionX)] = color;
                //  pixels[((image.height - 1 - (int)(centerY - positionY)) * image.width) + (int)(centerX + positionX)] = color;
                //  pixels[((image.height - 1 - (int)(centerY - positionY)) * image.width) + (int)(centerX - positionX)] = color;
                //  pixels[((image.height - 1 - (int)(centerY + positionY)) * image.width) + (int)(centerX - positionX)] = color;
            }

            image.SetPixels(pixels);
            image.Apply();
        }

        /// <summary>
        /// Draws a filled rectangle.
        /// </summary>
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x position of the left side of the rectangle.
        /// </param>
        /// <param name="y">
        /// The y position of the top side of the rectangle.
        /// </param>
        /// <param name="width">
        /// The width of the rectangle.
        /// </param>
        /// <param name="height">
        /// The height of the rectangle.
        /// </param>
        /// <param name="color">
        /// The value to use.
        /// </param>                                                 
        private void FillRectangle(  Texture2D image, int x, int y, int width, int height, Color color)
        {
            FillRectangle(image, x, y, width, height, color, this.BlendColorFunction);
        }

        /// <summary>
        /// Draws a filled rectangle.
        /// </summary>
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x position of the left side of the rectangle.
        /// </param>
        /// <param name="y">
        /// The y position of the top side of the rectangle.
        /// </param>
        /// <param name="width">
        /// The width of the rectangle.
        /// </param>
        /// <param name="height">
        /// The height of the rectangle.
        /// </param>
        /// <param name="color">
        /// The value to use.
        /// </param>                                                 
        /// <param name="blendCallback">
        /// A <see cref="Func{TResult}"/> callback that is used to perform pixel blending.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <see cref="blendCallback"/> is null.
        /// </exception>
        private void FillRectangle(  Texture2D image, int x, int y, int width, int height, Color color, Func<Color, Color, Color> blendCallback)
        {
            if (blendCallback == null)
            {
                throw new ArgumentNullException("blendCallback");
            }

            var pixels = image.GetPixels();
            for (var indexX = 0; indexX < width; indexX++)
            {
                for (var indexY = 0; indexY < height; indexY++)
                {
                    var positionX = indexX + x;
                    var positionY = indexY + y;
                    if (positionX > image.width - 1 || positionY > image.height - 1 || positionX < 0 || positionY < 0)
                    {
                        continue;
                    }

                    var index = ((image.height - 1 - positionY) * image.width) + positionX;
                    pixels[index] = blendCallback(pixels[index], color);
                }
            }

            image.SetPixels(pixels);
            image.Apply();
        }

        /// <summary>
        /// Draws a rectangle.
        /// </summary>
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x position of the left side of the rectangle.
        /// </param>
        /// <param name="y">
        /// The y position of the top side of the rectangle.
        /// </param>
        /// <param name="width">
        /// The width of the rectangle.
        /// </param>
        /// <param name="height">
        /// The height of the rectangle.
        /// </param>
        /// <param name="value">
        /// The value to use.
        /// </param>   
        private void DrawRectangle(  Texture2D image, int x, int y, int width, int height, Color value)
        {
            DrawRectangle(image, x, y, width, height, value, this.BlendColorFunction);
        }

        /// <summary>
        /// Draws a rectangle.
        /// </summary>        
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x position of the left side of the rectangle.
        /// </param>
        /// <param name="y">
        /// The y position of the top side of the rectangle.
        /// </param>
        /// <param name="width">
        /// The width of the rectangle.
        /// </param>
        /// <param name="height">
        /// The height of the rectangle.
        /// </param>
        /// <param name="value">
        /// The value to use.
        /// </param>
        /// <param name="blendCallback">
        /// A <see cref="Func{TResult}"/> callback that is used to perform pixel blending.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <see cref="blendCallback"/> is null.
        /// </exception>
        private void DrawRectangle(  Texture2D image, int x, int y, int width, int height, Color value, Func<Color, Color, Color> blendCallback)
        {
            if (blendCallback == null)
            {
                throw new ArgumentNullException("blendCallback");
            }

            var pixels = image.GetPixels();

            var bottom = y + height;
            var right = x + width;

            // allows negative widths
            if (x > right)
            {
                var temp = right;
                right = x;
                x = temp;
            }

            // allows negative heights
            if (y > bottom)
            {
                var temp = bottom;
                bottom = y;
                y = temp;
            }

            // top 
            if (y > -1 && y < image.height)
            {
                var leftValue = x < 0 ? 0 : x;
                var rightValue = right > image.width - 1 ? image.width - 1 : right;
                for (var i = leftValue; i <= rightValue; i++)
                {
                    var index = ((image.height - 1 - y) * image.width) + i;
                    pixels[index] = blendCallback(pixels[index], value);
                }
            }

            // bottom 
            if (bottom > -1 && bottom < image.height)
            {
                var leftValue = x < 0 ? 0 : x;
                var rightValue = right > image.width - 1 ? image.width - 1 : right;
                for (var i = leftValue; i <= rightValue; i++)
                {
                    var index = ((image.height - 1 - bottom) * image.width) + i;
                    pixels[index] = blendCallback(pixels[index], value);
                }
            }

            // left 
            if (x > -1 && x < image.width)
            {
                var topValue = y < 0 ? 0 : y;
                var bottomValue = bottom > image.height - 1 ? image.height - 1 : bottom;
                for (var i = topValue; i <= bottomValue; i++)
                {
                    var index = ((image.height - 1 - i) * image.width) + x;
                    pixels[index] = blendCallback(pixels[index], value);
                }
            }

            // right 
            if (right > -1 && right < image.width)
            {
                var topValue = y < 0 ? 0 : y;
                var bottomValue = bottom > image.height - 1 ? image.height - 1 : bottom;
                for (var i = topValue; i <= bottomValue; i++)
                {
                    var index = ((image.height - 1 - i) * image.width) + right;
                    pixels[index] = blendCallback(pixels[index], value);
                }
            }

            image.SetPixels(pixels);
            image.Apply();
        }

        /// <summary>
        /// Blends two colors together.
        /// </summary>
        /// <param name="sourceColor">
        /// The source color.
        /// </param>
        /// <param name="blendColor">
        /// The blend color.
        /// </param>
        /// <returns>
        /// Returns the result as a <see cref="Color"/> type.
        /// </returns>
        /// <remarks>
        /// From Wikipedia https://en.wikipedia.org/wiki/Alpha_compositing -> "Alpha blending" 
        /// </remarks>
        private Color BlendColorFunction(Color sourceColor, Color blendColor)
        {
            var sr = blendColor.r / 255f;
            var sg = blendColor.g / 255f;
            var sb = blendColor.b / 255f;
            var sa = blendColor.a / 255f;
            var dr = sourceColor.r / 255f;
            var dg = sourceColor.g / 255f;
            var db = sourceColor.b / 255f;
            var da = sourceColor.a / 255f;

            var oa = sa + (da * (1 - sa));
            var r = ((sr * sa) + ((dr * da) * (1 - sa))) / oa;
            var g = ((sg * sa) + ((dg * da) * (1 - sa))) / oa;
            var b = ((sb * sa) + ((db * da) * (1 - sa))) / oa;
            var a = oa;

            // if alpha is 0 just return source color else build new blended color
            return Math.Abs(a - 0) < Mathf.Epsilon ? sourceColor : new Color((byte)(r * 255), (byte)(g * 255), (byte)(b * 255), (byte)(a * 255));
        }

        // ReSharper disable UnusedMember.Local
        private void OnGUI()
        // ReSharper restore UnusedMember.Local
        {
            this.scroll = GUILayout.BeginScrollView(this.scroll, GUILayout.MaxHeight(1108));
            GUILayout.BeginVertical();

            this.outputFolder.Draw();

            GUILayout.Label("Type");
            this.cbType = EditorGUILayout.Popup(this.cbType, new[] { "Square", "Circle" });

            GUILayout.Label("Size");
            this.selectedSize = EditorGUILayout.IntPopup(this.selectedSize, this.displayedSizeOptions, this.sizeItems);

            this.dimensionFont = EditorGUILayout.ObjectField("Dimension Font", this.dimensionFont, typeof(Font), false) as Font;
            this.cbLabelFont = EditorGUILayout.ObjectField("Label Font", this.cbLabelFont, typeof(Font), false) as Font;
            this.cbAngleFont = EditorGUILayout.ObjectField("Angle Font", this.cbAngleFont, typeof(Font), false) as Font;

            this.dimensionFontSize = EditorGUILayout.IntField("Dimension Font Size", this.dimensionFontSize);
            this.dimensionFontSize = this.dimensionFontSize < 1 ? 1 : this.dimensionFontSize;
            this.dimensionFontSize = this.dimensionFontSize > 200 ? 200 : this.dimensionFontSize;

            this.angleFontSize = EditorGUILayout.IntField("Angle Font Size", this.angleFontSize);
            this.angleFontSize = this.angleFontSize < 1 ? 1 : this.angleFontSize;
            this.angleFontSize = this.angleFontSize > 200 ? 200 : this.angleFontSize;

            this.labelFontSize = EditorGUILayout.IntField("Label Font Size", this.labelFontSize);
            this.labelFontSize = this.labelFontSize < 1 ? 1 : this.labelFontSize;
            this.labelFontSize = this.labelFontSize > 200 ? 200 : this.labelFontSize;

            this.borderSize = EditorGUILayout.IntField("Border Size", this.borderSize);
            this.borderSize = this.borderSize < 1 ? 1 : this.borderSize;
            this.borderSize = this.borderSize > 200 ? 200 : this.borderSize;

            // create texture color controls
            GUILayout.BeginVertical();

            // mip maps
            this.chkMipMaps = GUILayout.Toggle(this.chkMipMaps, "Mipmaps");

            // dimension text
            this.chkDimensions = GUILayout.Toggle(this.chkDimensions, "Dimensions");

            // draw arrows?
            this.chkArrows = GUILayout.Toggle(this.chkArrows, "Arrows");

            // draw angle text?
            this.chkAngleText = GUILayout.Toggle(this.chkAngleText, "Angle Text");

            // draw angle symbol?
            this.chkAngleSymbol = GUILayout.Toggle(this.chkAngleSymbol, "Angle Degree Symbol");

            GUILayout.EndVertical();

            // grid size
            this.chkGrid = GUILayout.Toggle(this.chkGrid, "Grid");

            //colors
            // pnlStack = new StackPanel() { Orientation = Orientation.Vertical, Height = 125, MaxHeight = 125 };
            this.expander = EditorGUILayout.Foldout(this.expander, "Colors");
            if (this.expander)
            {
                this.fillColor = EditorGUILayout.ColorField("Fill Color", this.fillColor);
                this.borderColor = EditorGUILayout.ColorField("Border Color", this.borderColor);
                this.textColor = EditorGUILayout.ColorField("Text Color", this.textColor);
                this.dimensionTextColor = EditorGUILayout.ColorField("Dimension Text Color", this.dimensionTextColor);
                this.arrowColor = EditorGUILayout.ColorField("Arrow Color", this.arrowColor);
                this.gridColor = EditorGUILayout.ColorField("Grid Color", this.gridColor);
                this.angleColor = EditorGUILayout.ColorField("Angle Color", this.angleColor);
                this.angleTextColor = EditorGUILayout.ColorField("Angle Text Color", this.angleTextColor);
            }

            // grid size label
            GUILayout.Label("Grid Size");

            // grid size
            GUILayout.BeginHorizontal();
            GUILayout.Label("X:");
            this.gridSizeX = EditorGUILayout.IntField(this.gridSizeX);
            this.gridSizeX = this.gridSizeX < 1 ? 1 : this.gridSizeX;
            GUILayout.Label("Y:");
            this.gridSizeY = EditorGUILayout.IntField(this.gridSizeY);
            this.gridSizeY = this.gridSizeY < 1 ? 1 : this.gridSizeY;
            GUILayout.EndHorizontal();

            // arrow offset label
            GUILayout.Label("Arrow Offset");

            // arrow offset
            GUILayout.BeginHorizontal();
            GUILayout.Label("X:");
            this.vecArrowXOffset = EditorGUILayout.IntField(this.vecArrowXOffset);
            GUILayout.Label("Y:");
            this.vecArrowYOffset = EditorGUILayout.IntField(this.vecArrowYOffset);
            GUILayout.EndHorizontal();

            // dimension offset label
            GUILayout.Label("Dimensions Offset");

            // dimension offset
            GUILayout.BeginHorizontal();
            GUILayout.Label("X:");
            this.vecDimensionXOffset = EditorGUILayout.IntField(this.vecDimensionXOffset);
            GUILayout.Label("Y:");
            this.vecDimensionYOffset = EditorGUILayout.IntField(this.vecDimensionYOffset);
            GUILayout.EndHorizontal();

            // angle list
            this.angleCount = EditorGUILayout.IntField("Angle Count", this.angleCount);
            this.angleCount = this.angleCount < 0 ? 0 : this.angleCount;
            this.angleCount = this.angleCount > 359 ? 359 : this.angleCount;

            this.angleStep = EditorGUILayout.FloatField("Angle Step", this.angleStep);
            this.angleStep = this.angleStep < 1 ? 1 : this.angleStep;
            this.angleStep = this.angleStep > 359 ? 359 : this.angleStep;

            this.startAngle = EditorGUILayout.FloatField("Start Angle", this.startAngle);
            this.startAngle = this.startAngle < -359 ? -359 : this.startAngle;
            this.startAngle = this.startAngle > 359 ? 359 : this.startAngle;

            this.angleTextOffset = EditorGUILayout.IntField("Angle Text Offset", this.angleTextOffset);
            this.angleTextOffset = this.angleTextOffset < -359 ? -359 : this.angleTextOffset;
            this.angleTextOffset = this.angleTextOffset > 359 ? 359 : this.angleTextOffset;

            this.angleDistance = EditorGUILayout.IntField("Angle Distance", this.angleDistance);
            this.angleDistance = this.angleDistance < 0 ? 0 : this.angleDistance;
            this.angleDistance = this.angleDistance > 5000 ? 5000 : this.angleDistance;

            this.angleTextDistance = EditorGUILayout.IntField("Angle Text Distance", this.angleTextDistance);
            this.angleTextDistance = this.angleTextDistance < 0 ? 0 : this.angleTextDistance;
            this.angleTextDistance = this.angleTextDistance > 5000 ? 5000 : this.angleTextDistance;

            // angle offset label
            GUILayout.Label("Angle Origin");

            // angle offset
            GUILayout.BeginHorizontal();
            GUILayout.Label("X:");
            this.angleOriginX = EditorGUILayout.IntField(this.angleOriginX);
            GUILayout.Label("Y:");
            this.angleOriginY = EditorGUILayout.IntField(this.angleOriginY);
            GUILayout.EndHorizontal();

            // create texture names text box
            GUILayout.BeginVertical();
            GUILayout.Label("Labels");

            this.txtNames = GUILayout.TextArea(this.txtNames);
            GUILayout.EndVertical();

            // add generate button
            if (GUILayout.Button("Preview"))
            {
                this.GenerateTextures();
                Selection.objects = this.previewItems.Select(x => x.Texture).ToArray();
            }

            // add generate button
            if (GUILayout.Button("Generate"))
            {
                this.GenerateClick();
            }

            // preview panel for textures
            this.pnlPreview = GUILayout.BeginScrollView(this.pnlPreview);
            GUILayout.EndScrollView();

            GUILayout.EndVertical();
            GUILayout.EndScrollView();
        }

        private Texture2D GenerateTexture(string name)
        {
            var size = this.selectedSize;
            var bmp = new Texture2D(size, size);

            // Color32 col;
            switch (this.cbType)
            {
                case 0: //"Square":
                    // fill
                    this.FillRectangle(bmp,0, 0, bmp.width, bmp.height, this.fillColor);

                    // border
                    for (var i = 0; i < this.borderSize; i++)
                    {
                        this.DrawRectangle(bmp,i, i, size - (i * 2), size - (i * 2), this.borderColor);
                    }

                    break;

                case 1: //"Circle":
                    // fill
                    this.FillEllipse(bmp,0, 0, size, size, this.fillColor);

                    // border
                    for (var i = 0; i < this.borderSize; i++)
                    {
                        this.DrawEllipse(bmp,i, i, size - (i * 2), size - (i * 2), this.borderColor);
                    }

                    break;
            }

            // grid
            if (this.chkGrid)
            {
                for (var i = this.gridSizeX; i < bmp.width; i += this.gridSizeX)
                {
                    this.DrawLine(bmp, i, 0, i, size, this.gridColor);
                }

                for (var i = this.gridSizeY; i < bmp.height; i += this.gridSizeY)
                {
                    this.DrawLine(bmp, 0, i, size, i, this.gridColor);
                }
            }

            // angles
            //  col = this.angleColor;
            //  pen = new Pen(dColor.FromArgb(col.a, col.r, col.g, col.b));
            //  col = this.angleTextColor;
            //  brush = new SolidBrush(dColor.FromArgb(col.a, col.r, col.g, col.b));
            var angle = this.startAngle;
            for (var i = 0; i < this.angleCount; i++)
            {
                var pos = new Vector2(this.angleOriginX, this.angleOriginY);
                var endPos = new Vector2((float)Math.Cos(Math.PI / 180 * angle), (float)Math.Sin(Math.PI / 180 * angle));
                endPos.Normalize();
                endPos *= this.angleDistance;
                endPos += pos;
                this.DrawLine(bmp, (int)pos.x, (int)pos.y, (int)endPos.x, (int)endPos.y, this.angleColor);

                if (this.chkAngleText && this.cbAngleFont)
                {
                    // draw angle text

                    //    var fnt = new Font((string)this.cbAngleFont.Items[this.cbAngleFont.SelectedIndex], this.angleFontSize.Value);
                    //  var text = (angle + this.angleTextOffset).ToString() + (this.chkAngleSymbol ? "�" : string.Empty);


                    //endPos = new Vector2((float)Math.Cos(Math.PI / 180 * angle), (float)Math.Sin(Math.PI / 180 * angle));
                    //endPos.Normalize();
                    //endPos *= this.angleTextDistance;
                    //endPos += pos;

                    // bmp.DrawString(text, fnt, brush, endPos.x - (txtSize.Width / 2), endPos.y - (txtSize.Height / 2));
                }

                angle += this.angleStep;
            }


            if (this.chkDimensions && this.dimensionFont)
            {
                // Dimensions
                //   font = new Font((string)this.dimensionFont.Items[this.dimensionFont.SelectedIndex], this.dimensionFontSize.Value);
                //  var dimentionString = string.Format("{0}x{0}", size);

                // var dimentionStringSize = gfx.MeasureString(dimentionString, font);
                // col = this.dimensionTextColor;
                //  brush = new SolidBrush(dColor.FromArgb(col.a, col.r, col.g, col.b));
                //   bmp.DrawString(dimentionString, font, brush, borderSize + 4 + this.vecDimensionXOffset.Value, borderSize + 4 + this.vecDimensionYOffset.Value);
            }

            // label
            if (this.cbLabelFont)
            {
                //  font = new Font((string)this.cbLabelFont.Items[this.cbLabelFont.SelectedIndex], this.labelFontSize.Value);
                // var labelStringSize = this.cbLabelFont.MeasureString(name, this.labelFontSize, FontStyle.Normal);
                //var color = this.textColor;
                // var solidBrush = new SolidBrush(dColor.FromArgb(color.a, color.r, color.g, color.b));
                //  bmp.DrawString(this.cbLabelFont, name, size / 2f - labelStringSize.width / 2, size / 2f - labelStringSize.height / 2);
            }

            // orientation arrows
            if (this.chkArrows)
            {
                //  color = this.arrowColor;
                // pen = new Pen(dColor.FromArgb(color.a, color.r, color.g, color.b));
                // pen.EndCap = LineCap.ArrowAnchor;
                if (this.vecArrowXOffset != 0 | this.vecArrowYOffset != 0)
                {
                    this.DrawLine(bmp, this.vecArrowXOffset, this.vecArrowYOffset, this.vecArrowXOffset, this.vecArrowYOffset + (size / 4), this.arrowColor);
                    this.DrawLine(bmp, this.vecArrowXOffset, this.vecArrowYOffset, this.vecArrowXOffset + (size / 4), this.vecArrowYOffset, this.arrowColor);
                }
                else
                {
                    this.DrawLine(bmp, this.borderSize + 4, this.borderSize + 4 + this.dimensionFontSize, this.borderSize + 4, this.borderSize + 4 + this.dimensionFontSize + (size / 4), this.arrowColor);
                    this.DrawLine(bmp, this.borderSize + 4, this.borderSize + 4 + this.dimensionFontSize, this.borderSize + 4 + (size / 4), this.borderSize + 4 + this.dimensionFontSize, this.arrowColor);
                }
            }

            return bmp;
        }

        private void GenerateClick()
        {
            var outputPath = this.outputFolder.OutputPath;
            outputPath = outputPath.Trim();
            if (string.IsNullOrEmpty(outputPath))
            {
                Debug.LogError("No output folder specified!");
            }

            this.GenerateTextures();

            // save to output folder
            foreach (var item in this.previewItems)
            {
                if (!Directory.Exists(outputPath))
                {
                    Directory.CreateDirectory(outputPath);
                }

                var path = Path.Combine(Path.Combine("Assets", outputPath), string.IsNullOrEmpty(item.Name) ? "Empty" : item.Name);
                path = Path.ChangeExtension(path, ".png");

                File.WriteAllBytes(path, item.Texture.EncodeToPNG());
                AssetDatabase.ImportAsset(path, ImportAssetOptions.Default);
                Debug.Log(string.Format("Saved texture file {0}", path));
            }
        }

        private void GenerateTextures()
        {
            this.previewItems.Clear();
            var lines = this.txtNames.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length == 0)
            {
                lines = new[] { string.Empty };
            }

            var size = this.selectedSize;

            for (var index = 0; index < lines.Length; index++)
            {
                var image = new PreviewItem();
                this.previewItems.Add(image);
                image.Position = new Vector2(4, index * (size + 4));
            }

            this.currentImage = 0;

            foreach (var image in this.previewItems)
            {
                var label = lines[this.currentImage];
                image.Image = this.GenerateTexture(label.Replace(@"\n", "\n"));
                image.Texture = image.Image;
                image.Name = label.Trim();
                this.currentImage++;
            }
        }

        /// <summary>
        /// Shows the texture generator window.
        /// </summary>
        [MenuItem("Window/Codefarts/General Utilities/Texture Generator")]
        public static void ShowTextureGenerator()
        {
            var win = GetWindow<MeasurementTextureGenerator>();
            win.titleContent = new GUIContent("Texture Generator");
            win.Show();
            win.Focus();
        }
    }
}