namespace Codefarts.GeneralTools.Editor.Windows
{
    using System;

    using Codefarts.GeneralTools.Editor.Utilities;
    using Codefarts.Localization;

    using UnityEditor;

    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Provides a tool windows for converting the selected UI element to C# code.
    /// </summary>
    public class NewUIToCode : EditorWindow
    {

        /// <summary>
        /// Provides code wrapping definitions.
        /// </summary>
        public enum CodeWrapping
        {
            /// <summary>
            /// No code wrapping.
            /// </summary>
            None,

            /// <summary>
            /// Wrap code in a method.
            /// </summary>
            Method,

            /// <summary>
            /// Wrap code in a class.
            /// </summary>
            Class
        }

        /// <summary>
        /// The generated code.
        /// </summary>
        private string generatedCode = string.Empty;

        /// <summary>
        /// The scroll position.
        /// </summary>
        private Vector2 scroll;

        /// <summary>
        /// The type of wrapping code if any.
        /// </summary>
        private CodeWrapping wrapping = CodeWrapping.None;

        private string methodName = string.Empty;

        private string className = string.Empty;

        private bool buildChildren = true;

        private bool includeParents = true;

        //  private int nextCustomName;

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            GUILayout.BeginVertical();
            this.DrawHeader();
            this.DrawPreview();
            GUILayout.EndVertical();
        }

        private void DrawPreview()
        {
            this.scroll = GUILayout.BeginScrollView(this.scroll, false, false);
            this.generatedCode = GUILayout.TextArea(this.generatedCode, int.MaxValue);
            GUILayout.EndScrollView();
        }

        private void DrawHeader()
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Copy"))
            {
                ClipboardHelper.Clipboard = this.generatedCode;
            }

            this.wrapping = (CodeWrapping)EditorGUILayout.EnumPopup(this.wrapping, GUILayout.MaxWidth(128));

            switch (this.wrapping)
            {
                case CodeWrapping.None:
                    // do nothing
                    break;

                case CodeWrapping.Method:
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Method Name", GUILayout.ExpandWidth(false));
                    this.methodName = GUILayout.TextField(this.methodName, GUILayout.Width(128));
                    GUILayout.EndHorizontal();
                    break;

                case CodeWrapping.Class:
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Method Name", GUILayout.ExpandWidth(false));
                    this.methodName = GUILayout.TextField(this.methodName, GUILayout.Width(128));
                    GUILayout.Label("Class Name", GUILayout.ExpandWidth(false));
                    this.className = GUILayout.TextField(this.className, GUILayout.Width(128));
                    GUILayout.EndHorizontal();
                    break;
            }

            GUILayout.FlexibleSpace();

            this.buildChildren = GUILayout.Toggle(this.buildChildren, "Include Children");
            this.includeParents = GUILayout.Toggle(this.includeParents, "Include Parents");
            if (GUILayout.Button("Create"))
            {
                this.CreateCSharpCode();
            }
            GUILayout.EndHorizontal();
        }

        private void CreateCSharpCode()
        {
            var selections = Selection.transforms;
            this.generatedCode = string.Empty;

            // this.nextCustomName = 0;

            switch (wrapping)
            {
                case CodeWrapping.Method:
                    this.generatedCode = string.Format("public void {0}()\r\n{{\r\n", this.methodName);
                    break;

                case CodeWrapping.Class:
                    this.generatedCode = string.Format("public class {0}\r\n{{\r\n", this.className);
                    this.generatedCode += string.Format("public void {0}()\r\n{{\r\n", this.methodName);
                    break;
            }

            foreach (var selection in selections)
            {
                this.BuildCode(selection, null, this.buildChildren);
            }

            switch (wrapping)
            {
                case CodeWrapping.Method:
                    this.generatedCode += "}\r\n";
                    break;

                case CodeWrapping.Class:
                    this.generatedCode += "}\r\n}\r\n";
                    break;
            }
        }

        private void BuildCode(Transform transform, Transform parent, bool includeChildren)
        {
            var addCode = new Func<string, string>(c => string.IsNullOrEmpty(c) ? string.Empty : c + "\r\n\r\n");
            string generatedName;

            this.generatedCode += addCode(this.BuildGameObject(transform, out generatedName)).Trim() + "\r\n" + (!this.includeParents ? "\r\n" : string.Empty);

            if (includeChildren)
            {
                // build childrend before parent so variable declarations for children are 
                // generated before parent declarations.
                for (var i = 0; i < transform.childCount; i++)
                {
                    this.BuildCode(transform.GetChild(i), transform, includeChildren);
                }
            }

            var parentName = transform.parent != null ? transform.parent.name.Replace(" ", "_") : null;
            if (this.includeParents && !string.IsNullOrEmpty(parentName))
            {
                this.generatedCode += string.Format("{0}.transform.SetParent({1}.transform, false);\r\n\r\n", generatedName, parentName);
            }

            this.generatedCode += addCode(this.BuildRectTransform(generatedName, transform));
            this.generatedCode += addCode(this.BuildCanvasRenderer(generatedName, transform));
            this.generatedCode += addCode(this.BuildImage(generatedName, transform));
            this.generatedCode += addCode(this.BuildButton(generatedName, transform));
            this.generatedCode += addCode(this.BuildText(generatedName, transform));
            this.generatedCode += addCode(this.BuildRawImage(generatedName, transform));
            this.generatedCode += addCode(this.BuildSlider(generatedName, transform));
            this.generatedCode += addCode(this.BuildScrollBar(generatedName, transform));
            this.generatedCode += addCode(this.BuildToggle(generatedName, transform));
            this.generatedCode += addCode(this.BuildInputField(generatedName, transform));
            this.generatedCode += addCode(this.BuildCanvasGroup(generatedName, transform));
            this.generatedCode += addCode(this.BuildHorizLayoutGroup(generatedName, transform));
            this.generatedCode += addCode(this.BuildCanvas(generatedName, transform));
            this.generatedCode += addCode(this.BuildCanvasScaler(generatedName, transform));
            this.generatedCode += addCode(this.BuildGraphicRayCaster(generatedName, transform));
            this.generatedCode += addCode(this.BuildLayoutElement(generatedName, transform));
            this.generatedCode += addCode(this.BuildContentSizeFitter(generatedName, transform));
            this.generatedCode += addCode(this.BuildAspectRatioFitter(generatedName, transform));
            this.generatedCode += addCode(this.BuildVertLayoutGroup(generatedName, transform));
            this.generatedCode += addCode(this.BuildGridLayoutGroup(generatedName, transform));
        }

        private string BuildGridLayoutGroup(string name, Transform item)
        {
            var gridLayoutGroup = item.GetComponent<GridLayoutGroup>();
            if (gridLayoutGroup == null)
            {
                return null;
            }

            var code = "// set grid layout group properties\r\n";
            code += string.Format("var {0}_GridLayoutGroup = {0}.AddComponent<GridLayoutGroup>();\r\n", name);
            code += string.Format("{0}_GridLayoutGroup.padding = new RectOffset({1}, {2}, {3}, {4});\r\n", name, gridLayoutGroup.padding.left, gridLayoutGroup.padding.top, gridLayoutGroup.padding.right, gridLayoutGroup.padding.bottom);
            code += string.Format("{0}_GridLayoutGroup.cellSize = new Vector2({1}f, {2}f);\r\n", name, gridLayoutGroup.cellSize);
            code += string.Format("{0}_GridLayoutGroup.spacing = new Vector2({1}f, {2}f);\r\n", name, gridLayoutGroup.spacing);
            code += string.Format("{0}_GridLayoutGroup.startCorner = Corner.{1};\r\n", name, gridLayoutGroup.startCorner);
            code += string.Format("{0}_GridLayoutGroup.startAxis = Axis.{1};\r\n", name, gridLayoutGroup.startAxis);
            code += string.Format("{0}_GridLayoutGroup.childAlignment = TextAnchor.{1};\r\n", name, gridLayoutGroup.childAlignment);
            code += string.Format("{0}_GridLayoutGroup.constraint = Constraint.{1};\r\n", name, gridLayoutGroup.constraint);
            code += string.Format("{0}_GridLayoutGroup.constraintCount = {1};\r\n", name, gridLayoutGroup.constraintCount);

            return code;
        }

        private string BuildVertLayoutGroup(string name, Transform item)
        {
            var verticalLayoutGroup = item.GetComponent<VerticalLayoutGroup>();
            if (verticalLayoutGroup == null)
            {
                return null;
            }

            var code = "// set vertical layout group properties\r\n";
            code += string.Format("var {0}_VerticalLayoutGroup = {0}.AddComponent<VerticalLayoutGroup>();\r\n", name);
            code += string.Format("{0}_VerticalLayoutGroup.padding = new RectOffset({1}, {2}, {3}, {4});\r\n", name, verticalLayoutGroup.padding.left, verticalLayoutGroup.padding.top, verticalLayoutGroup.padding.right, verticalLayoutGroup.padding.bottom);
            code += string.Format("{0}_VerticalLayoutGroup.spacing = {1}f;\r\n", name, verticalLayoutGroup.spacing);
            code += string.Format("{0}_VerticalLayoutGroup.childAlignment = TextAnchor.{1};\r\n", name, verticalLayoutGroup.childAlignment);
            code += string.Format("{0}_VerticalLayoutGroup.childForceExpandWidth = {1};\r\n", name, verticalLayoutGroup.childForceExpandWidth.ToString().ToLower());
            code += string.Format("{0}_VerticalLayoutGroup.childForceExpandHeight = {1};\r\n", name, verticalLayoutGroup.childForceExpandHeight.ToString().ToLower());

            return code;
        }

        private string BuildAspectRatioFitter(string name, Transform item)
        {
            var aspectRatioFitter = item.GetComponent<AspectRatioFitter>();
            if (aspectRatioFitter == null)
            {
                return null;
            }

            var code = "// set aspect ratio fitter properties\r\n";
            code += string.Format("var {0}_AspectRatioFitter = {0}.AddComponent<AspectRatioFitter>();\r\n", name);
            code += string.Format("{0}_AspectRatioFitter.aspectMode = AspectMode.{1};\r\n", name, aspectRatioFitter.aspectMode);
            code += string.Format("{0}_AspectRatioFitter.aspectRatio = {1}f;\r\n", name, aspectRatioFitter.aspectRatio);

            return code;
        }

        private string BuildContentSizeFitter(string name, Transform item)
        {
            var sizeFitter = item.GetComponent<ContentSizeFitter>();
            if (sizeFitter == null)
            {
                return null;
            }

            var code = "// set content size fitter properties\r\n";
            code += string.Format("var {0}_ContentSizeFitter = {0}.AddComponent<ContentSizeFitter>();\r\n", name);
            code += string.Format("{0}_ContentSizeFitter.horizontalFit = ContentSizeFitter.FitMode.{1};\r\n", name, sizeFitter.horizontalFit);
            code += string.Format("{0}_ContentSizeFitter.verticalFit = ContentSizeFitter.FitMode.{1};\r\n", name, sizeFitter.verticalFit);

            return code;
        }

        private string BuildLayoutElement(string name, Transform item)
        {
            var layoutElement = item.GetComponent<LayoutElement>();
            if (layoutElement == null)
            {
                return null;
            }

            var code = "// set layout element properties\r\n";
            code += string.Format("var {0}_LayoutElement = {0}.AddComponent<LayoutElement>();\r\n", name);
            code += string.Format("{0}_LayoutElement.ignoreLayout = {1};\r\n", name, layoutElement.ignoreLayout.ToString().ToLower());
            if (layoutElement.minWidth != 0)
            {
                code += string.Format("{0}_LayoutElement.minWidth = {1}f;\r\n", name, layoutElement.minWidth);
            }

            if (layoutElement.minHeight != 0)
            {
                code += string.Format("{0}_LayoutElement.minHeight = {1}f;\r\n", name, layoutElement.minHeight);
            }

            if (layoutElement.preferredWidth != 0)
            {
                code += string.Format("{0}_LayoutElement.preferredWidth = {1}f;\r\n", name, layoutElement.preferredWidth);
            }

            if (layoutElement.preferredHeight != 0)
            {
                code += string.Format("{0}_LayoutElement.preferredHeight = {1}f;\r\n", name, layoutElement.preferredHeight);
            }

            if (layoutElement.flexibleWidth != 0)
            {
                code += string.Format("{0}_LayoutElement.flexibleWidth = {1}f;\r\n", name, layoutElement.flexibleWidth);
            }

            if (layoutElement.flexibleHeight != 0)
            {
                code += string.Format("{0}_LayoutElement.flexibleHeight = {1}f;\r\n", name, layoutElement.flexibleHeight);
            }

            return code;
        }

        private string BuildGraphicRayCaster(string name, Transform item)
        {
            var graphicRaycaster = item.GetComponent<GraphicRaycaster>();
            if (graphicRaycaster == null)
            {
                return null;
            }

            var code = "// set canvas properties\r\n";
            code += string.Format("var {0}_GraphicRaycaster = {0}.AddComponent<GraphicRaycaster>();\r\n", name);
            code += string.Format("{0}_GraphicRaycaster.ignoreReversedGraphics = {1};\r\n", name, graphicRaycaster.ignoreReversedGraphics.ToString().ToLower());
            code += string.Format("{0}_GraphicRaycaster.blockingObjects = GraphicRaycaster.BlockingObjects.{1};", name, graphicRaycaster.blockingObjects);

            return code;
        }

        private string BuildCanvasScaler(string name, Transform item)
        {
            var canvasScaler = item.GetComponent<CanvasScaler>();
            if (canvasScaler == null)
            {
                return null;
            }

            var code = "// set canvas scaler properties\r\n";
            code += string.Format("var {0}_CanvasScaler = {0}.AddComponent<CanvasScaler>();\r\n", name);
            code += string.Format("{0}_CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.{1};\r\n", name, canvasScaler.uiScaleMode);
            code += string.Format("{0}_CanvasScaler.scaleFactor = {1}f;\r\n", name, canvasScaler.scaleFactor);
            code += string.Format("{0}_CanvasScaler.referencePixelsPerUnit = {1}f;", name, canvasScaler.referencePixelsPerUnit);

            return code;
        }

        private string BuildCanvas(string name, Transform item)
        {
            var canvasGroup = item.GetComponent<Canvas>();
            if (canvasGroup == null)
            {
                return null;
            }

            var code = "// set canvas properties\r\n";
            code += string.Format("var {0}_CanvasGroup = {0}.AddComponent<Canvas>();\r\n", name);
            //code += string.Format("{0}_CanvasGroup.renderOrder = {1};\r\n", name, canvasGroup.renderOrder);
            code += string.Format("{0}_CanvasGroup.sortingOrder = {1};\r\n", name, canvasGroup.sortingOrder);
            code += string.Format("{0}_CanvasGroup.sortingLayerName = \"{1}\";\r\n", name, canvasGroup.sortingLayerName);
            code += string.Format("{0}_CanvasGroup.sortingLayerID = {1};\r\n", name, canvasGroup.sortingLayerID);
            code += string.Format("{0}_CanvasGroup.renderMode = RenderMode.{1};\r\n", name, canvasGroup.renderMode);
            code += string.Format("{0}_CanvasGroup.planeDistance = {1};\r\n", name, canvasGroup.planeDistance);
            code += string.Format("{0}_CanvasGroup.pixelPerfect = {1};\r\n", name, canvasGroup.pixelPerfect.ToString().ToLower());
            code += string.Format("{0}_CanvasGroup.overrideSorting = {1};", name, canvasGroup.overrideSorting.ToString().ToLower());

            return code;
        }

        private string BuildHorizLayoutGroup(string name, Transform item)
        {
            var canvasGroup = item.GetComponent<HorizontalLayoutGroup>();
            if (canvasGroup == null)
            {
                return null;
            }

            var code = "// set horizontal layout group properties\r\n";
            code += string.Format("var {0}_HorizontalLayoutGroup = {0}.AddComponent<HorizontalLayoutGroup>();\r\n", name);
            code += string.Format("{0}_HorizontalLayoutGroup.padding = new RectOffset({1}, {2}, {3}, {4});\r\n", name, canvasGroup.padding.left, canvasGroup.padding.top, canvasGroup.padding.right, canvasGroup.padding.bottom);
            code += string.Format("{0}_HorizontalLayoutGroup.spacing = {1}f;\r\n", name, canvasGroup.spacing);
            code += string.Format("{0}_HorizontalLayoutGroup.childAlignment = TextAnchor.{1};\r\n", name, canvasGroup.childAlignment);
            code += string.Format("{0}_HorizontalLayoutGroup.childForceExpandWidth = {1};\r\n", name, canvasGroup.childForceExpandWidth.ToString().ToLower());
            code += string.Format("{0}_HorizontalLayoutGroup.childForceExpandHeight = {1};\r\n", name, canvasGroup.childForceExpandHeight.ToString().ToLower());

            return code;
        }

        private string BuildCanvasGroup(string name, Transform item)
        {
            var canvasGroup = item.GetComponent<CanvasGroup>();
            if (canvasGroup == null)
            {
                return null;
            }

            var code = "// set canvas group properties\r\n";
            code += string.Format("var {0}_CanvasGroup = {0}.AddComponent<CanvasGroup>();\r\n", name);
            code += string.Format("{0}_CanvasGroup.interactable = {1};\r\n", name, canvasGroup.interactable.ToString().ToLower());
            code += string.Format("{0}_CanvasGroup.blocksRaycasts = {1};\r\n", name, canvasGroup.blocksRaycasts.ToString().ToLower());
            code += string.Format("{0}_CanvasGroup.ignoreParentGroups = {1};\r\n", name, canvasGroup.ignoreParentGroups.ToString().ToLower());

            return code;
        }

        private string BuildInputField(string name, Transform item)
        {
            var inputField = item.GetComponent<InputField>();
            if (inputField == null)
            {
                return null;
            }

            var code = "// set input field properties\r\n";
            code += string.Format("var {0}_InputField = {0}.AddComponent<InputField>();\r\n", name);
            code += string.Format("{0}_InputField.interactable = {1};\r\n", name, inputField.interactable.ToString().ToLower());
            code += this.BuildTransitionCode(name + "_InputField", inputField.transition, inputField.colors, inputField.spriteState, inputField.animationTriggers);

            if (inputField.textComponent != null)
            {
                code += string.Format("{0}_InputField.textComponent = {1}_Text;\r\n", name, inputField.textComponent.name);
            }

            code += string.Format("{0}_InputField.characterLimit = {1};\r\n", name, inputField.characterLimit);
            code += string.Format("{0}_InputField.contentType = UnityEngine.UI.InputField.ContentType.{1};\r\n", name, inputField.contentType);
            code += string.Format("{0}_InputField.lineType = UnityEngine.UI.InputField.LineType.{1};\r\n", name, inputField.lineType);

            if (inputField.placeholder != null)
            {
                code += string.Format("{0}_InputField.placeholder = {1}_Text;\r\n", name, inputField.placeholder.name);
            }

            code += string.Format("{0}_InputField.caretBlinkRate = {1}f;\r\n", name, inputField.caretBlinkRate);
            var color = inputField.selectionColor;
            code += string.Format("{0}_InputField.selectionColor = new Color({1}f, {2}f, {3}f, {4}f);\r\n", name, color.r, color.g, color.b, color.a);
            code += string.Format("{0}_InputField.shouldHideMobileInput = {1};\r\n", name, inputField.shouldHideMobileInput.ToString().ToLower());

            return code;
        }

        private string BuildTransitionCode(string name, Selectable.Transition transition, ColorBlock colors, SpriteState spriteState, AnimationTriggers animationTriggers)
        {
            Color color;
            string code = string.Empty;
            switch (transition)
            {
                case Selectable.Transition.None:
                    break;

                case Selectable.Transition.ColorTint:
                    // code += string.Format("{0}.targetGraphic = UnityEngine.UI.Graphic.{1};\r\n", name, button.targetGraphic);
                    color = colors.normalColor;
                    code += string.Format("var {0}_colors = {0}.colors;\r\n", name);
                    code += string.Format("{0}_colors.normalColor = new Color({1}f, {2}f, {3}f, {4}f);\r\n", name, color.r, color.g, color.b, color.a);
                    color = colors.pressedColor;
                    code += string.Format("{0}_colors.pressedColor = new Color({1}f, {2}f, {3}f, {4}f);\r\n", name, color.r, color.g, color.b, color.a);
                    color = colors.highlightedColor;
                    code += string.Format("{0}_colors.highlightedColor = new Color({1}f, {2}f, {3}f, {4}f);\r\n", name, color.r, color.g, color.b, color.a);
                    color = colors.disabledColor;
                    code += string.Format("{0}_colors.disabledColor = new Color({1}f, {2}f, {3}f, {4}f);\r\n", name, color.r, color.g, color.b, color.a);
                    code += string.Format("{0}_colors.colorMultiplier = {1}f;\r\n", name, colors.colorMultiplier);
                    code += string.Format("{0}_colors.fadeDuration = {1}f;\r\n", name, colors.fadeDuration);
                    code += string.Format("{0}.colors = {0}_colors;\r\n", name);
                    break;

                case Selectable.Transition.SpriteSwap:
                    if (spriteState.highlightedSprite != null)
                    {
                        var path = AssetDatabase.GetAssetPath(spriteState.highlightedSprite);
                        if (path.StartsWith("Resources/"))
                        {
                            code += string.Format("{0}.spriteState.highlightedSprite = Resources.Load<Sprite>(\"{1}\");\r\n", name, path);
                        }
                    }

                    if (spriteState.pressedSprite != null)
                    {
                        var path = AssetDatabase.GetAssetPath(spriteState.pressedSprite);

                        if (path.StartsWith("Resources/"))
                        {
                            code += string.Format("{0}.spriteState.pressedSprite = Resources.Load<Sprite>(\"{1}\");\r\n", name, path);
                        }
                    }

                    if (spriteState.disabledSprite != null)
                    {
                        var path = AssetDatabase.GetAssetPath(spriteState.disabledSprite);
                        if (path.StartsWith("Resources/"))
                        {
                            code += string.Format("{0}.spriteState.disabledSprite = Resources.Load<Sprite>(\"{1}\");\r\n", name, path);
                        }
                    }
                    break;

                case Selectable.Transition.Animation:
                    code += string.Format("{0}.animationTriggers.normalTrigger = {1};\r\n", name, animationTriggers.normalTrigger);
                    code += string.Format("{0}.animationTriggers.highlightedTrigger = {1};\r\n", name, animationTriggers.highlightedTrigger);
                    code += string.Format("{0}.animationTriggers.pressedTrigger = {1};\r\n", name, animationTriggers.pressedTrigger);
                    code += string.Format("{0}.animationTriggers.disabledTrigger = {1};\r\n", name, animationTriggers.disabledTrigger);
                    break;
            }

            return code;
        }

        private string BuildToggle(string name, Transform item)
        {
            var toggle = item.GetComponent<Toggle>();
            if (toggle == null)
            {
                return null;
            }

            var code = "// set toggle properties\r\n";
            code += string.Format("var {0}_Toggle = {0}.AddComponent<Toggle>();\r\n", name);
            code += string.Format("{0}_Toggle.interactable = {1};\r\n", name, toggle.interactable.ToString().ToLower());
            code += this.BuildTransitionCode(name + "_Toggle", toggle.transition, toggle.colors, toggle.spriteState, toggle.animationTriggers);
            code += string.Format("{0}_Toggle.isOn = {1};\r\n", name, toggle.isOn.ToString().ToLower());
            code += string.Format("{0}_Toggle.toggleTransition = UnityEngine.UI.Toggle.ToggleTransition.{1};\r\n", name, toggle.toggleTransition);
            if (toggle.graphic != null)
            {
                code += string.Format("{0}_Toggle.graphic = {1}_Image;\r\n", name, toggle.graphic.name);
            }

            if (toggle.group != null)
            {
                code += string.Format("{0}_Toggle.group = {1};\r\n", name, toggle.group.name);
            }

            return code;
        }

        private string BuildScrollBar(string name, Transform item)
        {
            var scrollbar = item.GetComponent<Scrollbar>();
            if (scrollbar == null)
            {
                return null;
            }

            var code = "// set scrollbar properties\r\n";
            code += string.Format("var {0}_Scrollbar = {0}.AddComponent<Scrollbar>();\r\n", name);
            code += string.Format("{0}_Scrollbar.interactable = {1};\r\n", name, scrollbar.interactable.ToString().ToLower());
            code += this.BuildTransitionCode(name + "_Scrollbar", scrollbar.transition, scrollbar.colors, scrollbar.spriteState, scrollbar.animationTriggers);

            if (scrollbar.handleRect != null)
            {
                code += string.Format("{0}_Scrollbar.handleRect = {1}_RectTransform;\r\n", name, scrollbar.handleRect.name);
            }
            code += string.Format("{0}_Scrollbar.direction = UnityEngine.UI.Scrollbar.Direction.{1};\r\n", name, scrollbar.direction);
            code += string.Format("{0}_Scrollbar.value = {1}f;\r\n", name, scrollbar.value);
            code += string.Format("{0}_Scrollbar.size = {1}f;\r\n", name, scrollbar.size);
            code += string.Format("{0}_Scrollbar.numberOfSteps = {1};\r\n", name, scrollbar.numberOfSteps);

            return code;
        }

        private string BuildSlider(string name, Transform item)
        {
            var slider = item.GetComponent<Slider>();
            if (slider == null)
            {
                return null;
            }

            var code = "// set slider properties\r\n";
            code += string.Format("var {0}_Slider = {0}.AddComponent<Slider>();\r\n", name);
            code += string.Format("{0}_Slider.interactable = {1};\r\n", name, slider.interactable.ToString().ToLower());
            code += this.BuildTransitionCode(name + "_Slider", slider.transition, slider.colors, slider.spriteState, slider.animationTriggers);

            if (slider.fillRect != null)
            {
                code += string.Format("{0}_Slider.fillRect = {1}_RectTransform;\r\n", name, slider.fillRect.name);
            }
            code += string.Format("{0}_Slider.direction = UnityEngine.UI.Slider.Direction.{1};\r\n", name, slider.direction);
            code += string.Format("{0}_Slider.minValue = {1}f;\r\n", name, slider.minValue);
            code += string.Format("{0}_Slider.maxValue = {1}f;\r\n", name, slider.maxValue);
            code += string.Format("{0}_Slider.wholeNumbers = {1};\r\n", name, slider.wholeNumbers.ToString().ToLower());
            code += string.Format("{0}_Slider.value = {1}f;\r\n", name, slider.value);

            return code;
        }

        private string BuildRawImage(string name, Transform item)
        {
            var image = item.GetComponent<RawImage>();
            if (image == null)
            {
                return null;
            }

            var code = "// add a RawImage component and set it's properties\r\n";
            code += string.Format("var {0}_RawImage = {0}.AddComponent<RawImage>();\r\n", name);

            if (image.texture != null)
            {
                var path = AssetDatabase.GetAssetPath(image.texture);
                if (path.StartsWith("Resources/"))
                {
                    code += string.Format("{0}_RawImage.texture = Resources.Load<Texture>(\"{1}\");\r\n", name, path);
                }
            }

            if (image.material != null)
            {
                var path = AssetDatabase.GetAssetPath(image.material);
                if (path.StartsWith("Resources/"))
                {
                    code += string.Format("{0}_RawImage.material = Resources.Load<Material>(\"{1}\");\r\n", name, path);
                }
            }

            code += string.Format("{0}_RawImage.color = new Color({1}f, {2}f, {3}f, {4}f);\r\n", name, image.color.r, image.color.g, image.color.b, image.color.a);
            code += string.Format("{0}_RawImage.uvRect = new Rect({1}f, {2}f, {3}f, {4}f);", name, image.uvRect.x, image.uvRect.y, image.uvRect.width, image.uvRect.height);

            return code;
        }

        private string BuildText(string name, Transform item)
        {
            var text = item.GetComponent<Text>();
            if (text == null)
            {
                return null;
            }

            var code = "// set text properties\r\n";
            code += string.Format("var {0}_Text = {0}.AddComponent<Text>();\r\n", name);
            code += string.Format("{0}_Text.text = \"{1}\";\r\n", name, text.text);
            if (text.font != null)
            {
                var path = AssetDatabase.GetAssetPath(text.font);
                if (path.StartsWith("Resources/"))
                {
                    code += string.Format("{0}_Text.font = Resources.Load<Font>(\"{1}\");\r\n", name, path);
                }
            }

            code += string.Format("{0}_Text.fontStyle = FontStyle.{1};\r\n", name, text.fontStyle);
            code += string.Format("{0}_Text.fontSize = {1};\r\n", name, text.fontSize);
            code += string.Format("{0}_Text.lineSpacing = {1};\r\n", name, text.lineSpacing);
            code += string.Format("{0}_Text.supportRichText = {1};\r\n", name, text.supportRichText.ToString().ToLower());
            code += string.Format("{0}_Text.alignment = TextAnchor.{1};\r\n", name, text.alignment);
            code += string.Format("{0}_Text.horizontalOverflow = HorizontalWrapMode.{1};\r\n", name, text.horizontalOverflow);
            code += string.Format("{0}_Text.verticalOverflow = VerticalWrapMode.{1};\r\n", name, text.verticalOverflow);
            code += string.Format("{0}_Text.resizeTextForBestFit = {1};\r\n", name, text.resizeTextForBestFit.ToString().ToLower());
            code += string.Format("{0}_Text.color = new Color({1}f, {2}f, {3}f, {4}f);\r\n", name, text.color.r, text.color.g, text.color.b, text.color.a);

            if (text.material != null)
            {
                var path = AssetDatabase.GetAssetPath(text.material);
                if (path.StartsWith("Resources/"))
                {
                    code += string.Format("{0}_Text.material = Resources.Load<Material>(\"{1}\");\r\n", name, path);
                }
            }
            return code;
        }

        private string BuildButton(string name, Transform item)
        {
            var button = item.GetComponent<Button>();
            if (button == null)
            {
                return null;
            }

            var code = "// set button properties\r\n";
            code += string.Format("var {0}_Button = {0}.AddComponent<Button>();\r\n", name);
            code += string.Format("{0}_Button.interactable = {1};\r\n", name, button.interactable.ToString().ToLower());
            code += this.BuildTransitionCode(name + "_Button", button.transition, button.colors, button.spriteState, button.animationTriggers);

            return code;
        }

        private string BuildImage(string name, Transform item)
        {
            var image = item.GetComponent<Image>();
            if (image == null)
            {
                return null;
            }

            var code = "// add a Image component and set it's properties\r\n";
            code += string.Format("var {0}_Image = {0}.AddComponent<Image>();\r\n", name);

            if (image.sprite != null)
            {
                var path = AssetDatabase.GetAssetPath(image.sprite);
                if (path.StartsWith("Resources/"))
                {
                    code += string.Format("{0}_Image.sprite = Resources.Load<Sprite>(\"{1}\");\r\n", name, path);
                }
            }

            if (image.material != null)
            {
                var path = AssetDatabase.GetAssetPath(image.material);
                if (path.StartsWith("Resources/"))
                {
                    code += string.Format("{0}_Image.material = Resources.Load<Material>(\"{1}\");\r\n", name, path);
                }
            }

            code += string.Format("{0}_Image.color = new Color({1}f, {2}f, {3}f, {4}f);\r\n", name, image.color.r, image.color.g, image.color.b, image.color.a);
            code += string.Format("{0}_Image.type = UnityEngine.UI.Image.Type.{1};\r\n", name, image.type);
            switch (image.type)
            {
                case Image.Type.Simple:
                    code += string.Format("{0}_Image.preserveAspect = {1};", name, image.preserveAspect.ToString().ToLower());
                    break;

                case Image.Type.Sliced:
                case Image.Type.Tiled:
                    code += string.Format("{0}_Image.fillCenter = {1};", name, image.fillCenter.ToString().ToLower());
                    break;

                case Image.Type.Filled:
                    code += string.Format("{0}_Image.fillMethod = UnityEngine.UI.Image.FillMethod.{1};\r\n", name, image.fillMethod);

                    switch (image.fillMethod)
                    {
                        case Image.FillMethod.Horizontal:
                        case Image.FillMethod.Vertical:
                            code += string.Format("{0}_Image.fillOrigin = {1};\r\n", name, image.fillOrigin);
                            code += string.Format("{0}_Image.fillAmount = {1}f;\r\n", name, image.fillAmount);
                            code += string.Format("{0}_Image.preserveAspect = {1};", name, image.preserveAspect.ToString().ToLower());
                            break;

                        case Image.FillMethod.Radial90:
                        case Image.FillMethod.Radial180:
                        case Image.FillMethod.Radial360:
                            code += string.Format("{0}_Image.fillOrigin = {1};\r\n", name, image.fillOrigin);
                            code += string.Format("{0}_Image.fillAmount = {1}f;\r\n", name, image.fillAmount);
                            code += string.Format("{0}_Image.preserveAspect = {1};\r\n", name, image.preserveAspect.ToString().ToLower());
                            code += string.Format("{0}_Image.fillClockwise = {1};", name, image.fillClockwise.ToString().ToLower());
                            break;
                    }

                    break;
            }

            return code;
        }

        private string BuildGameObject(Transform item, out string generatedName)
        {
            var name = string.IsNullOrEmpty(item.name.Trim()) ? this.GetUniqueName(item) : item.name.Replace(" ", "_");
            generatedName = name;//+ "GameObject";
            var code = string.Format("var {0} = new GameObject(\"{0}\");", name);
            //if (!string.IsNullOrEmpty(parent))
            //{
            //    code += string.Format("\r\n{0}.transform.SetParent({1}.transform, false);", name, parent);
            //}

            return code;
        }

        private string BuildCanvasRenderer(string name, Transform item)
        {
            var rect = item.GetComponent<CanvasRenderer>();
            if (rect == null)
            {
                return null;
            }

            var code = "// add a CanvasRenderer component\r\n";
            code += string.Format("var {0}_CanvasRenderer = {0}.AddComponent<CanvasRenderer>();", name);
            return code;
        }

        private string BuildRectTransform(string name, Transform item)
        {
            var rect = item.GetComponent<RectTransform>();
            if (rect == null)
            {
                return null;
            }

            var code = "// set rect transform properties\r\n";
            code += string.Format("var {0}_RectTransform = {0}.AddComponent<RectTransform>();\r\n", name);
            code += string.Format("{0}_RectTransform.pivot = new Vector2({1}f, {2}f);\r\n", name, rect.pivot.x, rect.pivot.y);
            code += string.Format("{0}_RectTransform.anchorMax = new Vector2({1}f, {2}f);\r\n", name, rect.anchorMax.x, rect.anchorMax.y);
            code += string.Format("{0}_RectTransform.anchorMin = new Vector2({1}f, {2}f);\r\n", name, rect.anchorMin.x, rect.anchorMin.y);
            code += string.Format("{0}_RectTransform.offsetMax = new Vector2({1}f, {2}f);\r\n", name, rect.offsetMax.x, rect.offsetMax.y);
            code += string.Format("{0}_RectTransform.offsetMin = new Vector2({1}f, {2}f);\r\n", name, rect.offsetMin.x, rect.offsetMin.y);
            code += string.Format("{0}_RectTransform.sizeDelta = new Vector2({1}f, {2}f);", name, rect.sizeDelta.x, rect.sizeDelta.y);
            return code;
        }

        private string GetUniqueName(Transform item)
        {
            // this.nextCustomName++;
            return "unnamed" + item.GetInstanceID().ToString();// this.nextCustomName;
        }

        /// <summary>
        /// Provides unity menu item to open the window.
        /// </summary>
        [MenuItem("Window/Codefarts/General Utilities/NewUI To Code")]
        private static void ShowWindow()
        {
            // get the window, show it, and hand it focus
            var local = LocalizationManager.Instance;
            GetWindow<NewUIToCode>(local.Get("NewUIToCode")).Show();
        }
    }
}


