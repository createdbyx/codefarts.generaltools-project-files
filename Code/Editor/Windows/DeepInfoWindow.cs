// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.Editor.Windows
{
    using System;
    using System.Collections.Generic;

    using Codefarts.GeneralTools.Code;
    using Codefarts.GeneralTools.TypeVisualizers;
    using Codefarts.Localization;
    using Codefarts.UIControls;
    using Codefarts.UIControls.Factories;

    using UnityEditor;

    using UnityEngine;

    using Object = UnityEngine.Object;

    /// <summary>
    /// Provides a tool windows for getting detailed information about various assets.
    /// </summary>
    public class DeepInfoWindow : EditorWindow
    {
        private bool autoUpdate;

        private struct SelectionModel
        {
            public object Item;

            public Type Type;
        }

        private List<SelectionModel> selectionList = new List<SelectionModel>();

        private int selectionIndex = -1;

        private Vector2 selectionListScroll;
        private Vector2 visualizerScroll;
        private RecompileClass recompile;

        private GUIStyle customBox;

        private bool selectionManager;

        private class RecompileClass
        {
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            // check if recompile variable is null
            if (this.recompile == null)
            {
                // if yes assume recompile then create a reference to a recompile class
                this.recompile = new RecompileClass();

                // do what you need to do here after detecting a recompile 
                this.selectionList = new List<SelectionModel>();
                this.selectionIndex = -1;

                if (this.autoUpdate)
                {
                    this.UpdateSelectionList();
                    this.SetCurrentVisualizer();
                }
            }

            if (this.customBox == null)
            {
                this.customBox = new GUIStyle(GUI.skin.box);
                this.customBox.alignment = TextAnchor.MiddleLeft;
                this.customBox.stretchWidth = true;
                this.customBox.normal.textColor = GUI.skin.label.normal.textColor;
                this.customBox.active.textColor = GUI.skin.label.active.textColor;
                this.customBox.wordWrap = false;
            }

            GUILayout.BeginVertical(GUI.skin.box);
            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            this.SideBar();
            this.DrawInfo();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            if (this.autoUpdate)
            {
                this.Repaint();
            }
        }

        private ControlVisualizerBase currentVisualizer;

        private object lastSelectedItem;

        private void DrawInfo()
        {
            GUILayout.BeginVertical();
            this.visualizerScroll = GUILayout.BeginScrollView(this.visualizerScroll, false, false);

            if (this.currentVisualizer == null)
            {
                GUILayout.Label("No Visualizer!");
            }
            else
            {
                var rect = GUILayoutUtility.GetRect(1, this.position.width, 1, this.position.height);
                this.currentVisualizer.Location = rect.position;
                this.currentVisualizer.Size = rect.size;
                UIControlsFactory.DefaultManager.Renderer.DrawControl(this.currentVisualizer, Time.deltaTime, Time.time);
            }

            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }

        private void SideBar()
        {
            var local = LocalizationManager.Instance;
            GUILayout.BeginVertical(GUI.skin.box, GUILayout.MaxWidth(256));

            // draw update controls
            GUILayout.BeginHorizontal();
            this.autoUpdate = GUILayout.Toggle(this.autoUpdate, local.Get("AutoUpdate"));
            var show = GUILayout.Toggle(this.selectionManager, local.Get("SelectionManager"));
            if (show != this.selectionManager)
            {
                this.selectionManager = show;
                this.UpdateSelectionList();
            }

            GUILayout.EndHorizontal();

            if (!this.autoUpdate)
            {
                if (GUILayout.Button(local.Get("Update"), GUILayout.MaxWidth(128)))
                {
                    this.UpdateSelectionList();
                }
            }

            // check to auto update the list 
            if (this.autoUpdate)
            {
                this.UpdateSelectionList();
            }

            // draw selection information
            var selection = Selection.activeObject;
            if (selection == null)
            {
                GUILayout.Label(local.Get("NoSelection"));
            }
            else
            {
                this.DrawSelectionList();
            }

            GUILayout.EndVertical();
        }

        private void DrawSelectionList()
        {
            var maxWidth = Math.Max(this.position.width * 0.25f, 256);
            GUILayout.BeginVertical(GUI.skin.box, GUILayout.Width(maxWidth));
            this.selectionListScroll = GUILayout.BeginScrollView(this.selectionListScroll, false, false);

            for (var i = 0; i < this.selectionList.Count; i++)
            {
                var item = this.selectionList[i];
                string itemName;
                itemName = string.Format("Type: {0}", item.Type.Name);
                if (item.Item is Object)
                {
                    var model = item.Item as Object;
                    itemName += string.Format(" Name: {0}", string.IsNullOrEmpty(model.name) ? "<noname>" : model.name);
                }

                var sameIndex = this.selectionIndex == i;
                var style = sameIndex ? this.customBox : GUI.skin.label;

                if (GUILayout.Button(itemName, style, GUILayout.ExpandWidth(true)) && this.selectionIndex != i)
                {
                    this.SetIndex(i);
                }
            }

            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }

        private void SetCurrentVisualizer()
        {
            if (this.selectionList.Count == 0)
            {
                return;
            }

            // get the selection type
            var model = this.selectionList[this.selectionIndex];

            // get the visualizer for the type
            var visualizer = TypeVisualizerManager.Instance.GetVisualizer(model.Type.FullName);
            if (visualizer != null)
            {
                this.currentVisualizer = visualizer.CreateControl();
                this.currentVisualizer.SetData(model.Item);
            }
            else
            {
                this.currentVisualizer = null;
            }
        }

        private void UpdateSelectionList()
        {
            this.selectionList.Clear();
            foreach (var item in this.selectionManager ? SelectionManager.Instance.GetSelections() : Selection.objects)
            {
                this.selectionList.Add(new SelectionModel() { Item = item, Type = item.GetType() });
            }

            var newIndex = this.selectionIndex;
            if (this.selectionIndex > this.selectionList.Count - 1)
            {
                newIndex = this.selectionList.Count - 1;
            }

            if (this.selectionList.Count > 0 && this.selectionIndex == -1)
            {
                newIndex = 0;
            }

            this.SetIndex(newIndex);
        }

        private void SetIndex(int value)
        {
            var changed = value != this.selectionIndex;

            this.selectionIndex = value;
            if (this.selectionIndex == -1)
            {
                this.lastSelectedItem = null;
                return;
            }

            var item = this.selectionList[this.selectionIndex].Item;
            if (this.lastSelectedItem != item || changed)
            {
                this.SetCurrentVisualizer();
            }

            this.lastSelectedItem = item;
        }

        /// <summary>
        /// Provides unity menu item to open the window.
        /// </summary>
        [MenuItem("Window/Codefarts/General Utilities/Deep Info")]
        private static void ShowWindow()
        {
            // get the window, show it, and hand it focus
            var local = LocalizationManager.Instance;
            GetWindow<DeepInfoWindow>(local.Get("DeepInfo")).Show();
        }
    }
}