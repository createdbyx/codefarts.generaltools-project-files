namespace Codefarts.GeneralTools.Editor.Windows
{
    using System;
    using System.IO;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides an immediate window where the user can input C# code and run it directly from within the unity editor.
    /// </summary>
    public class NewProjectWindow : EditorWindow
    {
        private string parentFolder = string.Empty;

        private bool[] createFolders;

        private string folderName = string.Empty;

        private string[] folderNames;

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            this.minSize = new Vector2(400, 400);

            this.folderNames = new[]
                                   {
                                       "Scripts",
                                       "Scenes",
                                       "Materials",
                                       "Textures",
                                       "Animations",
                                       "Resources",
                                       "Prefabs",
                                       "Models",
                                   };
            this.createFolders = new bool[this.folderNames.Length];
        }

        /// <summary>
        /// Called by unity to draw the window ui.
        /// </summary>
        public void OnGUI()
        {
            GUILayout.BeginVertical();

            this.DrawParentFolder();
            GUILayout.Space(4);
            this.DrawFolderName();
            GUILayout.Space(4);
            this.DrawFolderOptions();
            GUILayout.FlexibleSpace();
            this.DrawLowerCreateButton();

            GUILayout.EndVertical();
        }

        private void DrawFolderOptions()
        {
            for (var i = 0; i < this.folderNames.Length; i++)
            {
                this.createFolders[i] = GUILayout.Toggle(this.createFolders[i], this.folderNames[i], GUI.skin.button, GUILayout.MaxWidth(256));
            }
        }

        private void DrawLowerCreateButton()
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Create", GUILayout.Height(32), GUILayout.Width(128)))
            {
                this.DoCreate();
            }

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Close", GUILayout.Height(32), GUILayout.Width(128)))
            {
                this.Close();
            }

            GUILayout.EndHorizontal();
        }

        private void DoCreate()
        {
            var folder = this.folderName.Trim().TrimStart(new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
            if (string.IsNullOrEmpty(folder))
            {
                EditorUtility.DisplayDialog("Warning", "Folder name required", "Close");
                return;
            }

            var parent = this.parentFolder.TrimStart(new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
            var parentPath = Path.Combine(Application.dataPath, parent);
            var outPath = Path.Combine(parentPath, folder);

            Directory.CreateDirectory(outPath);
            for (var i = 0; i < this.folderNames.Length; i++)
            {
                if (!this.createFolders[i])
                {
                    continue;
                }

                var presetFolderName = this.folderNames[i];
                var path = Path.Combine(outPath, presetFolderName);
                Directory.CreateDirectory(path);
            }

            AssetDatabase.ImportAsset(outPath);
            AssetDatabase.Refresh();
        }

        private void DrawFolderName()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Folder Name", GUILayout.ExpandWidth(false));
            this.folderName = GUILayout.TextField(this.folderName);
            GUILayout.EndHorizontal();
        }

        private void DrawParentFolder()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Parent Folder", GUILayout.ExpandWidth(false));
            this.parentFolder = GUILayout.TextField(this.parentFolder, GUILayout.MinWidth(1));
            if (GUILayout.Button("...", GUILayout.MaxWidth(32)))
            {
                var selectedPath = EditorUtility.OpenFolderPanel("Folder", this.parentFolder, string.Empty);
                if (!string.IsNullOrEmpty(selectedPath))
                {
                    if (selectedPath.StartsWith(Application.dataPath, StringComparison.OrdinalIgnoreCase))
                    {
                        this.parentFolder = selectedPath.Substring(Application.dataPath.Length);
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Warning", "Must select a path within the project Assets folder", "Close");
                    }
                }
            }

            GUILayout.EndHorizontal();
        }

        [MenuItem("Window/Codefarts/General Utilities/New Project Setup")]
        public static void ShowWindow()
        {
            // get the window, show it, and hand it focus
            try
            {
                var window = GetWindow<NewProjectWindow>();
                window.titleContent = new GUIContent("New Project Setup");
                window.Show();
                window.Focus();
                window.Repaint();
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
    }
}