/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.GeneralTools.Editor.Windows
{
    using System;
    using System.CodeDom.Compiler;
    using System.IO;
    using System.Reflection;

    using Codefarts.GeneralTools.Code;

    using Microsoft.CSharp;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides an immediate window where the user can input C# code and run it directly from within the unity editor.
    /// </summary>
    public class ImmediateWindow : EditorWindow
    {
        // script text
        private string scriptText = string.Empty;

        // cache of last method we compiled so repeat executions only incur a single compilation
        private MethodInfo lastScriptMethod;

        // position of scroll view
        private Vector2 scrollPosition;

        private void ValidateScript()
        {
            try
            {
                // create and configure the code provider
                var codeProvider = new CSharpCodeProvider();
                var options = new CompilerParameters();
                options.GenerateInMemory = true;
                options.GenerateExecutable = false;

                // bring in system libraries
                options.ReferencedAssemblies.Add("System.dll");
                options.ReferencedAssemblies.Add("System.Core.dll");

                // bring in Unity assemblies
                //options.ReferencedAssemblies.Add(typeof(EditorWindow).Assembly.Location);
                //options.ReferencedAssemblies.Add(typeof(Transform).Assembly.Location);
                //options.ReferencedAssemblies.Add(typeof(ImmediateWindow).Assembly.Location);
                //options.ReferencedAssemblies.Add(typeof(DrawingHelpers).Assembly.Location);

                foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    options.ReferencedAssemblies.Add(assembly.Location);
                }

                // compile an assembly from our source code
                var result = codeProvider.CompileAssemblyFromSource(options, string.Format(scriptFormat, this.scriptText, this.namespaces));

                // log any errors we got
                if (result.Errors.Count > 0)
                {
                    foreach (CompilerError error in result.Errors)
                    {
                        // the magic -11 on the line is to compensate for usings and class wrapper around the user script code.
                        // subtracting 11 from it will give the user the line numbers in their code.
                        Debug.LogError(string.Format("Immediate Compiler Error ({0}): {1}", error.Line - 11, error.ErrorText));
                    }

                    this.lastScriptMethod = null;
                }

                // otherwise use reflection to pull out our action method so we can invoke it
                else
                {
                    var type = result.CompiledAssembly.GetType("ImmediateWindowCodeWrapper");
                    this.lastScriptMethod = type.GetMethod("PerformAction", BindingFlags.Public | BindingFlags.Static);
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex.ToString());
            }
        }


        private void RunScript()
        {
            // if we have a compiled method, invoke it
            this.ValidateScript();
            if (this.lastScriptMethod != null)
            {
                this.lastScriptMethod.Invoke(null, null);
            }
        }

        private void SaveScript()
        {
            var file = this.fileName;
            if (string.IsNullOrEmpty(file))
            {
                file = EditorUtility.SaveFilePanel("Save Snippet", string.Empty, string.Empty, "txt");
            }

            if (file.Length > 0)
            {
                File.WriteAllText(file, this.scriptText);
                this.fileName = file;
                this.titleContent = new GUIContent("Immediate - " + file);
            }
        }

        private void LoadScript()
        {
            var file = EditorUtility.OpenFilePanel("Load Snippet", string.Empty, "txt");
            if (file.Length > 0)
            {
                this.scriptText = File.ReadAllText(file);
                this.fileName = file;
                this.titleContent = new GUIContent("Immediate - " + file);
                GUIUtility.keyboardControl = 0;
                GUIUtility.hotControl = 0;
            }
        }

        /// <summary>
        /// Called by unity to draw the window ui.
        /// </summary>
        void OnGUI()
        {
            this.DrawToolBar();
            this.DrawCodeArea();
        }

        private void DrawToolBar()
        {
            GUILayout.BeginHorizontal();
            try
            {
                GUILayout.Label("Type", GUILayout.ExpandWidth(false));
                this.projectType = EditorGUILayout.Popup(this.projectType, new[] { "Program", "Expression" }, GUILayout.ExpandWidth(false));
                this.showUsing = GUILayout.Toggle(this.showUsing, "Namespaces", GUI.skin.button, GUILayout.ExpandWidth(false));

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Validate"))
                {
                    // if our script method needs compiling
                    this.ValidateScript();
                    if (this.lastScriptMethod != null)
                    {
                        Debug.Log("No script errors! :D");
                    }
                }

                GUILayout.Space(16);

                if (GUILayout.Button("Run"))
                {
                    this.RunScript();
                }

                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Save"))
                {
                    this.SaveScript();
                }

                if (GUILayout.Button("Load"))
                {
                    this.LoadScript();
                }
            }
            finally
            {
                GUILayout.EndHorizontal();
            }

            if (this.showUsing)
            {
                this.namespaces = GUILayout.TextArea(this.namespaces);
            }
        }

        private void DrawCodeArea()
        {
            // start the scroll view
            this.scrollPosition = EditorGUILayout.BeginScrollView(this.scrollPosition);

            // show the script field
            this.scriptText = EditorGUILayout.TextArea(this.scriptText, GUILayout.ExpandHeight(true));
            EditorGUILayout.EndScrollView();
        }

        [MenuItem("Window/Codefarts/General Utilities/Immediate")]
        public static void ShowImmediateWindow()
        {
            // get the window, show it, and hand it focus
            try
            {
                var window = GetWindow<ImmediateWindow>();
                window.titleContent = new GUIContent("Immediate");
                window.Show();
                window.Focus();
                window.Repaint();
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        // script we wrap around user entered code
        static readonly string scriptFormat = @"
                using UnityEngine;
                using UnityEditor;
                using System.Collections;
                using System.Collections.Generic;
                using System.Text;
                using System;
                {1}
                public static class ImmediateWindowCodeWrapper
                {{
                    public static void PerformAction()
                    {{
                        // user code goes here
                        {0};
                    }}
                }}";

        private int projectType;

        private bool showUsing;

        private string namespaces = string.Empty;

        private string fileName = string.Empty;
    }
}