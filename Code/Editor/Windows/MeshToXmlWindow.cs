// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Codefarts" file="MeshToXmlWindow.cs">
//   Copyright (c) 2012 Codefarts
//   //   All rights reserved.
//   //   contact@codefarts.com
//   //   http://www.codefarts.com
// </copyright>
// <summary>
//   Provides a window where you can select objects by typing in there Instance ID.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Codefarts.GeneralTools.Editor.Windows
{
    using Codefarts.GeneralTools.Editor.Controls;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides a window where you can select objects by typing in there Instance ID.
    /// </summary>
    public class MeshToXmlWindow : EditorWindow
    {
        #region Fields

        /// <summary>
        /// Holds the path to the filename that will be saved.
        /// </summary>
        private string fileName = string.Empty;

        /// <summary>
        /// Holds a reference to a <see cref="SelectOutputFolderControl"/> used to select a output folder.
        /// </summary>
        private SelectOutputFolderControl output = new SelectOutputFolderControl();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Shows the window.
        /// </summary>
        [MenuItem("Window/Codefarts/General Utilities/Mesh to Xml")]
        public static void ShowWindow()
        {
            // get the window, show it, and hand it focus
            var window = GetWindow<MeshToXmlWindow>();
            window.titleContent =new GUIContent( "Mesh to Xml");
            window.Show();
            window.Focus();
            window.Repaint();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called by unity to draw the window gui.
        /// </summary>
        private void OnGUI()
        {
            GUILayout.BeginVertical();
            this.output.Draw();

            GUILayout.BeginHorizontal();
            GUILayout.Label("File name", GUILayout.ExpandWidth(false));
            this.fileName = GUILayout.TextField(this.fileName);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Save", GUILayout.Width(64), GUILayout.Height(32)))
            {
                this.SaveMesh();
            }

            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
        }

        /// <summary>
        /// Saves the mesh to a file.
        /// </summary>
        private void SaveMesh()
        {
            // var transforms = Selection.GetTransforms(SelectionMode.Deep);
        }

        #endregion
    }
}