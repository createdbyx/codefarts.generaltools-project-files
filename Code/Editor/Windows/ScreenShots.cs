﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.GeneralTools.Editor
{
    using System.IO;

    using Codefarts.CoreProjectCode.Settings;
    using Codefarts.Localization;

    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// Provides screen shot related functionality.
    /// </summary>
    public class ScreenShots
    {
        /// <summary>
        /// Takes a screen shot.
        /// </summary>
        [MenuItem("Window/Codefarts/General Utilities/Take Screen Shot")]
        public static void TakeScreenShot()
        {
            // get settings
            var settings = SettingsManager.Instance;
            var path = settings.GetSetting(GlobalConstants.ScreenShotPath, "Assets");
            var fileName = settings.GetSetting(GlobalConstants.ScreenShotFileName, "ScreenShot.png");
            var console = settings.GetSetting(GlobalConstants.ScreenShotReportToConsole, true);
            var superSize = settings.GetSetting(GlobalConstants.ScreenShotSuperSize, 1);

            // get a unique file name
            var file = AssetDatabase.GenerateUniqueAssetPath(Path.Combine("Assets/", Path.Combine(path, fileName)));

            // take the screen shot
            Application.CaptureScreenshot(file, superSize);

            // check to report to console
            if (console)
            {
                Debug.Log(string.Format(LocalizationManager.Instance.Get("SavedScreenShot"), file));
            }

            AssetDatabase.ImportAsset(file, ImportAssetOptions.ForceUpdate);
            AssetDatabase.Refresh();
        }
    }
}
