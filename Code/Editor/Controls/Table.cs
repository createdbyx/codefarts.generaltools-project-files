/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.GeneralTools.Editor.Controls
{
    using System;

    using Codefarts.GeneralTools.Common;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides a table control for drawing grid tables.
    /// </summary>
    /// <typeparam name="T">The data type associated with the table.</typeparam>
    public class Table<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Table{T}"/> class.
        /// </summary>
        /// <param name="tableName">
        /// Sets the value of the <see cref="TableName"/> property.
        /// </param>
        public Table(string tableName)
            : this(tableName, null)
        {
            this.TableName = tableName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Table{T}"/> class.
        /// </summary>
        /// <param name="model">
        /// Sets the value of the <see cref="Model"/> property.
        /// </param>
        public Table(ITableModel<T> model)
            : this(string.Empty, model)
        {
            this.Model = model;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Table{T}"/> class.
        /// </summary>
        public Table()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Table{T}"/> class.
        /// </summary>
        /// <param name="tableName">
        /// Sets the value of the <see cref="TableName"/> property.
        /// </param>
        /// <param name="model">
        /// Sets the value of the <see cref="Model"/> property.
        /// </param>
        public Table(string tableName, ITableModel<T> model)
            : this()
        {
            this.TableName = tableName;
            this.Model = model;
        }

        /// <summary>
        /// Provides a event after a table cell has been drawn.
        /// </summary>
        public event EventHandler<TableDrawEventArgs<T>> AfterDrawCell;

        /// <summary>
        /// Provides a event after a table column has been drawn.
        /// </summary>
        public event EventHandler<TableDrawEventArgs<T>> AfterDrawColumn;

        /// <summary>
        /// Provides a event after a table row has been drawn.
        /// </summary>
        public event EventHandler<TableDrawEventArgs<T>> AfterDrawRow;

        /// <summary>
        /// Provides a event before a table cell has been drawn.
        /// </summary>
        public event EventHandler<TableDrawEventArgs<T>> BeforeDrawCell;

        /// <summary>
        /// Provides a event before a table column has been drawn.
        /// </summary>
        public event EventHandler<TableDrawEventArgs<T>> BeforeDrawColumn;

        /// <summary>
        /// Provides a event before a table row has been drawn.
        /// </summary>
        public event EventHandler<TableDrawEventArgs<T>> BeforeDrawRow;

        /// <summary>
        /// Gets or sets a value indicating whether or not the table should always be drawn even if the <see cref="Model"/> property has not been set.
        /// </summary>
        public bool AlwaysDraw { get; set; }

        /// <summary>
        /// Gets or sets the model associated with the table.
        /// </summary>
        public ITableModel<T> Model { get; set; }

        /// <summary>
        /// Gets or sets a value that determines the height for each row.
        /// </summary>
        public int RowHeight { get; set; }

        /// <summary>
        /// Gets or sets the name of the table that will be displayed at the top.
        /// </summary>
        /// <remarks>Used to draw the table name on top of the table to help identify it. If null or empty the table name will not be drawn.</remarks>
        public string TableName { get; set; }

        /// <summary>
        /// Draws the table.
        /// </summary>
        public void Draw()
        {
            // if there is no model don't draw.
            if (this.Model == null && !this.AlwaysDraw)
            {
                return;
            }

            GUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            this.DrawTableName();

            // if there is no model to draw just exit
            if (this.Model == null)
            {
                GUILayout.EndVertical();
                return;
            }

            // get row height options
            var options = new GUILayoutOption[0];
            if (this.RowHeight > 0)
            {
                options = new[] { GUILayout.Height(this.RowHeight), GUILayout.MaxHeight(this.RowHeight), GUILayout.MinHeight(this.RowHeight) };
            }

            // draw each column
            GUILayout.BeginHorizontal();
            for (var column = 0; column < this.Model.ColumnCount; column++)
            {
                // raise before draw column event
                this.DoBeforeDrawColumn(column);

                // get column display options
                var colOptions = new GUILayoutOption[0];
                var columnWidth = this.Model.GetColumnWidth(column);
                if (columnWidth > 0)
                {
                    colOptions = new[] { GUILayout.MaxWidth(columnWidth), GUILayout.MinWidth(columnWidth) };
                }

                // draw column header
                GUILayout.BeginVertical("box", colOptions);
                if (this.Model.UseHeaders)
                {
                    GUILayout.BeginHorizontal("box");
                    GUILayout.Label(this.Model.GetColumnName(column));
                    GUILayout.EndHorizontal();
                }

                // draw each row
                this.DoBeforeDrawRow(column);
                for (var row = 0; row < this.Model.RowCount; row++)
                {
                    // draw the cell
                    this.DrawCell(row, column, options);
                }

                // row has finished drawing
                this.DoAfterDrawRow(column);

                GUILayout.EndVertical();

                // column has finished drawing
                this.DoAfterDrawColumn(column);
            }

            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
        }

        /// <summary>
        /// Raises the <see cref="AfterDrawCell"/> event.
        /// </summary>
        /// <param name="row">
        /// The row of the cell that was drawn.
        /// </param>
        /// <param name="column">
        /// The column of the cell that was drawn.
        /// </param>
        private void DoAfterDrawCell(int row, int column)
        {
            if (this.AfterDrawCell != null)
            {
                this.AfterDrawCell(this, new TableDrawEventArgs<T> { Column = column, Row = row, Model = this.Model });
            }
        }

        /// <summary>
        /// Raises the <see cref="AfterDrawColumn"/> event.
        /// </summary>     
        /// <param name="column">
        /// The column that was drawn.
        /// </param>
        private void DoAfterDrawColumn(int column)
        {
            if (this.AfterDrawColumn != null)
            {
                this.AfterDrawColumn(this, new TableDrawEventArgs<T> { Column = column, Row = -1, Model = this.Model });
            }
        }

        /// <summary>
        /// Raises the <see cref="AfterDrawRow"/> event.
        /// </summary>     
        /// <param name="row">
        /// The row that was drawn.
        /// </param>
        private void DoAfterDrawRow(int row)
        {
            if (this.AfterDrawRow != null)
            {
                this.AfterDrawRow(this, new TableDrawEventArgs<T> { Column = -1, Row = row, Model = this.Model });
            }
        }

        /// <summary>
        /// Raises the <see cref="BeforeDrawCell"/> event.
        /// </summary>
        /// <param name="row">
        /// The row of the cell that will be drawn.
        /// </param>
        /// <param name="column">
        /// The column of the cell that will drawn.
        /// </param>
        private void DoBeforeDrawCell(int row, int column)
        {
            if (this.BeforeDrawCell != null)
            {
                this.BeforeDrawCell(this, new TableDrawEventArgs<T> { Column = column, Row = row, Model = this.Model });
            }
        }

        /// <summary>
        /// Raises the <see cref="BeforeDrawColumn"/> event.
        /// </summary>     
        /// <param name="column">
        /// The column that will be drawn.
        /// </param>
        private void DoBeforeDrawColumn(int column)
        {
            if (this.BeforeDrawColumn != null)
            {
                this.BeforeDrawColumn(this, new TableDrawEventArgs<T> { Column = column, Row = -1, Model = this.Model });
            }
        }

        /// <summary>
        /// Raises the <see cref="BeforeDrawRow"/> event.
        /// </summary>     
        /// <param name="row">
        /// The row that will drawn.
        /// </param>
        private void DoBeforeDrawRow(int row)
        {
            if (this.BeforeDrawRow != null)
            {
                this.BeforeDrawRow(this, new TableDrawEventArgs<T> { Column = -1, Row = row, Model = this.Model });
            }
        }

        /// <summary>
        /// Draws a table cell.
        /// </summary>
        /// <param name="row">
        /// The row of the cell being drawn.
        /// </param>
        /// <param name="column">
        /// The column of the cell being drawn.
        /// </param>
        /// <param name="options">
        /// The layout options to use when drawing the cell.
        /// </param>
        private void DrawCell(int row, int column, GUILayoutOption[] options)
        {
            var obj = this.Model.GetValue(row, column);

            // if the value is a callback invoke the callback
            var callback = obj as Action<int, int, T>;
            if (callback != null)
            {
                // raise after draw cell event
                this.DoAfterDrawCell(row, column);
                
                // if value is callback invoke it and let it handle drawing the table cell
                callback(row, column, this.Model.Elements[row]);
                
                // raise after draw cell event
                this.DoAfterDrawCell(row, column);
            
                // exit
                return;
            }

            // if a value can not be edited just draw a label
            if (!this.Model.CanEdit(row, column))
            {
                GUILayout.Label(obj == null ? string.Empty : obj.ToString(), options);
                return;
            }
          
            // raise before draw cell event
            this.DoBeforeDrawCell(row, column);

            // draw the control for the given value
            this.DrawControlForValue(row, column, options, obj);

            // raise after draw cell event
            this.DoAfterDrawCell(row, column);
        }

        /// <summary>
        /// Draws a control for value contained within a table cell.
        /// </summary>
        /// <param name="row">
        /// The row of the cell being drawn.
        /// </param>
        /// <param name="column">
        /// The column of the cell being drawn.
        /// </param>
        /// <param name="options">
        /// The layout options to use when drawing the cell.
        /// </param>
        /// <param name="obj">
        /// The value to be drawn.
        /// </param>
        private void DrawControlForValue(int row, int column, GUILayoutOption[] options, object obj)
        {
            // draw controls based on the type
            var text = obj as string;
            if (text != null)
            {
                this.Model.SetValue(row, column, GUILayout.TextField(text, options));
            }
            else if (obj is bool)
            {
                this.Model.SetValue(row, column, GUILayout.Toggle((bool)obj, string.Empty, options));
            }
#if UNITY_EDITOR
            else if (obj is int)
            {
                this.Model.SetValue(row, column, EditorGUILayout.IntField((int)obj, options));
            }
            else if (obj is float)
            {
                this.Model.SetValue(row, column, EditorGUILayout.FloatField((float)obj, options));
            }
            else
            {
                var texture = obj as Texture2D;
                if (texture != null)
                {
                    this.Model.SetValue(row, column, EditorGUILayout.ObjectField(texture, typeof(Texture2D), false, options));
                }
                else
                {
                    var gameObject = obj as GameObject;
                    if (gameObject != null)
                    {
                        this.Model.SetValue(row, column, EditorGUILayout.ObjectField(gameObject, typeof(GameObject), false, options));
                    }
                    else
                    {
                        var material = obj as Material;
                        if (material != null)
                        {
                            this.Model.SetValue(row, column, EditorGUILayout.ObjectField(material, typeof(Material), false, options));
                        }
                    }
                }
            }
#endif
        }

        /// <summary>
        /// Draws the table name at the top of the table.
        /// </summary>
        private void DrawTableName()
        {
            if (string.IsNullOrEmpty(this.TableName))
            {
                return;
            }

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(this.TableName);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
    }
}