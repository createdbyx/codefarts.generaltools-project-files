/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.GeneralTools.Editor.Processors
{
    using UnityEditor;
    using Codefarts.CoreProjectCode.Settings;

    /// <summary>
    /// Provides a processor for making a texture readable when it is imported into unity.
    /// </summary>
    public class MakeReadableTextureProcessor : AssetPostprocessor
    {
        /// <summary>
        /// Called by Unity when preprocessing occurs.
        /// </summary>
        public void OnPreprocessTexture()
        {
            var extension = SettingsManager.Instance.GetSetting(GlobalConstants.ReadableTextureProcessor, "_NewFileNameTempPewpzIRReadableTex");

            var assetPathFilename = System.IO.Path.GetFileNameWithoutExtension(this.assetPath);
            if (assetPathFilename != null && !assetPathFilename.EndsWith(extension))
            {
                return;
            }

            var textureImporter = this.assetImporter as TextureImporter;
            if (textureImporter == null)
            {
                return;
            }

            // make readable
            textureImporter.isReadable = true;
        }
    }
}