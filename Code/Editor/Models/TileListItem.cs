/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.GeneralTools.Editor.Models
{
    using System.Collections.Generic;

    using Codefarts.GeneralTools.Models;
    using Codefarts.GeneralTools.Utilities.Models;  

    using UnityEngine;                        

    public class TileListItem
    {
        public Texture2D Texture;
        public Rect SourceRect;
        public List<Point2D> Locations;
        public bool Checked;
        public PixelsContainer Image;
    }
}