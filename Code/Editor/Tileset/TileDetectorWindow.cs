/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.GeneralTools.Editor.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Codefarts.GeneralTools.Code;
    using Codefarts.GeneralTools.Editor.Models;
    using Codefarts.GeneralTools.Editor.Utilities;
    using Codefarts.GeneralTools.Utilities;
    using Codefarts.GeneralTools.Utilities.Models;   

    using UnityEditor;

    using UnityEngine;
                                             
    using Object = UnityEngine.Object;
    using Vector2 = UnityEngine.Vector2;

    public class TileDetectorWindow : EditorWindow
    {
        private Vector2 PreviewScroll;
        private int tileWidth = 32;
        private int tileHeight = 32;
        // private Texture2D texture;
        private string textureFile = string.Empty;
        private bool textureAsset;
        private string outputFileName = string.Empty;
        private Vector2 tileList;
        private List<TileListItem> tileEntries = new List<TileListItem>();
        private bool isRunning;
        private string runningMessage = string.Empty;
        TileSetDetector detector;
        private int generatedSize = 2;
        private float completionProgress;
        private AtlasGenerator atlasGenerator;
        private int spacing = 1;
        private Texture2D readableTexture;
        private Texture2D srcTexture;


        private void OnDestroy()
        {
            Debug.Log("Destroying");
            if (this.isRunning && this.detector != null) this.detector.Cancel();
            this.DestroyReadableTexture();
            AssetDatabase.Refresh();
            this.ClearTiles();
        }

        private void OnGUI()
        {
            if (this.isRunning && this.detector != null)
            {
                this.runningMessage = string.Format("{0}% Processed: {1} of {2} Unique: {3}", Math.Round(this.detector.PercentageCompleted * 100, 2), this.detector.ProcessedTiles, this.detector.TotalTiles, this.detector.UniqueTiles);
                // detector.Update();
                if (!this.detector.Complete)
                {
                    if (EditorUtility.DisplayCancelableProgressBar(
                        "Detecting ...",
                        this.runningMessage,
                        this.detector.PercentageCompleted))
                    {
                        this.detector.Cancel();
                        while (this.detector.IsRunning)
                        {
                            // do nothing
                            Debug.Log("Waiting");
                            System.Threading.Thread.Sleep(0);
                        }

                        Debug.Log("Completed2");
                        this.BuildTileEntries();

                        // this.detector.Dispose();
                        this.detector = null;
                        this.runningMessage = string.Empty;
                        this.isRunning = false;
                        EditorUtility.ClearProgressBar();
                    }

                    this.Repaint();
                }
                else
                {
                    Debug.Log("Completed");
                    this.BuildTileEntries();

                    // this.detector.Dispose();
                    this.detector = null;
                    this.runningMessage = string.Empty;
                    this.isRunning = false;
                }
            }
            else if (this.isRunning && this.atlasGenerator != null)
            {
                this.runningMessage = string.Format("{0}% Processed: {1} of {2}", Math.Round(this.atlasGenerator.PercentageCompleted * 100, 2), this.atlasGenerator.ProcessedTiles, this.atlasGenerator.TileCount);

                // Debug.Log("after update");
                if (!this.atlasGenerator.Complete)
                {
                    if (EditorUtility.DisplayCancelableProgressBar(
                        "Building Tile Atlas(s) ...",
                        this.runningMessage,
                        this.atlasGenerator.PercentageCompleted))
                    {
                        this.atlasGenerator = null;
                        this.runningMessage = string.Empty;
                        this.isRunning = false;
                    }

                    this.Repaint();
                }
                else
                {
                    //   Debug.Log("completed");
                    this.atlasGenerator = null;
                    this.runningMessage = string.Empty;
                    this.isRunning = false;
                }
            }
            else
            {
                try
                {
                    EditorUtility.ClearProgressBar();
                    GUILayout.BeginHorizontal();
                    this.DrawPreview();
                    this.DrawSideControls();
                    GUILayout.EndHorizontal();
                }
                catch
                {
                }
            }
        }

        private void BuildTileEntries()
        {
            // dispose textures
            foreach (var entry in this.tileEntries)
            {
                if (entry.Texture != null)
                {
                    Object.DestroyImmediate(entry.Texture, false);
                }

                entry.Texture = null;
            }

            this.tileEntries.Clear();
            var output = string.Empty;
            //   Debug.Log("started getting tile ids");
            //  System.Threading.Thread.Sleep(1);

            //output =  this.detector.GetTileString(0) + "\r\n\r\n------\r\n\r\n" + this.detector.GetTileString(7) + "\r\n";

            //  var path = System.IO.Path.Combine(Environment.CurrentDirectory, "Assets/Textures");
            //output = System.IO.Path.Combine(path,"FileA.png") + "\r\n";

            //  var ids = this.detector.TileIds.ToArray();


            //var tex = this.detector.GetTileImage(ids[0]).ToTexture2D();
            //System.IO.File.WriteAllBytes(System.IO.Path.Combine(path, "FileA.png"), tex.EncodeToPNG());
            //Object.DestroyImmediate(tex, true);
            //tex = this.detector.GetTileImage(ids[7]).ToTexture2D();
            //System.IO.File.WriteAllBytes(System.IO.Path.Combine(path, "FileB.png"), tex.EncodeToPNG());
            //Object.DestroyImmediate(tex, true);

            foreach (var id in this.detector.TileIds)
            {
                output += id + "\r\n";
                var points = this.detector.GetTilePoints(id);
                var tileListItem = new TileListItem();
                tileListItem.Locations = points.ToList();
                tileListItem.SourceRect = new Rect(points[0].X, points[0].Y, this.detector.TileWidth, this.detector.TileHeight);
                //  Debug.Log("getting time image");

                //var tileBitmap = this.detector.GetTileImage(id);
                //tileListItem.Texture = tileBitmap.ToTexture2D();

                var tileBitmap = new Texture2D(this.detector.TileWidth, this.detector.TileHeight, TextureFormat.ARGB32, false);
                var bytes = this.readableTexture.GetPixels(points[0].X, points[0].Y, this.detector.TileWidth, this.detector.TileHeight, 0);
                tileBitmap.SetPixels(0, 0, this.detector.TileWidth, this.detector.TileHeight, bytes);
                tileBitmap.Apply();
                //tileBitmap.Blit(this.readableTexture, 0, 0, points[0].X, points[0].Y, this.detector.TileWidth, this.detector.TileHeight);
                tileListItem.Texture = tileBitmap;

                //var img = new GenericImage<Imaging.Color>(this.detector.TileWidth, this.detector.TileHeight);

                //img.Draw(this.readableTexture.ToGenericImage(), 0, 0, points[0].X, points[0].Y, this.detector.TileWidth, this.detector.TileHeight);
                tileListItem.Image = new PixelsContainer() { colors = bytes, Width = this.detector.TileWidth, Height = this.detector.TileHeight };// tileBitmap.GetP  ixels(0);
                //tileListItem.Image = img;


                //  Debug.Log("image retrieved and converted");
                // tileBitmap.Dispose();
                this.tileEntries.Add(tileListItem);
            }

            Debug.Log(output);
        }

        private void DrawSideControls()
        {
            GUILayout.BeginVertical(GUILayout.Width(200), GUILayout.MinWidth(200));
            var size = EditorGUILayout.Vector2Field("Tile Size", new Vector2(this.tileWidth, this.tileHeight));
            this.tileWidth = (int)size.x;
            this.tileHeight = (int)size.y;

            var tmpValue = GUILayout.Toggle(this.textureAsset, this.textureAsset ? "Asset" : "File");
            if (tmpValue != this.textureAsset)
            {
                switch (tmpValue)
                {
                    case true:
                        this.SetupSelectedTextureAsset(this.srcTexture);
                        break;

                    case false:
                        this.SetupSelectedTextureFile(this.textureFile);
                        break;
                }

                this.tileEntries.Clear();
            }
            this.textureAsset = tmpValue;

            if (this.textureAsset)
            {
                var tmp = (Texture2D)EditorGUILayout.ObjectField("Texture", this.srcTexture, typeof(Texture2D), true);
                if (tmp != this.srcTexture && tmp != null)
                {
                    this.SetupSelectedTextureAsset(tmp);
                }
            }
            else
            {
                this.textureFile = GUILayout.TextField(this.textureFile, GUILayout.MaxWidth(200));
                if (GUILayout.Button("Select", GUILayout.MaxWidth(64)))
                {
                    this.DoSelectTextureFile();
                }
            }

            GUILayout.Label("Output filename");
            this.outputFileName = GUILayout.TextField(this.outputFileName, GUILayout.MaxWidth(200));
            if (GUILayout.Button("Set", GUILayout.MaxWidth(40)))
            {
                this.DoSelectOutputPath();
            }

            GUILayout.Space(8);
            if (GUILayout.Button("Detect"))
            {
                this.DoDetection();
            }

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Remove", GUILayout.Width(64)))
            {
                this.RemoveSelectedTiles();
            }

            if (GUILayout.Button("Clear", GUILayout.Width(64)))
            {
                this.ClearTiles();
            }

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("All", GUILayout.Width(64)))
            {
                this.SelectAll();
            }

            if (GUILayout.Button("None", GUILayout.Width(64)))
            {
                this.SelectNone();
            }

            GUILayout.EndHorizontal();

            this.DrawDetectedTilesList();

            //GUILayout.Space(8);
            //if (GUILayout.Button("Optimize")) { }

            GUILayout.Space(8);
            GUILayout.Label("Generated Size");
            this.generatedSize = EditorGUILayout.Popup(this.generatedSize, new[] { "256", "512", "1024", "2048", "4096" });

            GUILayout.Space(4);
            this.spacing = EditorGUILayout.IntField("Spacing", this.spacing);
            this.spacing = this.spacing < 0 ? 0 : this.spacing;

            GUILayout.Space(4);
            if (GUILayout.Button("Generate Atlas")) this.GenerateTileAtlas();

            GUILayout.Space(4);
            if (GUILayout.Button("Save Individually")) this.SaveIndividually();

            GUILayout.EndVertical();

            //if (GUILayout.Button("test"))
            //{
            //    var one = new byte[]{23,155,32,56,89};
            //    var two = new byte[] { 23, 155, 32, 56, 89 };
            //    Debug.Log("onehash: " + one.GetHashCode() + "\r\ntwohash: " + two.GetHashCode());
            //    Debug.Log(this.ByteArrayToString(one) + "\r\n" + this.ByteArrayToString(two));
            //    Debug.Log(this.ByteArrayToString(one).GetHashCode() + "\r\n" + this.ByteArrayToString(two).GetHashCode());
            //}
        }

        private void SaveIndividually()
        {
            if (string.IsNullOrEmpty(this.outputFileName))
            {
                return;
            }

            if (this.tileEntries == null)
            {
                return;
            }

            var directory = System.IO.Path.GetDirectoryName(this.outputFileName);
            if (directory != null && !System.IO.Directory.Exists(directory))
            {
                return;
            }

            for (int i = 0; i < this.tileEntries.Count; i++)
            {
                var tile = this.tileEntries[i];
                if (tile.Checked)
                {
                    var file = System.IO.Path.Combine(directory, i + ".png");
                    System.IO.File.WriteAllBytes(file, tile.Texture.EncodeToPNG());
                    Debug.Log(file);
                }
            }
        }

        //private string ByteArrayToString(byte[] arrInput)
        //{
        //    int i;
        //    var sOutput = string.Empty;// new StringBuilder(arrInput.Length);
        //    for (i = 0; i < arrInput.Length; i++)
        //    {
        //        sOutput += arrInput[i].ToString() +",";//.ToString("X2"));
        //    }
        //    return sOutput;
        //}

        private void SelectNone()
        {
            foreach (var entry in this.tileEntries)
            {
                entry.Checked = false;
            }
        }

        private void SelectAll()
        {
            foreach (var entry in this.tileEntries)
            {
                entry.Checked = true;
            }
        }

        private void ClearTiles()
        {
            this.SelectAll();
            this.RemoveSelectedTiles();
        }

        private void GenerateTileAtlas()
        {
            if (string.IsNullOrEmpty(this.outputFileName))
            {
                return;
            }

            if (this.tileEntries == null)
            {
                return;
            }

            var directory = System.IO.Path.GetDirectoryName(this.outputFileName);
            if (directory != null && !System.IO.Directory.Exists(directory))
            {
                return;
            }

            // create texture
            var size = 512;
            switch (this.generatedSize)
            {
                case 0:
                    size = 256;
                    break;

                case 1:
                    size = 512;
                    break;

                case 2:
                    size = 1024;
                    break;

                case 3:
                    size = 2048;
                    break;

                case 4:
                    size = 4096;
                    break;
            }

            var items = new List<PixelsContainer>();
            foreach (var tile in this.tileEntries)
            {
                if (tile.Checked)
                {
                    items.Add(tile.Image);
                }
            }

            this.atlasGenerator = new AtlasGenerator(size, this.spacing, this.outputFileName, items);
            this.isRunning = true;
            this.atlasGenerator.Run();
        }

        //private string SaveOutTextureAtlas(Texture2D tex, int atlasIndex)
        //{
        //    if (!System.IO.Directory.Exists(this.outputFileName)) System.IO.Directory.CreateDirectory(this.outputFileName);
        //    var file = System.IO.Path.Combine(this.outputFileName, this.assetName + (atlasIndex == 0 ? string.Empty : atlasIndex.ToString()) + ".png");
        //    System.IO.File.WriteAllBytes(file, tex.EncodeToPNG());
        //    Object.DestroyImmediate(tex, true);
        //    AssetDatabase.ImportAsset(file, ImportAssetOptions.Default);
        //    return file;
        //}

        //private void GenerateTileAtlas()
        //{
        //    if (string.IsNullOrEmpty(this.outputFileName)) return;
        //    if (this.tileEntries == null) return;
        //    if (!System.IO.Directory.Exists(this.outputFileName)) return;

        //    // create texture
        //    var size = 512;
        //    if (this.generatedSize == 0) size = 256;
        //    if (this.generatedSize == 1) size = 512;
        //    if (this.generatedSize == 2) size = 1024;
        //    if (this.generatedSize == 3) size = 2048;
        //    if (this.generatedSize == 4) size = 4096;

        //    var tempPath = Helpers.GetTempAssetPath();
        //    bool tempPathCreated = false;
        //    if (!System.IO.Directory.Exists(tempPath))
        //    {
        //        System.IO.Directory.CreateDirectory(tempPath);
        //        tempPathCreated = true;
        //    }

        //    var bmp = new Bitmap(size, size, PixelFormat.Format32bppArgb);
        //    var gfx = Graphics.FromImage(bmp);
        //    var pos = new Point();
        //    string file;
        //    var atlasIndex = 0;
        //    foreach (var entry in this.tileEntries)
        //    {
        //        if (!entry.Checked) continue;

        //        file = System.IO.Path.Combine(tempPath, this.assetName + ".png");

        //        System.IO.File.WriteAllBytes(file, entry.Texture.EncodeToPNG());
        //        AssetDatabase.ImportAsset(file, ImportAssetOptions.Default);

        //        var tmp = (Texture2D)AssetDatabase.LoadAssetAtPath(file, typeof(Texture2D));

        //        var readablePath = Helpers.CreateReadableTexture(tmp);

        //        Object.DestroyImmediate(tmp, true);
        //        AssetDatabase.DeleteAsset(file);

        //        var readTex = (Texture2D)AssetDatabase.LoadAssetAtPath(readablePath, typeof(Texture2D));

        //        var tmpBmp = readTex.ToBitmap();
        //        gfx.DrawImage(tmpBmp, pos);
        //        tmpBmp.Dispose();
        //        tmpBmp = null;

        //        pos.X += readTex.width;
        //        if (pos.X >= bmp.Width)
        //        {
        //            pos.X = 0;
        //            pos.Y += readTex.height;
        //            //detect if we have moved passed bottom of bitmap 
        //            if (pos.Y >= bmp.Height)
        //            {
        //                // save current atlas and start a new one
        //                this.SaveOutTextureAtlas(bmp, atlasIndex++);

        //                // dispose bitmap and graphics
        //                bmp.Dispose();
        //                gfx.Flush();
        //                gfx.Dispose();

        //                // start a new texture atlas bitmap
        //                bmp = new Bitmap(size, size, PixelFormat.Format32bppArgb);
        //                gfx = Graphics.FromImage(bmp);
        //                pos = new Point();
        //            }
        //        }

        //        Object.DestroyImmediate(readTex, true);
        //        AssetDatabase.DeleteAsset(readablePath);
        //    }
        //    gfx.Flush();
        //    gfx.Dispose();

        //    file = this.SaveOutTextureAtlas(bmp, atlasIndex);

        //    // no longer need bitmap
        //    bmp.Dispose();

        //    if (System.IO.Directory.Exists(tempPath) && tempPathCreated) System.IO.Directory.Delete(tempPath);

        //    AssetDatabase.Refresh();
        //}

        //private string SaveOutTextureAtlas(Bitmap bmp, int atlasIndex)
        //{
        //    var file = System.IO.Path.Combine(this.outputFileName, this.assetName + (atlasIndex == 0 ? string.Empty : atlasIndex.ToString()) + ".png");
        //    var tex = bmp.ToTexture2D();
        //    System.IO.File.WriteAllBytes(file, tex.EncodeToPNG());
        //    Object.DestroyImmediate(tex, true);
        //    if (!System.IO.Directory.Exists(this.outputFileName)) System.IO.Directory.CreateDirectory(this.outputFileName);
        //    AssetDatabase.ImportAsset(file, ImportAssetOptions.Default);
        //    return file;
        //}

        private void RemoveSelectedTiles()
        {
            var index = 0;
            while (index < this.tileEntries.Count)
            {
                var tile = this.tileEntries[index];
                if (tile.Checked)
                {
                    if (tile.Texture != null) Object.DestroyImmediate(tile.Texture, false);
                    this.tileEntries.RemoveAt(index);
                }
                else
                {
                    index++;
                }
            }
        }

        private void DrawDetectedTilesList()
        {
            this.tileList = GUILayout.BeginScrollView(this.tileList, false, true, GUILayout.Height(256));
            foreach (var item in this.tileEntries)
            {
                var guiContent = new GUIContent(string.Format("{0},{1} Count: {2}", item.SourceRect.x, item.SourceRect.y, item.Locations.Count), item.Texture);
                // item.Checked = GUILayout.Toggle(item.Checked,guiContent,  GUILayout.Height(Math.Max( item.Texture.height,GUI.skin.toggle.lineHeight)));
                //  var guiContent = new GUIContent(string.Format("{0},{1} Count: {2}", item.SourceRect.x, item.SourceRect.y, item.Locations.Count));
                item.Checked = GUILayout.Toggle(item.Checked, guiContent, GUILayout.Height(GUI.skin.toggle.CalcHeight(guiContent, 10000)));
            }

            GUILayout.EndScrollView();
        }

        private void DoDetection()
        {
            if (this.isRunning || this.readableTexture == null)
            {
                return;
            }

            this.isRunning = true;
            this.detector = new TileSetDetector(this.readableTexture);
            this.detector.TileWidth = this.tileWidth;
            this.detector.TileHeight = this.tileHeight;
            this.detector.Run();
            Debug.Log("started detection");
        }

        private void DoSelectOutputPath()
        {
            this.outputFileName = EditorUtility.SaveFilePanel("Select output path", string.Empty, this.outputFileName, "png");
        }

        private void DoSelectTextureFile()
        {
            var path = this.textureFile.Trim();
            if (!string.IsNullOrEmpty(path))
            {
                var dir = System.IO.Path.GetDirectoryName(path);
                if (System.IO.Directory.Exists(dir)) path = dir;
            }

            var file = EditorUtility.OpenFilePanel("Select Texture file", path, string.Empty);
            if (string.IsNullOrEmpty(file))
            {
                return;
            }

            this.SetupSelectedTextureFile(file);

            //var path = this.textureFile.Trim();
            //if (!string.IsNullOrEmpty(path))
            //{
            //    var dir = System.IO.Path.GetDirectoryName(path);
            //    if (System.IO.Directory.Exists(dir)) path = dir;
            //}
            //var file = EditorUtility.OpenFilePanel("Select Texture file", path, string.Empty);
            //Bitmap bmp;
            //try
            //{
            //    bmp = new Bitmap(file);
            //}
            //catch (System.Exception)
            //{
            //    EditorUtility.DisplayDialog("Error", "Could not load file as a image!", "OK");
            //    return;
            //}

            //var newBmp = new Bitmap(bmp.Width, bmp.Height, PixelFormat.Format32bppArgb);
            //var gfx = Graphics.FromImage(newBmp);
            //gfx.DrawImage(bmp, 0, 0);
            //gfx.Flush();
            //gfx.Dispose();

            //if (this.texture != null) Object.DestroyImmediate(this.texture, false);
            //this.texture = newBmp.ToTexture2D();

            //bmp.Dispose();
            //newBmp.Dispose();
            //this.textureFile = file;
        }

        private void SetupSelectedTextureAsset(Texture2D tmp)
        {
            this.DestroyReadableTexture();

            var file = AssetDatabase.GetAssetPath(tmp);
            if (string.IsNullOrEmpty(file)) return;
            var filePath = System.IO.Path.GetDirectoryName(file);

            var tempFile = System.IO.Path.Combine(filePath, System.IO.Path.GetFileNameWithoutExtension(file) + "_TileArrangeTemp_" + Math.Round(UnityEngine.Random.value * 10000).ToString());
            tempFile = System.IO.Path.ChangeExtension(tempFile, System.IO.Path.GetExtension(file));
            System.IO.File.Copy(file, tempFile);
            AssetDatabase.ImportAsset(tempFile, ImportAssetOptions.Default);

            this.readableTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(tempFile, typeof(Texture2D));
            AssetDatabase.Refresh();

            this.srcTexture = tmp;
        }

        private void SetupSelectedTextureFile(string file)
        {
            file = file.Trim();
            if (string.IsNullOrEmpty(file)) return;
            if (!System.IO.File.Exists(file)) return;

            this.DestroyReadableTexture();

            var tempFile = System.IO.Path.Combine("Assets", System.IO.Path.GetFileNameWithoutExtension(file) + "_TileArrangeTemp_" + Math.Round(UnityEngine.Random.value * 10000).ToString());
            tempFile = System.IO.Path.ChangeExtension(tempFile, System.IO.Path.GetExtension(file));
            System.IO.File.Copy(file, tempFile);
            AssetDatabase.ImportAsset(tempFile, ImportAssetOptions.Default);

            this.readableTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(tempFile, typeof(Texture2D));
            AssetDatabase.Refresh();

            this.textureFile = file;
        }

        private void DestroyReadableTexture()
        {
            if (this.readableTexture != null)
            {
                var tmpPath = AssetDatabase.GetAssetPath(this.readableTexture);
               // Object.DestroyImmediate(this.readableTexture, false);
                AssetDatabase.DeleteAsset(tmpPath);
                //  Debug.Log("destroyed");
            }
        }

        private void DrawPreview()
        {
            this.PreviewScroll = GUILayout.BeginScrollView(this.PreviewScroll, true, true);
            if (this.readableTexture != null)
            {
                GUILayout.Label(string.Empty, GUILayout.Width(this.readableTexture.width), GUILayout.Height(this.readableTexture.height));
                GUI.DrawTexture(new Rect(4, 4, this.readableTexture.width, this.readableTexture.height), this.readableTexture, ScaleMode.StretchToFill, true, 1);

                // draw first selection rectangles
                var first = this.tileEntries.FirstOrDefault(c => c.Checked);
                if (first != null)
                {
                    // draw first blue rectangle
                    var sourceRect = first.SourceRect;
                    //   Debug.Log(sourceRect);
                    sourceRect.x = sourceRect.x;// *sourceRect.width;
                    sourceRect.y = sourceRect.y;// *sourceRect.height;
                    GUILineDrawingHelpers.DrawRect(sourceRect, UnityEngine.Color.blue);
                    //   Debug.Log(sourceRect.ToUnityRect());

                    // draw first red rectangles
                    for (int index = 1; index < first.Locations.Count; index++)
                    {
                        var location = first.Locations[index];
                        sourceRect.x = location.X;// *sourceRect.width;
                        sourceRect.y = location.Y;// *sourceRect.height;
                        GUILineDrawingHelpers.DrawRect(sourceRect, UnityEngine.Color.red);
                    }
                }
            }
            // draw area
            GUILayout.EndScrollView();
        }

        [MenuItem("Window/Codefarts/Tile Mapping Utilities/Tile Detector")]
        static void Init()
        {
            // get the window, show it, and hand it focus
            try
            {
                var window = EditorWindow.GetWindow<TileDetectorWindow>();
                window.titleContent =new GUIContent( "Tile Detector");
                window.Show();
                window.Focus();
                window.Repaint();
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
    }
}