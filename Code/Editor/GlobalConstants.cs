﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.GeneralTools.Editor
{
    using Codefarts.CoreProjectCode.Settings;

    /// <summary>
    /// Provides various keys as global constant values for use with the settings system.
    /// </summary>
    public class GlobalConstants
    {
        
        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the filename string added to the end of textures when converting them in to readable textures.
        /// </summary>
        public const string ReadableTextureProcessor = "GeneralTools.Processors.ReadableTextureProcessorString";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the border size used to determine
        /// how fast the panning speed is when scene view auto panning is enabled. 
        /// </summary>
        public const string SceneViewAutoPanSpeedKey = "GeneralTools.SceneViewAutoPan.Speed";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the border size used to determine
        /// when to start auto panning when scene view auto panning is enabled. 
        /// </summary>
        public const string SceneViewAutoPanSizeKey = "GeneralTools.SceneViewAutoPan.Size";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to whether scene view auto panning is enabled. 
        /// </summary>
        public const string SceneViewAutoPanEnabledKey = "GeneralTools.SceneViewAutoPan.Enabled";
      
        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the available shaders that will be listed. 
        /// </summary>
        public const string TileMaterialCreationShadersKey = "GeneralTools.TileMaterialCreation.Shaders";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the default state of the free form
        /// checkbox in the tile material creation window. 
        /// </summary>
        public const string TileMaterialCreationFreeformKey = "GeneralTools.TileMaterialCreation.Freeform";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the default state of the tile width field in the tile material creation window. 
        /// </summary>
        public const string TileMaterialCreationDefaultWidthKey = "GeneralTools.TileMaterialCreation.DefaultWidth";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the default state of the tile height field in the tile material creation window. 
        /// </summary>
        public const string TileMaterialCreationDefaultHeightKey = "GeneralTools.TileMaterialCreation.DefaultHeight";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to whether the default state of the "As List" check box is checked in the tile material creation window. 
        /// </summary>
        public const string TileMaterialCreationAsListKey = "GeneralTools.TileMaterialCreation.AsList";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the default color displayed in the tile material creation window. 
        /// </summary>
        public const string DefaultTileMaterialCreationColorKey = "GeneralTools.TileMaterialCreation.DefaultColor";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the path where screen shots should be saved to. 
        /// </summary>
        public const string ScreenShotPath = "GeneralTools.ScreenShot.Path";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to the filename that is used when saving the screen shot. 
        /// </summary>
        public const string ScreenShotFileName = "GeneralTools.ScreenShot.FileNameTemplate";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to whether information is displayed in the console window about the saved screen shot. 
        /// </summary>
        public const string ScreenShotReportToConsole = "GeneralTools.ScreenShot.ReportToConcole";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to how big the saved screen shot will be. 
        /// </summary>
        public const string ScreenShotSuperSize = "GeneralTools.ScreenShot.SuperSize";
      
        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to whether or not text assets can be edited in the inspector window. 
        /// </summary>
        public const string TextAssetEditingEnabled = "GeneralTools.TextAssetEditingEnabled";

        /// <summary>
        /// Provides a unique id key for storing and retrieving <see cref="IValues{TKey}"/> settings relating to whether or not mono script assets can be edited in the inspector window. 
        /// </summary>
        public const string MonoScriptEditingEnabled = "GeneralTools.MonoScriptEditingEnabled";
    }
}
