﻿namespace Codefarts.GeneralTools.Editor
{
    using Codefarts.GeneralTools.Scripts.Camera;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides a editor for your text asset files that you can edit within the inspector.
    /// </summary>
    [CustomEditor(typeof(ExtendedFlycam))]
    public class ExtendedFlyCamEditor : Editor
    {
        /// <summary>
        /// Called by unity to draw the inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            var cam = this.target as ExtendedFlycam;

            GUILayout.BeginVertical();
            cam.Yaw = EditorGUILayout.FloatField("Yaw", cam.Yaw);
            cam.Yaw = GUILayout.HorizontalSlider(cam.Yaw, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.Pitch = EditorGUILayout.FloatField("Pitch", cam.Pitch);
            cam.Pitch = GUILayout.HorizontalSlider(cam.Pitch, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.Roll = EditorGUILayout.FloatField("Roll", cam.Roll);
            cam.Roll = GUILayout.HorizontalSlider(cam.Roll, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginHorizontal();
            cam.LockYaw = GUILayout.Toggle(cam.LockYaw, "Lock Yaw");
            cam.LockPitch = GUILayout.Toggle(cam.LockPitch, "Lock Pitch");
            cam.LockRoll = GUILayout.Toggle(cam.LockRoll, "Lock Roll");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            cam.CanMove = GUILayout.Toggle(cam.CanMove, "Can Move");
            cam.CanRotate = GUILayout.Toggle(cam.CanRotate, "Can Rotate");
            cam.CanRoll = GUILayout.Toggle(cam.CanRoll, "Can Roll");
            GUILayout.EndHorizontal();

            // yaw min max
            cam.RestrictYaw = EditorGUILayout.BeginToggleGroup("Restrict Yaw", cam.RestrictYaw);
            cam.WrapYaw = GUILayout.Toggle(cam.WrapYaw, "Wrap Yaw");

            GUILayout.BeginVertical();
            cam.MinYaw = EditorGUILayout.FloatField("Min Yaw", cam.MinYaw);
            cam.MinYaw = GUILayout.HorizontalSlider(cam.MinYaw, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.MaxYaw = EditorGUILayout.FloatField("Max Yaw", cam.MaxYaw);
            cam.MaxYaw = GUILayout.HorizontalSlider(cam.MaxYaw, 0, 360);
            GUILayout.EndVertical();

            EditorGUILayout.EndToggleGroup();

            // pitch min max
            cam.RestrictPitch = EditorGUILayout.BeginToggleGroup("Restrict Pitch", cam.RestrictPitch);
            cam.WrapPitch = GUILayout.Toggle(cam.WrapPitch, "Wrap Pitch");

            GUILayout.BeginVertical();
            cam.MinPitch = EditorGUILayout.FloatField("Min Pitch", cam.MinPitch);
            cam.MinPitch = GUILayout.HorizontalSlider(cam.MinPitch, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.MaxPitch = EditorGUILayout.FloatField("Max Pitch", cam.MaxPitch);
            cam.MaxPitch = GUILayout.HorizontalSlider(cam.MaxPitch, 0, 360);
            GUILayout.EndVertical();

            EditorGUILayout.EndToggleGroup();

            // roll min max
            cam.RestrictRoll = EditorGUILayout.BeginToggleGroup("Restrict Roll", cam.RestrictRoll);
            cam.WrapRoll = GUILayout.Toggle(cam.WrapRoll, "Wrap Roll");

            GUILayout.BeginVertical();
            cam.MinRoll = EditorGUILayout.FloatField("Min Roll", cam.MinRoll);
            cam.MinRoll = GUILayout.HorizontalSlider(cam.MinRoll, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.MaxRoll = EditorGUILayout.FloatField("Max Roll", cam.MaxRoll);
            cam.MaxRoll = GUILayout.HorizontalSlider(cam.MaxRoll, 0, 360);
            GUILayout.EndVertical();

            EditorGUILayout.EndToggleGroup();

            GUILayout.BeginVertical();
            cam.CameraSensitivity = EditorGUILayout.FloatField("Camera Sensitivity", cam.CameraSensitivity);
            cam.CameraSensitivity = GUILayout.HorizontalSlider(cam.CameraSensitivity, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.ClimbSpeed = EditorGUILayout.FloatField("Climb Speed", cam.ClimbSpeed);
            cam.ClimbSpeed = GUILayout.HorizontalSlider(cam.ClimbSpeed, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.NormalMoveSpeed = EditorGUILayout.FloatField("Normal Move Speed", cam.NormalMoveSpeed);
            cam.NormalMoveSpeed = GUILayout.HorizontalSlider(cam.NormalMoveSpeed, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.SlowMoveMultiplier = EditorGUILayout.FloatField("Slow Move Multiplier", cam.SlowMoveMultiplier);
            cam.SlowMoveMultiplier = GUILayout.HorizontalSlider(cam.SlowMoveMultiplier, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.FastMoveMultiplier = EditorGUILayout.FloatField("Fast Move Multiplier", cam.FastMoveMultiplier);
            cam.FastMoveMultiplier = GUILayout.HorizontalSlider(cam.FastMoveMultiplier, 0, 360);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            cam.UseMouseButtonToRotate = GUILayout.Toggle(cam.UseMouseButtonToRotate, "Use Mouse To Rotate");
            cam.MouseButton = EditorGUILayout.IntField("Mouse Button Index", cam.MouseButton);
            GUILayout.EndVertical();


            GUILayout.BeginVertical();
            GUILayout.Label("Forward & Backward Axis Name");
            cam.ForwardAndBackwardAxisName = GUILayout.TextField(cam.ForwardAndBackwardAxisName);
            GUILayout.Label("Left & Right Axis Name");
            cam.LeftAndRightAxisName = GUILayout.TextField(cam.LeftAndRightAxisName);
            GUILayout.Label("Yaw Axis Name");
            cam.YawAxisName = GUILayout.TextField(cam.YawAxisName);
            GUILayout.Label("Pitch Axis Name");
            cam.PitchAxisName = GUILayout.TextField(cam.PitchAxisName);
            GUILayout.Label("Roll Axis Name");
            cam.RollAxisName = GUILayout.TextField(cam.RollAxisName);
            GUILayout.EndVertical();
        }
    }
}