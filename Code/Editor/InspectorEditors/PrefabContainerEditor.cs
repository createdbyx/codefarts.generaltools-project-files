﻿namespace Codefarts.GeneralTools.Editor
{
    using System;

    using Codefarts.GeneralTools.Scripts;

    using UnityEditor;

    using UnityEngine;

    using Object = UnityEngine.Object;

    /// <summary>
    /// Provides a editor for your script files that you can edit within the inspector.
    /// </summary>
    [CustomEditor(typeof(PrefabContainer))]
    public class PrefabContainerEditor : Editor
    {
        /// <summary>
        /// Called by unity to draw the inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            var target = this.target as PrefabContainer;
            this.DrawDefaultInspector();

            try
            {
                GUILayout.BeginVertical();

                try
                {
                    GUILayout.BeginHorizontal();
                    this.DrawAddButtons(target);
                }
                finally
                {
                    GUILayout.EndHorizontal();
                }

                this.DrawList(target);

                try
                {
                    GUILayout.BeginHorizontal();
                    this.DrawAddButtons(target);
                }
                finally
                {
                    GUILayout.EndHorizontal();
                }
            }
            finally
            {
                GUILayout.EndVertical();
            }
        }

        private void DrawAddButtons(PrefabContainer target)
        {
            GUILayout.Label(string.Format("Count: {0}", target.Names == null ? 0 : target.Names.Length));
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Add"))
            {
                Array.Resize(ref target.Names, target.Names.Length + 1);
                Array.Resize(ref target.Prefabs, target.Prefabs.Length + 1);
            }

            if (GUILayout.Button("Add Selected"))
            {
                var gameObjects = Selection.gameObjects;
                var startIndex = target.Names.Length;
                Array.Resize(ref target.Names, target.Names.Length + gameObjects.Length);
                Array.Resize(ref target.Prefabs, target.Prefabs.Length + gameObjects.Length);
                foreach (var gameObject in gameObjects)
                {
                    target.Names[startIndex] = gameObject.name;
                    target.Prefabs[startIndex] = gameObject;
                    startIndex++;
                }
            }
        }

        private void DrawList(PrefabContainer target)
        {
            var i = 0;
            while (i < target.Names.Length)
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("X", GUILayout.MaxWidth(26)))
                {
                    target.Names = this.RemoveRange(target.Names, i, 1);
                    target.Prefabs = this.RemoveRange(target.Prefabs, i, 1);
                    continue;
                }

                GUILayout.Label(i + ": ", GUILayout.ExpandWidth(false));

                var text = target.Names[i] == null ? string.Empty : target.Names[i];
                target.Names[i] = GUILayout.TextField(text, GUILayout.Width(128));
                target.Prefabs[i] = EditorGUILayout.ObjectField(target.Prefabs[i], typeof(Object), true);
                GUILayout.EndHorizontal();

                i++;
            }
        }

        private T[] RemoveRange<T>(T[] array, int index, int length)
        {

            if (length < 1)
            {
                return array;
            }

            if (index < 0 || index > array.Length - 1)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            if (index + length > array.Length - 1)
            {
                Array.Resize(ref array, index);
                return array;
            }

            var endLength = Math.Max(0, Math.Min(array.Length - index, array.Length - (index + length)));
            var tempArray = new T[endLength];
            Array.Copy(array, index + length, tempArray, 0, endLength);
            Array.Resize(ref array, array.Length - length);
            tempArray.CopyTo(array, array.Length - endLength);
            return array;
        }
    }
}