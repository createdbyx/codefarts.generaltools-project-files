﻿namespace Codefarts.GeneralTools.Editor
{
    using Codefarts.GeneralTools.Scripts.Camera;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides a editor for your script files that you can edit within the inspector.
    /// </summary>
    [CustomEditor(typeof(CameraMousePanning))]
    public class CameraMousePanningEditor : Editor
    {    
        /// <summary>
        /// Called by unity to draw the inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            this.DrawDefaultInspector();
            var cam = this.target as CameraMousePanning;
            cam.CameraObject = EditorGUILayout.ObjectField("Camera", cam.CameraObject, typeof(Camera), true) as Camera;
        }
    }
}