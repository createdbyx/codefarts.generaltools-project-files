﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Codefarts" file="TextAssetEditor.cs">
//   Copyright (c) 2012 Codefarts
//     All rights reserved.
//     contact@codefarts.com
//     http://www.codefarts.com
// </copyright>
// <summary>
//   Provides a editor for your text asset files that you can edit within the inspector.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Codefarts.GeneralTools.Editor
{
    using System.IO;

    using Codefarts.CoreProjectCode.Settings;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides a editor for your text asset files that you can edit within the inspector.
    /// </summary>
#if TEXTASSET_EDITOR
    [CustomEditor(typeof(TextAsset))]
#endif
    public class TextAssetEditor : Editor
    {
        /// <summary>
        /// Holds a working copy of the code.
        /// </summary>
        private string workingText = string.Empty;

        /// <summary>
        /// Holds a value indicating whether or not there has been changes made to the script.
        /// </summary>
        private bool isDirty;

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            var asset = this.target as TextAsset;
            if (asset != null)
            {
                this.workingText = asset.text;
            }
        }

        /// <summary>
        /// Called by unity to draw the inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            GUI.enabled = true;

            var settings = SettingsManager.Instance;
            if (!settings.GetSetting(GlobalConstants.TextAssetEditingEnabled, true))
            {
                this.DrawDefaultInspector();
                var asset = this.target as TextAsset;
                if (asset != null)
                {
                    GUILayout.TextArea(asset.text);
                }

                return;
            }

            this.DrawHeaderInfo();

            this.DrawEditor();
        }

        /// <summary>
        /// Draws the header information.
        /// </summary>
        private void DrawHeaderInfo()
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Original") && this.isDirty
                && EditorUtility.DisplayDialog("Are you sure?", "Are you sure you want to restore to the original?", "Yes", "Cancel"))
            {
                var asset = this.target as TextAsset;
                if (asset != null)
                {
                    this.workingText = asset.text;
                    this.isDirty = false;
                }
            }

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Save") && this.isDirty
                && EditorUtility.DisplayDialog("Are you sure?", "Are you sure you want to save over the asset?", "Yes", "Cancel"))
            {
                var asset = this.target as TextAsset;
                var file = AssetDatabase.GetAssetPath(asset);
                File.WriteAllText(file, this.workingText);
                AssetDatabase.Refresh();
                this.isDirty = false;
            }

            GUILayout.EndHorizontal();
        }

        /// <summary>
        /// Draws a text area for editing the data.
        /// </summary>
        private void DrawEditor()
        {
            GUILayout.BeginVertical();

            var value = GUILayout.TextArea(this.workingText);
            if (value != this.workingText)
            {
                this.workingText = value;
                this.isDirty = true;
            }

            GUILayout.EndVertical();
        }
    }
}