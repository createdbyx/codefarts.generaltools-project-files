﻿namespace Codefarts.GeneralTools.Editor
{
    using System;

    using Codefarts.GeneralTools.Scripts;
    using Codefarts.UnityExtensionMethods;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides a editor for your script files that you can edit within the inspector.
    /// </summary>
    [CustomEditor(typeof(VectorGrid))]
    public class VectorGridEditor : Editor
    {
        /// <summary>
        /// Called by unity to draw the inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            var grid = this.target as VectorGrid;
            //  this.DrawDefaultInspector();

            if (grid == null)
            {
                return;
            }

            var changed = false;
            grid.ShowMain = GUILayoutEx.Toggle(grid.ShowMain, "Show Main", (p, n) => changed = true);
            grid.ShowSub = GUILayoutEx.Toggle(grid.ShowSub, "Show Sub", (p, n) => changed = true);
            grid.UseMesh = GUILayoutEx.Toggle(grid.UseMesh, "Use Mesh", (p, n) => changed = true);

            grid.SmallStep = EditorGUILayout.FloatField("Small Step", grid.SmallStep);
            grid.LargeStep = EditorGUILayout.FloatField("Large Step", grid.LargeStep);

            grid.Width = EditorGUILayout.FloatField("Width", grid.Width);
            grid.Height = EditorGUILayout.FloatField("Height", grid.Height);
            grid.Depth = EditorGUILayout.FloatField("Depth", grid.Depth);

            grid.Offset = EditorGUILayout.Vector3Field("Offset", grid.Offset);

            grid.Origin = EditorGUILayout.ObjectField("Origin", grid.Origin, typeof(Transform), true) as Transform;
            grid.GridMaterial = EditorGUILayout.ObjectField("Grid Material", grid.GridMaterial, typeof(Material), true) as Material;
            grid.MainColor = EditorGUILayout.ColorField("Main Color", grid.MainColor);
            grid.SubColor = EditorGUILayout.ColorField("Sub Color", grid.SubColor);
                                                   
            if (GUI.changed)
            {
                SceneView.RepaintAll();
            }
        }
    }
}