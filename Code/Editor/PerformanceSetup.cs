/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
#if PERFORMANCE
namespace Codefarts.GeneralTools
{
    using Codefarts.GeneralTools.Code;
    using Codefarts.PerformanceTesting;
    using UnityEditor;

    /// <summary>
    /// Provides performance inspection menu items that integrate into unity's menu.
    /// </summary>
    [InitializeOnLoad]
    public class PerformanceSetup
    {
        /// <summary>
        /// Initializes static members of the <see cref="PerformanceSetup"/> class.
        /// </summary>
        static PerformanceSetup()
        {
            var perf = PerformanceTesting<string>.Instance;
            perf.ConvertFromStringCallback = (x) => x;
            perf.ConvertToStringCallback = (x) => x;

            perf.Create(PerformanceConstants.GetPerformanceKeys());
        }
    }
}
#endif