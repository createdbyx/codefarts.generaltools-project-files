﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

using System;
using Codefarts.CoreProjectCode.Settings;
using Codefarts.Localization;
using UnityEditor;
using UnityEngine;

namespace Codefarts.GeneralTools.Editor.TileSets
{
    /// <summary>
    /// Provides methods for drawing processor settings.
    /// </summary>
    public class ProcessorSettings
    {
        /// <summary>
        /// Draws the processor setting controls.
        /// </summary>
        public static void Draw()
        {
            var local = LocalizationManager.Instance;

            GUILayout.Label(local.Get("SETT_ReadableTextureProcessorString"));
            SettingHelpers.DrawSettingsTextField(GlobalConstants.TextAssetEditingEnabled, string.Empty, "_NewFileNameTempPewpzIRReadableTex",
                () =>
                {
                    var extension = SettingsManager.Instance.GetSetting(GlobalConstants.ReadableTextureProcessor, "_NewFileNameTempPewpzIRReadableTex");
                    if (string.IsNullOrEmpty(extension.Trim()))
                    {
                        SettingsManager.Instance.SetValue(GlobalConstants.ReadableTextureProcessor, "_NewFileNameTempPewpzIRReadableTex");
                    }
                });
        }
    }
}