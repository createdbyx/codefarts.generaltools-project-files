﻿/*
<copyright>
Copyright (c) 2012 Codefarts
All rights reserved.
contact@codefarts.com
http://www.codefarts.com
</copyright>
*/
namespace Codefarts.GeneralTools.Editor.TileSets
{
    using Codefarts.GeneralTools.Editor;
    using Codefarts.GeneralTools.Editor.Controls;
    using Codefarts.Localization;

    /// <summary>
    /// Provides methods for drawing screen hot settings.
    /// </summary>
    public class ScreenShotSettings
    {
        /// <summary>
        /// Holds a reference to a folder control used to draw settings.
        /// </summary>
        private static readonly SelectOutputFolderControl Folder = new SelectOutputFolderControl();

        /// <summary>
        /// Draws the screen shot setting controls.
        /// </summary>
        public static void Draw()
        {
            var local = LocalizationManager.Instance;

            SettingHelpers.DrawSettingsFolder(GlobalConstants.ScreenShotPath, "Assets", null, Folder);
            SettingHelpers.DrawSettingsTextField(GlobalConstants.ScreenShotFileName, local.Get("SETT_ScreenShotFileName"), "ScreenShot.png");
            SettingHelpers.DrawSettingsCheckBox(GlobalConstants.ScreenShotReportToConsole, local.Get("SETT_ScreenShotReportToConsole"), true);
            SettingHelpers.DrawSettingsIntField(GlobalConstants.ScreenShotSuperSize, local.Get("SETT_ScreenShotSuperSize"), 1, 1, 25);
        }
    }
}