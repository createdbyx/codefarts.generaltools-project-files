﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.GeneralTools.Editor.TileSets
{
    using System;

    using Codefarts.Localization;

    using UnityEditor;

    /// <summary>
    /// Provides methods for drawing screen hot settings.
    /// </summary>
    public class GeneralToolsSettings
    {
        /// <summary>
        /// Draws the screen shot setting controls.
        /// </summary>
        public static void Draw()
        {
            var local = LocalizationManager.Instance;

            SettingHelpers.DrawSettingsCheckBox(
                GlobalConstants.TextAssetEditingEnabled,
                local.Get("SETT_EnableTextAssetEditing"),
                true,
                value =>
                {
                    var platformTargets = Enum.GetValues(typeof(BuildTargetGroup)) as BuildTargetGroup[];
                    GeneralToolsSetup.SetupConditionalCompilation(platformTargets, new[] { "TEXTASSET_EDITOR" }, value);
                });

            SettingHelpers.DrawSettingsCheckBox(
                GlobalConstants.MonoScriptEditingEnabled,
                local.Get("SETT_EnableMonoScriptEditing"),
                true,
                value =>
                {
                    var platformTargets = Enum.GetValues(typeof(BuildTargetGroup)) as BuildTargetGroup[];
                    GeneralToolsSetup.SetupConditionalCompilation(platformTargets, new[] { "MONOSCRIPT_EDITOR" }, value);
                });
        }
    }
}