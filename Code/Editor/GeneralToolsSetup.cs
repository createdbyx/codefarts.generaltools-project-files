/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.GeneralTools.Editor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Codefarts.CoreProjectCode;
    using Codefarts.CoreProjectCode.Services;
    using Codefarts.GeneralTools.Editor.SceneViewAutoPan;
    using Codefarts.GeneralTools.Editor.TileSets;
    using Codefarts.GeneralTools.Interfaces;
    using Codefarts.Localization;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides menu items that integrate into unity's menu.
    /// </summary>
    [InitializeOnLoad]
    public class GeneralToolsSetup
    {
        /// <summary>
        /// Initializes static members of the <see cref="GeneralToolsSetup"/> class.
        /// </summary>
        static GeneralToolsSetup()
        {
            EditorCallbackService.Instance.Register(() =>
            {
                var settings = UnityPreferencesManager.Instance;
                var local = LocalizationManager.Instance;

                // register setting draw methods                              
                settings.Register(local.Get("SETT_TileMaterialCreation"), TileMaterialCreationGeneralSettings.Draw);
                settings.Register(local.Get("SETT_ScreenShots"), ScreenShotSettings.Draw);
                settings.Register(local.Get("SETT_GeneralTools"), GeneralToolsSettings.Draw);
                settings.Register(local.Get("SETT_Processors"), ProcessorSettings.Draw);
                settings.Register(local.Get("SETT_SceneViewAutoPan"), SceneViewAutoPanGeneralMenuItem.Draw);

                SetupTypeVisualizers();
            });
        }

        private static void SetupTypeVisualizers()
        {
            // register type visualizers
            var manager = TypeVisualizerManager.Instance;

            try
            {
#if UNITY_WEBPLAYER
                
#else
                foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (var type in assembly.GetTypes())
                    {
                        if (type.IsAbstract)
                        {
                            continue;                                                                          
                        }

                        foreach (var item in type.GetInterfaces())
                        {
                            if (item.FullName == typeof(ITypeVisualizer).FullName)
                            {
                                var reference = assembly.CreateInstance(type.FullName) as ITypeVisualizer;
                                manager.Register(reference);
                            }
                        }
                    }
                }

                Debug.Log("Number of registered type visualizers: " + manager.GetEnumerator().Count());
#endif
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        /// <summary>
        /// Adds conditional compilation symbols.
        /// </summary>
        /// <param name="platformTargets">
        /// The platform targets that will be updated.
        /// </param>
        /// <param name="symbolsToInclude">
        /// The symbols to include.
        /// </param>
        /// <param name="add">Use true to add the symbols and false to remove symbols.</param>
        public static void SetupConditionalCompilation(BuildTargetGroup[] platformTargets, string[] symbolsToInclude, bool add)
        {
            if (platformTargets == null || symbolsToInclude == null)
            {
                return;
            }

            foreach (var type in platformTargets)
            {
                var hasEntry = new bool[symbolsToInclude.Length];
                var conditionals = PlayerSettings.GetScriptingDefineSymbolsForGroup(type).Trim();
                var existingConditionals = new List<string>(conditionals.Split(';').Where(x => !string.IsNullOrEmpty(x.Trim())).Select(x => x));
                var changed = false;

                foreach (var part in existingConditionals)
                {
                    for (var i = 0; i < symbolsToInclude.Length; i++)
                    {
                        if (part.Trim() == symbolsToInclude[i].Trim())
                        {
                            hasEntry[i] = true;
                            break;
                        }
                    }
                }

                for (var i = 0; i < hasEntry.Length; i++)
                {
                    if (add && !hasEntry[i])
                    {
                        conditionals += (string.IsNullOrEmpty(conditionals) ? string.Empty : ";") + symbolsToInclude[i];
                    }

                    if (!add && hasEntry[i])
                    {
                        existingConditionals.Remove(symbolsToInclude[i]);
                    }

                    changed = true;
                }

                // if not adding generate a new conditionals string containing only the remaining items in the existingConditionals list
                if (!add)
                {
                    conditionals = string.Join(";", existingConditionals.ToArray());
                }

                PlayerSettings.SetScriptingDefineSymbolsForGroup(type, conditionals);

                if (changed)
                {
                    Debug.Log(string.Format("Updated player conditional compilation symbols for {0}: {1}", type, conditionals));
                }
            }
        }
    }
}