﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>


namespace Assets.Codefarts_Game.GeneralTools.Code.Editor.Common
{
 using System;
using System.IO;
using Codefarts.CoreProjectCode.Settings;
using Codefarts.GeneralTools.Editor;                
using UnityEditor;
using UnityEngine;

   public static class Texture2DExtensionMethods
    {
        /// <summary>
        /// Invokes a callback for each pixel.
        /// </summary>
        /// <param name="image">
        /// The source texture.
        /// </param>
        /// <param name="callback">The callback that will be invoked for each pixel.</param>
         /// <remarks><p>Invokes the callback with each pixel location going from top to bottom/left to right.</p>
        /// <p>The callback takes the arguments (Source Image, X, Y).</p></remarks>
       public static void ForEach(this Texture2D image, Action<Texture2D, int, int> callback)
        {
            for (var y = 0; y < image.height; y++)
            {
                for (var x = 0; x < image.width; x++)
                {
                    callback(image, x, y);
                }
            }
        }

        public static bool TryCreateReadableTexture(this Texture  sourceTexture, out Texture readableTexture)
        {
            // if no texture provided just exit
            if (sourceTexture == null)
            {
                readableTexture = null;
                return false;
            }

            // get the file path to the source texture file
            var file = AssetDatabase.GetAssetPath(sourceTexture);

            // if no file returned just exit
            if (string.IsNullOrEmpty(file))
            {
                readableTexture = null;
                return false;
            }

            // get the directory that the file is in
            var filePath = Path.GetDirectoryName(file);

            var extension = SettingsManager.Instance.GetSetting(GlobalConstants.ReadableTextureProcessor, "_NewFileNameTempPewpzIRReadableTex");

            // construct a temp filename for the new readable texture file
            var tempFile = Path.Combine(filePath, Path.GetFileNameWithoutExtension(file) + extension);
            tempFile = Path.ChangeExtension(tempFile, Path.GetExtension(file));

            try
            {
                // if the temp filename already exists delete it
                if (File.Exists(tempFile))
                {
                    AssetDatabase.DeleteAsset(tempFile);
                }

                // make a copy of the file source texture file
                File.Copy(file, tempFile);

                // import the new temp asset texture
                AssetDatabase.ImportAsset(tempFile, ImportAssetOptions.Default);

                // attempt to load the new temporary texture asset
                readableTexture = AssetDatabase.LoadAssetAtPath(tempFile, typeof(Texture )) as Texture ;

                return true;
                // // attempt to convert the readable texture into a generic image type
                // readableTexture = tmpTex.ToGenericImage();

                // // destroy the temp texture reference
                //UnityEngine.Object.DestroyImmediate(tmpTex, true);
            }
            catch (Exception)
            {
                // catch any errors that may have occurred during processing
                try
                {
                    // try to ensure that the temp texture asset is removed
                    AssetDatabase.DeleteAsset(tempFile);
                }
                catch
                {
                }
            }

            try
            {
                // if the temp texture file exists attempt to delete it
                if (File.Exists(tempFile))
                {
                    // try to ensure that the temp texture asset is removed
                    AssetDatabase.DeleteAsset(tempFile);
                }
            }
            catch
            {
            }

            readableTexture = null;
            return false;
        }
    }
}
