﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.GeneralTools.Common
{
    using UnityEngine;

    /// <summary>
    /// Extension methods for the <see cref="Color"/> type.
    /// </summary>
    public static class ColorExtensionMethods
    {
        #region Public Methods and Operators

        /// <summary>
        /// Blends two colors together.
        /// </summary>
        /// <param name="color">
        /// The source color.
        /// </param>
        /// <param name="src">
        /// The blend color.
        /// </param>
        /// <returns>
        /// Returns the result as a <see cref="Color"/> type.
        /// </returns>
        /// <remarks>
        /// From Wikipedia https://en.wikipedia.org/wiki/Alpha_compositing -> "Alpha blending" 
        /// </remarks>
        public static Color32 Blend(this Color32 color, Color32 src)
        {
            var sr = src.r / 255f;
            var sg = src.g / 255f;
            var sb = src.b / 255f;
            var sa = src.a / 255f;
            var dr = color.r / 255f;
            var dg = color.g / 255f;
            var db = color.b / 255f;
            var da = color.a / 255f;

            var oa = sa + (da * (1 - sa));
            var r = ((sr * sa) + ((dr * da) * (1 - sa))) / oa;
            var g = ((sg * sa) + ((dg * da) * (1 - sa))) / oa;
            var b = ((sb * sa) + ((db * da) * (1 - sa))) / oa;
            var a = oa;

            return new Color((byte)(r * 255), (byte)(g * 255), (byte)(b * 255), (byte)(a * 255));
        }

        #endregion
    }
}