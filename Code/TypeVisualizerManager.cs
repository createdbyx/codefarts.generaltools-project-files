﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Codefarts.GeneralTools.Interfaces;
    using Codefarts.GeneralTools.TypeVisualizers;

    /// <summary>
    /// Provides a manager class for <see cref="ITypeVisualizer"/> implementations.
    /// </summary>
    public class TypeVisualizerManager
    {
        /// <summary>
        /// private singleton instance of the <see cref="TypeVisualizerManager"/> class.
        /// </summary>
        private static TypeVisualizerManager instance;

        /// <summary>
        /// Used to prevent cross threading access.
        /// </summary>
        private readonly static object lockObject = new object();

        /// <summary>
        /// Holds the registered visualizers.
        /// </summary>
        private Dictionary<string, IList<ITypeVisualizer>> visualizers = new Dictionary<string, IList<ITypeVisualizer>>();

        /// <summary>
        /// Gets the visualizer.
        /// </summary>
        /// <param name="type">The type associated with the visualizer.</param>
        /// <returns>A reference to a <see cref="ITypeVisualizer"/> implementation.</returns>
        public ITypeVisualizer GetVisualizer(string type)
        {
            return this.visualizers.ContainsKey(type) ? this.visualizers[type].FirstOrDefault() : null;
        }

        /// <summary>
        /// Gets the visualizer.
        /// </summary>
        /// <param name="type">The type associated with the visualizer.</param>
        /// <returns>A reference to a <see cref="ITypeVisualizer"/> implementation.</returns>
        public IList<ITypeVisualizer> GetVisualizers(string type)
        {
            return this.visualizers.ContainsKey(type) ? this.visualizers[type] : null;
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, IList<ITypeVisualizer>>> GetEnumerator()
        {
            foreach (var pair in this.visualizers)
            {
                yield return pair;
            }
        }

        /// <summary>
        /// Registers a <see cref="ITypeVisualizer"/> implementation.
        /// </summary>
        /// <param name="visualizer">A reference to a type visualizer.</param>
        public void Register(ITypeVisualizer visualizer)
        {
            if (visualizer == null)
            {
                throw new ArgumentNullException("visualizer");
            }

            var renderers = (TypeVisualizerAttribute[])visualizer.GetType().GetCustomAttributes(typeof(TypeVisualizerAttribute), false);
            foreach (var item in renderers)
            {
                IList<ITypeVisualizer> list;
                if (!this.visualizers.ContainsKey(item.FullName))
                {
                    list = new List<ITypeVisualizer>();
                    this.visualizers[item.FullName] = list;
                }

                list = this.visualizers[item.FullName];
                if (!list.Contains(visualizer))
                {
                    list.Add(visualizer);
                }
            }

            //var local = LocalizationManager.Instance;

            //// ensure visualizer reference is not null 
            //if (visualizer == null)
            //{
            //    throw new ArgumentNullException("visualizer");
            //}

            //// check if visualizer Type property has a value
            //if (visualizer.Type == null)
            //{
            //    throw new NullReferenceException(local.Get("ERR_VisualizerTypePropertyIsNull"));
            //}

            //this.visualizers[visualizer.Type] = visualizer;
        }

        /// <summary>
        ///  Unregister a <see cref="ITypeVisualizer"/> implementation.
        /// </summary>
        /// <param name="visualizer">A reference to a type visualizer.</param>
        public void UnRegister(ITypeVisualizer visualizer)
        {
            //  var local = LocalizationManager.Instance;

            // ensure visualizer reference is not null 
            if (visualizer == null)
            {
                throw new ArgumentNullException("visualizer");
            }

            var renderers = (TypeVisualizerAttribute[])visualizer.GetType().GetCustomAttributes(typeof(TypeVisualizerAttribute), false);
            foreach (var item in renderers)
            {
                IList<ITypeVisualizer> list;
                if (!this.visualizers.ContainsKey(item.FullName))
                {
                    break;
                }

                list = this.visualizers[item.FullName];
                list.Remove(visualizer);
                if (list.Count == 0)
                {
                    this.visualizers.Remove(item.FullName);
                }
            }

            //// check if visualizer Type property has a value
            //if (visualizer.Type == null)
            //{
            //    throw new NullReferenceException(local.Get("ERR_VisualizerTypePropertyIsNull"));
            //}

            //// remove the visualizer
            //this.visualizers.Remove(visualizer.Type);
        }

        /// <summary>
        /// Gets a singleton instance of <see cref="TypeVisualizerManager"/>.
        /// </summary>
        /// <value>
        /// The singleton instance.
        /// </value>
        public static TypeVisualizerManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        instance = new TypeVisualizerManager();
                    }
                }

                return instance;
            }
        }
    }
}
