namespace Codefarts.GeneralTools.TypeVisualizers
{
    using Codefarts.GeneralTools.Interfaces;             
    using Codefarts.UIControls;

    /// <summary>
    /// Provides a base class implementation for type visualizer controls that render themselves.
    /// </summary>
    /// <typeparam name="T">The type that is to be visualized.</typeparam>
    public abstract class ControlVisualizerBase : Control, ITypeVisualizer
    {
        #region Implementation of ITypeVisualizer

        /// <summary>
        /// Creates the control that is intended to visualize the type.
        /// </summary>
        /// <returns>
        /// A new control reference of the desired type.
        /// </returns>
        public abstract ControlVisualizerBase CreateControl();

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public abstract void SetData(object data);

        #endregion

        #region Overrides of Control

        /// <summary>Gets the default size of the control.</summary>
        /// <returns>The default <see cref="Control.Size" /> of the control.</returns>
        protected override Size DefaultSize
        {
            get
            {
                return new Size(200, 75);
            }
        }

        #endregion
    }
}