﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;
    using Codefarts.UIControls.Controls;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a visualizer for the unity <see cref="Texture2D"/> type.
    /// </summary>
    [TypeVisualizer(typeof(Texture2D))]
    public class UnityTexture2DVisualizer : UnityControlVisualizerBase<Texture2D>
    {
        private TextBlock label;
        private UnityObjectVisualizer objectVisualizer;
        private Image previewImage;

        #region Implementation of ITypeVisualizer

        public override ControlVisualizerBase CreateControl()
        {
            return new UnityTexture2DVisualizer();
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityTexture2DVisualizer"/> class.
        /// </summary>
        public UnityTexture2DVisualizer()
        {
            var stack = new StackPanel(Orientation.Vertical)
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = this.Width,
                Height = this.Height
            };

            this.label = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Width = this.Width,
            };

            this.objectVisualizer = new UnityObjectVisualizer();
            this.previewImage = new Image();
            stack.Controls.Add(this.objectVisualizer);
            stack.Controls.Add(this.label);
            stack.Controls.Add(this.previewImage);
            this.Controls.Add(stack);
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            this.objectVisualizer.SetData(data);
            var model = (Texture2D)data;
            var text = string.Empty;
            text += string.Format("Format: {0}\r\n", model.format);
            text += string.Format("Dimentions: {0}x{1}\r\n", model.width, model.height);
            text += string.Format("MipMap Count: {0}\r\n", model.mipmapCount);
            text += string.Format("anisoLevel: {0}\r\n", model.anisoLevel);
            text += string.Format("Filter Mode: {0}\r\n", model.filterMode);
            text += string.Format("mipMapBias: {0}\r\n", model.mipMapBias);
            text += string.Format("texelSize: {0}\r\n", model.texelSize);
            text += string.Format("Wrap Mode: {0}", model.wrapMode);
            this.label.Text = text;
            this.previewImage.Source = new Texture2DSource(model);
            this.previewImage.Stretch = Stretch.None;
        }
    }
}