﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;

    using UnityEngine;

    /// <summary>
    /// Provides a visualizer for the unity <see cref="Object"/> type.
    /// </summary>
    [TypeVisualizer(typeof(Object))]
    public class UnityObjectVisualizer : UnityControlVisualizerBase<Object>
    {
        private TextBlock label;

        #region Implementation of ITypeVisualizer

        public override ControlVisualizerBase CreateControl()
        {
            return new UnityObjectVisualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            this.label.Text = string.Format("Name: {0} InstanceID: {1}", this.Model.name, this.Model.GetInstanceID());
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityObjectVisualizer"/> class.
        /// </summary>
        public UnityObjectVisualizer()
        {
            this.label = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = this.Width,
                Height = this.Height
            };

            this.Controls.Add(this.label);
        }

    }
}
