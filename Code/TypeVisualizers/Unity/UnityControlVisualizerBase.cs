﻿namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    /// <summary>
    /// Provides a base class implementation for type visualizer controls that render themselves.
    /// </summary>
    /// <typeparam name="T">The type that is to be visualized.</typeparam>
    public abstract class UnityControlVisualizerBase<T> : ControlVisualizerBase
    {              
        /// <summary>
        /// Gets or sets the data to be visualized.
        /// </summary>                            
        public T Model { get; set; }
                                 
        #region Implementation of ITypeVisualizer
                      
        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            this.Model = (T)data;
        }

        #endregion
    }
}