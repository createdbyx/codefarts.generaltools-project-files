﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;

    using UnityEngine;

    /// <summary>
    /// Provides a visualizer for the unity <see cref="Shader"/> type.
    /// </summary>
    [TypeVisualizer(typeof(Shader))]
    public class UnityShaderVisualizer : UnityControlVisualizerBase<Shader>
    {
        private UnityObjectVisualizer objectVisualizer;
        private TextBlock label;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityShaderVisualizer"/> class.
        /// </summary>
        public UnityShaderVisualizer()
        {
            this.objectVisualizer = new UnityObjectVisualizer();
            this.label = new TextBlock();
            this.Controls.Add(this.objectVisualizer);
            this.Controls.Add(this.label);
        }

        #region Overrides of UnityControlVisualizerBase<Shader>

        /// <summary>
        /// Creates the control that is intended to visualize the type.
        /// </summary>
        /// <returns>
        /// A new control reference of the desired type.
        /// </returns>
        public override ControlVisualizerBase CreateControl()
        {
            return new UnityShaderVisualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            this.objectVisualizer.SetData(data);
            var model = (Shader)data;
            var text = string.Empty;
            text += string.Format("Is Supported: {0}\r\n", model.isSupported);
            text += string.Format("Maximum LOD: {0}\r\n", model.maximumLOD);
            text += string.Format("Render Queue: {0}", model.renderQueue);
            this.label.Text = text;
        }

        #endregion
    }
}