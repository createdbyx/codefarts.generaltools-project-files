﻿namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;

    using UnityEngine;

    /// <summary>
    /// Provides a visualizer for the unity <see cref="Vector2"/> type.
    /// </summary>
    [TypeVisualizer(typeof(Vector2))]
    public class UnityVector2Visualizer : UnityControlVisualizerBase<Vector2>
    {
        private TextBlock label;

        #region Implementation of ITypeVisualizer

        public override ControlVisualizerBase CreateControl()
        {
            return new UnityVector2Visualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            var model = this.Model;
            this.label.Text = string.Format("X:{0} Y:{1}", model.x, model.y);
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityVector2Visualizer"/> class.
        /// </summary>
        public UnityVector2Visualizer()
        {
            this.label = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = this.Width,
                Height = this.Height
            };

            this.Controls.Add(this.label);
        }       
    }
}