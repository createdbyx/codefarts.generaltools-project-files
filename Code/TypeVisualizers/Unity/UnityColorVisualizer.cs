﻿namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;
                             
    using Color = UnityEngine.Color;

    [TypeVisualizer(typeof(Color))]
    public class UnityColorVisualizer : UnityControlVisualizerBase<Color>
    {
        private TextBlock label;

        #region Implementation of ITypeVisualizer

        public override ControlVisualizerBase CreateControl()
        {
            return new UnityColorVisualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            var model = this.Model;
            this.label.Text = string.Format("R:{0} G:{1} B:{2} A:{3}", model.r, model.g, model.b, model.a);
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityColorVisualizer"/> class.
        /// </summary>
        public UnityColorVisualizer()
        {
            this.label = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = this.Width,
                Height = this.Height
            };

            this.Controls.Add(this.label);
        }
    }
}