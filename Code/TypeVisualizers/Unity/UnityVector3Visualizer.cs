namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;

    using UnityEngine;

    /// <summary>
    /// Provides a visualizer for the unity <see cref="Vector3"/> type.
    /// </summary>
    [TypeVisualizer(typeof(Vector3))]
    public class UnityVector3Visualizer : UnityControlVisualizerBase<Vector3>
    {
        private TextBlock label;

        #region Implementation of ITypeVisualizer

        public override ControlVisualizerBase CreateControl()
        {
            return new UnityVector3Visualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            var model = this.Model;
            this.label.Text = string.Format("X:{0} Y:{1} Z:{2}", model.x, model.y, model.z);
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityVector3Visualizer"/> class.
        /// </summary>
        public UnityVector3Visualizer()
        {
            this.label = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = this.Width,
                Height = this.Height
            };

            this.Controls.Add(this.label);
        }
    }
}