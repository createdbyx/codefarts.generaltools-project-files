﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;

    using UnityEngine;

    /// <summary>
    /// Provides a visualizer for the untiy <see cref="Mesh"/> type.
    /// </summary>
    [TypeVisualizer(typeof(Mesh))]
    public class UnityMeshVisualizer : UnityControlVisualizerBase<Mesh>
    {
        private UnityObjectVisualizer objectVisualizer;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityMeshVisualizer"/> class.
        /// </summary>
        public UnityMeshVisualizer()
        {
            this.objectVisualizer = new UnityObjectVisualizer();
        }

        #region Overrides of UnityControlVisualizerBase<Mesh>

        /// <summary>
        /// Call this method when the control needs to draw it self.
        /// </summary>
        /// <param name="args">The rendering arguments.</param>
        public   void OnDraw(ControlRenderingArgs args)
        {
            if (this.Model == null)
            {
                GUILayout.Label("No Mesh!");
                return;
            }

            GUILayout.BeginVertical();
            var model = this.Model;
          //  this.objectVisualizer.OnDraw(args);

            var vertexes = model.vertices;
            var normals = model.normals;
            var uv = model.uv;
            var uv2 = model.uv2;
            var uv3 = model.uv3;
            var colors = model.colors;
            var tangents = model.tangents;
            var triangles = model.triangles;

            var labelHeight = GUI.skin.label.CalcSize(new GUIContent("Tj")).y;
            GUILayout.Label(string.Format("Sub Meshes: {0}", model.subMeshCount));
            for (var i = 0; i < model.subMeshCount; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(labelHeight);
                GUILayout.Label(string.Format("{0}: Faces: {1}", i, model.GetIndices(i).Length / 3));
                GUILayout.EndHorizontal();
            }

            // display topology
            GUILayout.Label("Topology Data");
            for (var i = 0; i < model.subMeshCount; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(labelHeight);
                GUILayout.Label(string.Format("{0}: Topology: {1}", i, model.GetTopology(i)));
                GUILayout.EndHorizontal();
            }

            GUILayout.Label(string.Format("Vertexes: {0}", vertexes.Length));
            GUILayout.Label(string.Format("Normals: {0}", normals.Length));
            GUILayout.Label(string.Format("Uv: {0}", uv.Length));
            GUILayout.Label(string.Format("Uv2: {0}", uv2.Length));
            GUILayout.Label(string.Format("Uv3: {0}", uv3.Length));
            GUILayout.Label(string.Format("Colors: {0}", colors.Length));
            GUILayout.Label(string.Format("Tangents: {0}", tangents.Length));
            GUILayout.Label(string.Format("Triangles: {0}", triangles.Length / 3));
            GUILayout.Label(string.Format("Blend Shapes: {0}", model.blendShapeCount));

            GUILayout.EndVertical();
        }

        #endregion

        #region Overrides of UnityControlVisualizerBase<Mesh>

        /// <summary>
        /// Creates the control that is intended to visualize the type.
        /// </summary>
        /// <returns>
        /// A new control reference of the desired type.
        /// </returns>
        public override ControlVisualizerBase CreateControl()
        {
            return new UnityMeshVisualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            this.objectVisualizer.SetData(data);
        }

        #endregion
    }
}