﻿namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;

    using UnityEngine;

    [TypeVisualizer(typeof(Color32))]
    public class UnityColor32Visualizer : UnityControlVisualizerBase<Color32>
    {
        private TextBlock label;

        #region Implementation of ITypeVisualizer

        public override ControlVisualizerBase CreateControl()
        {
            return new UnityColor32Visualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            var model = this.Model;
            this.label.Text = string.Format("R:{0} G:{1} B:{2} A:{3}", model.r, model.g, model.b, model.a);
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityColor32Visualizer"/> class.
        /// </summary>
        public UnityColor32Visualizer()
        {
            this.label = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = this.Width,
                Height = this.Height
            };

            this.Controls.Add(this.label);
        }
    }
}