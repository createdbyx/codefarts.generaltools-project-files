namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;

    using UnityEngine;

    /// <summary>
    /// Provides a visualizer for the unity <see cref="Vector4"/> type.
    /// </summary>
    [TypeVisualizer(typeof(Vector4))]
    public class UnityVector4Visualizer : UnityControlVisualizerBase<Vector4>
    {
        private TextBlock label;

        #region Implementation of ITypeVisualizer

        public override ControlVisualizerBase CreateControl()
        {
            return new UnityVector4Visualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            var model = this.Model;
            this.label.Text = string.Format("X:{0} Y:{1} Z:{2} W:{3}", model.x, model.y, model.z, model.w);
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityVector4Visualizer"/> class.
        /// </summary>
        public UnityVector4Visualizer()
        {
            this.label = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = this.Width,
                Height = this.Height
            };

            this.Controls.Add(this.label);
        }
    }
}