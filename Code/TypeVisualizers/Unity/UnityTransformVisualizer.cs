﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;

    using UnityEngine;

    /// <summary>
    /// Provides a visualizer for the untiy <see cref="Transform"/> type.
    /// </summary>
    [TypeVisualizer(typeof(Transform))]
    public class UnityTransformVisualizer : UnityControlVisualizerBase<Transform>
    {
        private Grid gridControl;

        private NumericTextField positionX;
        private NumericTextField positionY;
        private NumericTextField positionZ;
        private NumericTextField rotationX;
        private NumericTextField rotationY;
        private NumericTextField rotationZ;
        private NumericTextField scaleX;
        private NumericTextField scaleY;
        private NumericTextField scaleZ;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityTransformVisualizer" /> class.
        /// </summary>
        public UnityTransformVisualizer()
        {
            this.gridControl = new Grid()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = this.Width,
                Height = this.Height,
                MaximumSize = new Size(0, (16 * 3) + 16),
                Rows = 3,
                Columns = 7
            };

            this.gridControl.ColumnDefinitions[0].Width = 60;
            this.gridControl.ColumnDefinitions[1].Width = 20;
            this.gridControl.ColumnDefinitions[3].Width = 20;
            this.gridControl.ColumnDefinitions[5].Width = 20;

            this.gridControl.RowDefinitions[0].MaxHeight = 16;
            this.gridControl.RowDefinitions[1].MaxHeight = 16;
            this.gridControl.RowDefinitions[2].MaxHeight = 16;
            this.gridControl.RowDefinitions[0].Height = 16;
            this.gridControl.RowDefinitions[1].Height = 16;
            this.gridControl.RowDefinitions[2].Height = 16;

            this.gridControl.SetCell(0, 0, new TextBlock("Position"));
            this.gridControl.SetCell(1, 0, new TextBlock(" X: "));
            this.positionX = new NumericTextField();
            this.gridControl.SetCell(2, 0, this.positionX);
            this.gridControl.SetCell(3, 0, new TextBlock(" Y: "));
            this.positionY = new NumericTextField();
            this.gridControl.SetCell(4, 0, this.positionY);
            this.gridControl.SetCell(5, 0, new TextBlock(" Z: "));
            this.positionZ = new NumericTextField();
            this.gridControl.SetCell(6, 0, this.positionZ);

            this.gridControl.SetCell(0, 1, new TextBlock("Rotation"));
            this.gridControl.SetCell(1, 1, new TextBlock(" X: "));
            this.rotationX = new NumericTextField();
            this.gridControl.SetCell(2, 1, this.rotationX);
            this.gridControl.SetCell(3, 1, new TextBlock(" Y: "));
            this.rotationY = new NumericTextField();
            this.gridControl.SetCell(4, 1, this.rotationY);
            this.gridControl.SetCell(5, 1, new TextBlock(" Z: "));
            this.rotationZ = new NumericTextField();
            this.gridControl.SetCell(6, 1, this.rotationZ);

            this.gridControl.SetCell(0, 2, new TextBlock("Scale"));
            this.gridControl.SetCell(1, 2, new TextBlock(" X: "));
            this.scaleX = new NumericTextField();
            this.gridControl.SetCell(2, 2, this.scaleX);
            this.gridControl.SetCell(3, 2, new TextBlock(" Y: "));
            this.scaleY = new NumericTextField();
            this.gridControl.SetCell(4, 2, this.scaleY);
            this.gridControl.SetCell(5, 2, new TextBlock(" Z: "));
            this.scaleZ = new NumericTextField();
            this.gridControl.SetCell(6, 2, this.scaleZ);

            this.Controls.Add(this.gridControl);
        }

      //  #region Overrides of UnityControlVisualizerBase<Transform>

        ///// <summary>
        ///// Call this method when the control needs to draw it self.
        ///// </summary>
        ///// <param name="args">The rendering arguments.</param>
        //public override void OnDraw(ControlRenderingArgs args)
        //{
        //    if (this.Model == null)
        //    {
        //        GUILayout.Label("No Transform!");
        //        return;
        //    }

        //    //GUILayout.BeginVertical();
        //    GUILayout.Label(string.Empty, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        //    var rect = GUILayoutUtility.GetLastRect();
        //    //var rect = GUILayoutUtility.GetRect(0, int.MaxValue, 0, int.MaxValue);
        //    var model = this.Model;

        //    if (model != null)
        //    {
        //        // GUILayoutUtility.BeginGroup("gc");
        //        GUI.BeginGroup(rect);
        //        this.gridControl.Location = Point.Empty;
        //        this.gridControl.Size = rect.size;
        //        args.Manager.DrawControl(this.gridControl, args.ElapsedGameTime, args.TotalGameTime);
        //        GUI.EndGroup();
        //        //model.localPosition = Vector3Field("Position", model.localPosition);
        //        //model.localEulerAngles = Vector3Field("Rotation", model.localEulerAngles);
        //        //model.localScale = Vector3Field("Scale", model.localScale);
        //        // GUILayoutUtility.EndGroup("gc");
        //    }
        //    else
        //    {
        //        GUILayout.Label("<Bad data!>");
        //    }

        //    //GUILayout.EndVertical();
        //}

       // #endregion        

        #region Overrides of UnityControlVisualizerBase<Transform>

        /// <summary>
        /// Creates the control that is intended to visualize the type.
        /// </summary>
        /// <returns>
        /// A new control reference of the desired type.
        /// </returns>
        public override ControlVisualizerBase CreateControl()
        {
            return new UnityTransformVisualizer();
        }

        #endregion
    }
}