﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.TypeVisualizers.Unity
{
    using Codefarts.UIControls;

    using UnityEngine;

    /// <summary>
    /// Provides a visualizer for the untiy <see cref="Component"/> type.
    /// </summary>
    [TypeVisualizer(typeof(Component))]
    public class UnityComponentVisualizer : UnityControlVisualizerBase<Component>
    {
        private UnityObjectVisualizer objectVisualizer;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityComponentVisualizer"/> class.
        /// </summary>
        public UnityComponentVisualizer()
        {
            this.objectVisualizer = new UnityObjectVisualizer();
            this.Controls.Add(this.objectVisualizer);
            this.Controls.Add(new TextBlock("Component info here"));
        }


        #region Overrides of UnityControlVisualizerBase<Component>

        /// <summary>
        /// Creates the control that is intended to visualize the type.
        /// </summary>
        /// <returns>
        /// A new control reference of the desired type.
        /// </returns>
        public override ControlVisualizerBase CreateControl()
        {
            return new UnityComponentVisualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            this.objectVisualizer.SetData(data);
        }

        #endregion
    }
}