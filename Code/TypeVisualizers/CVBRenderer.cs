namespace Codefarts.GeneralTools.TypeVisualizers
{
    using Codefarts.UIControls;
    using Codefarts.UIControls.Renderers;

    using UnityEngine;

    [ControlRenderer(typeof(ControlVisualizerBase))]
    public class CVBRenderer : BaseRenderer
    {
        #region Overrides of BaseRenderer

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var controls = args.Control.Controls;
            var rect = new Rect(args.Control.Left, args.Control.Top, args.Control.Width, args.Control.Height);

            // draw background
            var brush = args.Control.Background;
            if (brush != null)
            {
                BrushExtensions.Draw(brush, rect);
            }

            if (controls != null)
            {
                using (new GUI.GroupScope(rect))
                {
                    foreach (var control in controls)
                    {
                        args.Manager.DrawControl(control, args.ElapsedGameTime, args.TotalGameTime);
                    }
                }
            }
        }

        #endregion
    }
}