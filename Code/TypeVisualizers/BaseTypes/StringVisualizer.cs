namespace Codefarts.GeneralTools.TypeVisualizers.BaseTypes
{
    using Codefarts.GeneralTools.TypeVisualizers.Unity;
    using Codefarts.UIControls;
                                      
    [TypeVisualizer(typeof(string))]
    public class StringVisualizer : UnityControlVisualizerBase<string>
    {
        private TextBlock label;

        #region Implementation of ITypeVisualizer

        public override ControlVisualizerBase CreateControl()
        {
            return new StringVisualizer();
        }

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        public override void SetData(object data)
        {
            base.SetData(data);
            this.label.Text = this.Model;
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="StringVisualizer"/> class.
        /// </summary>
        public StringVisualizer()
        {
            this.label = new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = this.Width,
                Height = this.Height
            };

            this.Controls.Add(this.label);
        }
    }
}