﻿namespace Codefarts.GeneralTools.TypeVisualizers.UIControls
{
    using Codefarts.GeneralTools.TypeVisualizers.Unity;
    using Codefarts.UIControls;

    [TypeVisualizer(typeof(Control))]
    public class ControlVisualizer : UnityControlVisualizerBase<Control>
    {
        private TextBlock infoLabel;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlVisualizer"/> class.
        /// </summary>
        public ControlVisualizer() 
        {
            this.infoLabel = new TextBlock() { HorizontalAlignment = HorizontalAlignment.Stretch };
            this.Controls.Add(this.infoLabel);
        }

        #region Implementation of ITypeVisualizer

        public override ControlVisualizerBase CreateControl()
        {
            return new ControlVisualizer();
        }

        public void SetData(object data)
        {
            var model = (Control)data;
            this.infoLabel.Text = model.ToString();
        }

        #endregion
    }
}