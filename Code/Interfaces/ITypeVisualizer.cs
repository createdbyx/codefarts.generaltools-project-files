﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.Interfaces
{
    using Codefarts.GeneralTools.TypeVisualizers;

    /// <summary>
    /// Provides a interface for type visualization.
    /// </summary>
    public interface ITypeVisualizer
    {
        /// <summary>
        /// Creates the control that is intended to visualize the type.
        /// </summary>
        /// <returns>A new control reference of the desired type.</returns>
        ControlVisualizerBase CreateControl();

        /// <summary>
        /// Sets the data that the visualizer is supposed to render.
        /// </summary>
        /// <param name="data">The data to be passed.</param>
        void SetData(object data);
    }
}
