﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.GeneralTools
{
    /// <summary>
    /// Provides a enumeration that defines the type of sync.
    /// </summary>
    public enum CodeSyncType
    {
        /// <summary>
        /// A individual file sync.
        /// </summary>
        File,

        /// <summary>
        /// A folder sync.
        /// </summary>
        Folder,

        /// <summary>
        /// A visual studio project file sync.
        /// </summary>
        Project,
    }
}