﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.GeneralTools.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines a model for a individual sync operation.
    /// </summary>
    public class SyncModel
    {
        /// <summary>
        /// Holds the value for the <see cref="Destination"/> property.
        /// </summary>
        private string destination;

        /// <summary>
        /// Holds the value for the <see cref="Source"/> property.
        /// </summary>
        private string source;

        /// <summary>
        /// Holds the value for the <see cref="SyncType"/> property.
        /// </summary>
        private CodeSyncType syncType;

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncModel"/> class.
        /// </summary>
        public SyncModel()
        {
            this.Filters = new List<FilterModel>();
            this.source = string.Empty;
            this.destination = string.Empty;
        }

        /// <summary>
        /// Gets or sets the destination file or folder that will be synced from the source.
        /// </summary>
        /// <remarks>When assigning a value if the value is different then the existing value the <see cref="IsDirty"/> property will be set to true.</remarks>
        public string Destination
        {
            get
            {
                return this.destination;
            }

            set
            {
                if (value == this.destination)
                {
                    return;
                }

                this.destination = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a callback that will be used to draw the <see cref="Destination"/> property.
        /// </summary>
        public Action<int, int, SyncModel> DestinationCallback { get; set; }

        /// <summary>
        /// Gets or sets a callback that will be used to draw the <see cref="Filters"/> property.
        /// </summary>
        public Action<int, int, SyncModel> FilterCallback { get; set; }

        /// <summary>
        /// Gets or sets a <see cref="IList{T}"/> of <see cref="FilterModel"/> types.
        /// </summary>
        public IList<FilterModel> Filters { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the model has under gone changes.
        /// </summary>
        public bool IsDirty { get; set; }

        /// <summary>
        /// Gets or sets a callback that will be used to draw a remove button.
        /// </summary>
        public Action<int, int, SyncModel> RemoveCallback { get; set; }

        /// <summary>
        /// Gets or sets the source file or folder that will be synced to the destination.
        /// </summary>
        /// <remarks>When assigning a value if the value is different then the existing value the <see cref="IsDirty"/> property will be set to true.</remarks>
        public string Source
        {
            get
            {
                return this.source;
            }

            set
            {
                if (value == this.source)
                {
                    return;
                }

                this.source = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a callback that will be used to draw the <see cref="Source"/> property.
        /// </summary>
        public Action<int, int, SyncModel> SourceCallback { get; set; }

        /// <summary>
        /// Gets or sets the type of sync this model performs.
        /// </summary>
        /// <remarks>When assigning a value if the value is different then the existing value the <see cref="IsDirty"/> property will be set to true.</remarks>
        public CodeSyncType SyncType
        {
            get
            {
                return this.syncType;
            }

            set
            {
                if (value == this.syncType)
                {
                    return;
                }

                this.syncType = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a callback that will be used to draw the <see cref="SyncType"/> property.
        /// </summary>
        public Action<int, int, SyncModel> SyncTypeCallback { get; set; }
    }
}