﻿namespace Codefarts.GeneralTools.Models
{
    using System;

    using UnityEngine;

    using Object = UnityEngine.Object;

    [Serializable]
    public class AssetPathModel : ScriptableObject
    {
        [SerializeField]  
        public string Path;

        [SerializeField]
        public string Type;

        [SerializeField]
        public Object Reference;
    }
}
