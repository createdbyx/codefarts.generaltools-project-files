﻿namespace Codefarts.GeneralTools.Models
{
    using System;

    using UnityEngine;

    [Serializable]
    public class AssetPathCollectionModel : ScriptableObject
    {
        [SerializeField]
        public AssetPathModel[] Models;
    }
}