﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.GeneralTools.Models
{
    using System;

    /// <summary>
    /// Provides a filter model for filtering out and replacing content within files and folders.
    /// </summary>
    public class FilterModel
    {
        /// <summary>
        /// Holds the value for the <see cref="Extension"/> property.
        /// </summary>
        private string extension;

        /// <summary>
        /// Holds the value for the <see cref="Replace"/> property.
        /// </summary>
        private string replace;

        /// <summary>
        /// Holds the value for the <see cref="Search"/> property.
        /// </summary>
        private string search;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterModel"/> class.
        /// </summary>
        public FilterModel()
        {
            this.Extension = string.Empty;
            this.Search = string.Empty;
            this.Replace = string.Empty;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the file or folder will be allowed to sync.
        /// </summary>
        public bool Allow { get; set; }

        /// <summary>
        /// Gets or sets a file extension that this filter works against.
        /// </summary>
        /// <remarks>When assigning a value if the value is different then the existing value the <see cref="IsDirty"/> property will be set to true.</remarks>
        public string Extension
        {
            get
            {
                return this.extension;
            }

            set
            {
                if (this.extension == value)
                {
                    return;
                }

                this.extension = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the model has been changed.
        /// </summary>
        public bool IsDirty { get; set; }

        /// <summary>
        /// Gets or sets a callback that is responsible for drawing a remove button.
        /// </summary>
        public Action<int, int, FilterModel> RemoveCallback { get; set; }

        /// <summary>
        /// Gets or sets a value that will be used to replace content that was matched with <see cref="Search"/>.
        /// </summary>
        /// <remarks>When assigning a value if the value is different then the existing value the <see cref="IsDirty"/> property will be set to true.</remarks>
        public string Replace
        {
            get
            {
                return this.replace;
            }

            set
            {
                if (this.replace == value)
                {
                    return;
                }

                this.replace = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a value that will be used to search for content within the file.
        /// </summary>
        /// <remarks>When assigning a value if the value is different then the existing value the <see cref="IsDirty"/> property will be set to true.</remarks>
        public string Search
        {
            get
            {
                return this.search;
            }

            set
            {
                if (this.search == value)
                {
                    return;
                }

                this.search = value;
                this.IsDirty = true;
            }
        }
    }
}