namespace Codefarts.GeneralTools.Utilities.Models
{
    using UnityEngine;

    /// <summary>
    /// Used to hold texture pixel data.
    /// </summary>
    public struct PixelsContainer
    {
        /// <summary>
        /// The colors that represent the texture data.
        /// </summary>
        public Color[] colors;

        /// <summary>
        /// The width of the texture data.
        /// </summary>
        public int Width;

        /// <summary>
        /// The height of the texture data.
        /// </summary>
        public int Height;
    }
}