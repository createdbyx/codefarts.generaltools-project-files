﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.Debugging
{
    using UnityEngine;

    [ExecuteInEditMode]
    public class MeshNormalsVisualizer : MonoBehaviour
    {
        public float scaleNormals = 1;

        public Color color = Color.white;

        private void OnRenderObject()
        {
            if (!this.enabled)
            {
                return;
            }

            var meshFilter = this.GetComponent<MeshFilter>();
            if (meshFilter == null)
            {
                return;
            }

            var mesh = meshFilter.sharedMesh != null ? meshFilter.sharedMesh : meshFilter.mesh;
            if (mesh == null)
            {
                return;
            }

            var vertexes = mesh.vertices;
            var normals = mesh.normals;

            GL.PushMatrix();
            GL.MultMatrix(Matrix4x4.TRS(this.transform.position, this.transform.rotation, Vector3.one));
            GL.Begin(GL.LINES);
            GL.Color(this.color);

            for (var i = 0; i < vertexes.Length; i++)
            {
                var vertex = vertexes[i];
                var normal = vertex + (normals[i] * this.scaleNormals);
                GL.Vertex3(vertex.x, vertex.y, vertex.z);
                GL.Vertex3(normal.x, normal.y, normal.z);
            }

            GL.End();
            GL.PopMatrix();
        }
    }
}
