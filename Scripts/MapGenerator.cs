// Source: Sebastian Lague (Youtube channel)
// Youtube: https://www.youtube.com/watch?v=v7yyZZjF1z4&list=PLQBrCLoduX3_wq4orgVAI4ytLqyrug9Q3
namespace Codefarts.GeneralTools.Scripts
{
    using UnityEngine;

    public class MapGenerator : MonoBehaviour
    {                           
        public int width;
        public int height;

        public string seed;
        public bool useRandomSeed;

        [Range(0, 100)]
        public int randomFillPercent;

        int[,] map;

        [Range(0, 100)]
        private int smoothingPasses = 5;

        private void Start()
        {
            this.GenerateMap();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.GenerateMap();
            }
        }

        private void GenerateMap()
        {
            this.map = new int[this.width, this.height];
            this.RandomFillMap();

            for (var i = 0; i < this.smoothingPasses; i++)
            {
                this.SmoothMap();
            }
        }


        private void RandomFillMap()
        {
            if (this.useRandomSeed)
            {
                this.seed = Time.time.ToString();
            }

            var pseudoRandom = new System.Random(this.seed.GetHashCode());

            for (var x = 0; x < this.width; x++)
            {
                for (var y = 0; y < this.height; y++)
                {
                    if (x == 0 || x == this.width - 1 || y == 0 || y == this.height - 1)
                    {
                        this.map[x, y] = 1;
                    }
                    else
                    {
                        this.map[x, y] = (pseudoRandom.Next(0, 100) < this.randomFillPercent) ? 1 : 0;
                    }
                }
            }
        }

        private void SmoothMap()
        {
            for (var x = 0; x < this.width; x++)
            {
                for (var y = 0; y < this.height; y++)
                {
                    var neighbourWallTiles = this.GetSurroundingWallCount(x, y);

                    if (neighbourWallTiles > 4)
                    {
                        this.map[x, y] = 1;
                    }
                    else if (neighbourWallTiles < 4)
                    {
                        this.map[x, y] = 0;
                    }

                }
            }
        }

        private int GetSurroundingWallCount(int gridX, int gridY)
        {
            var wallCount = 0;
            for (var neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
            {
                for (var neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
                {
                    if (neighbourX >= 0 && neighbourX < this.width && neighbourY >= 0 && neighbourY < this.height)
                    {
                        if (neighbourX != gridX || neighbourY != gridY)
                        {
                            wallCount += this.map[neighbourX, neighbourY];
                        }
                    }
                    else
                    {
                        wallCount++;
                    }
                }
            }

            return wallCount;
        }


        private void OnDrawGizmos()
        {
            if (this.map != null)
            {
                for (var x = 0; x < this.width; x++)
                {
                    for (var y = 0; y < this.height; y++)
                    {
                        Gizmos.color = (this.map[x, y] == 1) ? Color.black : Color.white;
                        var pos = new Vector3(-this.width / 2 + x + .5f, 0, -this.height / 2 + y + .5f);
                        Gizmos.DrawCube(pos, Vector3.one);
                    }
                }
            }
        }

    }
}