﻿namespace Codefarts.GeneralTools.Scripts
{
    using System;
    using System.Globalization;

    using UnityEngine;

    [RequireComponent(typeof(IndexedPrefabs))]
    public class IndexedPrefabTester : MonoBehaviour
    {
        private IndexedPrefabs prefabs;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.prefabs = this.GetComponent<IndexedPrefabs>();
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            var data = "Has Prefabs: " + this.prefabs.Prefabs != null && this.prefabs.Prefabs.Models != null
                       && this.prefabs.Prefabs.Models.Length > 0
                           ? "true"
                           : "false";

            if (this.prefabs.Prefabs != null && this.prefabs.Prefabs.Models != null && this.prefabs.Prefabs.Models != null)
            {
                foreach (var model in this.prefabs.Prefabs.Models)
                {
                    if (model == null)
                    {
                        data += "\r\nNull modell reference";
                    }
                    else
                    {
                        data += string.Format(
                            "\r\nType: {0}" + "\r\nPath: {1}" + "\r\nReference Type: {2}\r\nInstanceID: {3}",
                            model.Type,
                            model.Path,
                            model.Reference == null ? "null" : model.Reference.GetType().FullName,
                            model.Reference == null ? "null" : model.Reference.GetInstanceID().ToString(CultureInfo.InvariantCulture));
                    }
                }
            }

            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), data);
        }
    }
}