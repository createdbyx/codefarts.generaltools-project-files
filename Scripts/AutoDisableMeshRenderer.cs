// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.Scripts
{
    using UnityEngine;

    [RequireComponent(typeof(MeshRenderer))]
    public class AutoDisableMeshRenderer : MonoBehaviour
    {
        private float lastShown;

        [SerializeField]
        private float hideDelay;

        private Renderer meshRenderer;

        public float HideDelay
        {
            get
            {
                return this.hideDelay;
            }

            set
            {
                this.hideDelay = value;
            }
        }

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.meshRenderer = this.GetComponent<MeshRenderer>();
        }

        public void Show()                                                    
        {
            this.meshRenderer.enabled = true;
            this.lastShown = Time.time;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            var enabled = (Time.time - this.lastShown) < this.hideDelay;
            this.meshRenderer.enabled = enabled;
        }
    }
}