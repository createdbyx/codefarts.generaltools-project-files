﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.Scripts
{
    using UnityEngine;

    /// <summary>
    /// The selection bounds.
    /// </summary>
    public class SelectionBounds : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// The align to object flag used to determin if the selection bounds will be aligned to the object rotation.
        /// </summary>
        public bool alignToObject;

        /// <summary>
        /// Used to determine how long the corner lines are as a percentage of the render bounds size.
        /// </summary>
        public float lineLengthPercentage = 0.1f;

        /// <summary>
        /// The line material used to render the lines.
        /// </summary>
        public Material lineMaterial;

        /// <summary>
        /// The color that will be used to tint the material.
        /// </summary>
        public Color color = Color.white;

        /// <summary>
        /// Determine whether or not the bounds will be scaled (multiplied) or added.
        /// </summary>
        public bool scaler = true;

        /// <summary>
        /// Used to determine how much to scale or add to the size of the render bounds depending on the state of <see cref="scaler"/>.
        /// </summary>
        public float scalerValue = 1.02f;

        /// <summary>
        /// A reference to a transform that will act as the origin.
        /// </summary>
        /// <remarks>If no origin is specified the transform that this component is attached to will be used.</remarks>
        public Transform origin;

        /// <summary>
        /// The center of the custom bounds.
        /// </summary>
        public Vector3 center;

        /// <summary>
        /// THe size of the custom bounds
        /// </summary>
        public Vector3 size;

        /// <summary>
        /// If true will use custom bounds values.
        /// </summary>
        public bool useCustomBounds;

        /// <summary>
        /// The use mesh flag that will determine weather or not open GL will be used to draw the boundary or a call to <see cref="Graphics.DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,int,UnityEngine.Camera,int,UnityEngine.MaterialPropertyBlock,bool)"/>.
        /// </summary>
        public bool useMesh;

        /// <summary>
        /// The meshHolds a reference to a mesh used to draw bounds.
        /// </summary>
        private Mesh mesh;

        /// <summary>
        /// The layer that the <see cref="mesh"/> will be rendered on.
        /// </summary>
        public int MeshLayer;

        #endregion

        #region Methods

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            if (this.useMesh)
            {
                var transformReference = this.origin != null ? this.origin : this.transform;
                var bounds = this.GetBounds();
                bounds.center -= transformReference.position;
                this.DrawMesh(bounds, transformReference);
            }
        }

        /// <summary>
        /// Called by unity when after camera has rendered the scene.
        /// </summary>
        private void OnRenderObject()
        {
            if (!this.useMesh)
            {
                this.DrawOpenGL(this.GetBounds());
            }
        }

        private Bounds GetBounds()
        {
            var transformReference = this.origin != null ? this.origin : this.transform;

            // check if there is a mesh renderer atached and if not we just exit
            var bounds = new Bounds();
            if (this.useCustomBounds)
            {
                bounds = new Bounds(this.center + transformReference.position, this.size);
            }
            else
            {
                var meshRenderer = transformReference.GetComponent<MeshRenderer>();
                if (meshRenderer != null)
                {
                    bounds = meshRenderer.bounds;
                }
                else
                {
                    var meshFilter = transformReference.GetComponent<MeshFilter>();
                    if (meshFilter != null)
                    {
                        bounds = meshFilter.mesh.bounds;
                    }
                    else
                    {
                        var meshCollider = transformReference.GetComponent<Collider>();
                        if (meshCollider != null)
                        {
                            bounds = meshCollider.bounds;
                        }
                        else
                        {
                            return bounds;
                        }
                    }
                }
            }

            return bounds;
        }

        private void DrawMesh(Bounds bounds, Transform transformReference)
        {
            if (this.mesh == null)
            {
                this.mesh = new Mesh();
            }

            // pre-calculate values to prevent unnecessary redundant calculations and improve performance
            var sizeX = this.scaler ? bounds.size.x * this.scalerValue : bounds.size.x + this.scalerValue;
            var sizeY = this.scaler ? bounds.size.y * this.scalerValue : bounds.size.y + this.scalerValue;
            var sizeZ = this.scaler ? bounds.size.z * this.scalerValue : bounds.size.z + this.scalerValue;

            var maxX = bounds.max.x;
            var maxY = bounds.max.y;
            var maxZ = bounds.max.z;
            var minX = bounds.min.x;
            var minY = bounds.min.y;
            var minZ = bounds.min.z;
            var lengthX = sizeX * this.lineLengthPercentage;
            var lengthY = sizeY * this.lineLengthPercentage;
            var lengthZ = sizeZ * this.lineLengthPercentage;

            var lines = this.GetLines(minX, minY, minZ, maxX, maxY, maxZ, lengthX, lengthY, lengthZ);
            var indicies = new int[lines.Length];
            for (var i = 0; i < indicies.Length; i++)
            {
                indicies[i] = i;
            }

            this.mesh.vertices = lines;
            this.mesh.SetIndices(indicies, MeshTopology.Lines, 0);
            this.mesh.RecalculateBounds();

            var rotation = this.alignToObject ? transformReference.localRotation : Quaternion.identity;
            Graphics.DrawMesh(this.mesh, transformReference.position, rotation, this.lineMaterial, this.MeshLayer, UnityEngine.Camera.current, 0);
        }

        private void DrawOpenGL(Bounds bounds)
        {
            // set the material pass is a material has been specified
            var material = this.lineMaterial;
            if (material != null)
            {
                material.SetPass(0);
            }

            // pre-calculate values to prevent unnecessary redundant calculations and improve performance
            var sizeX = this.scaler ? bounds.size.x * this.scalerValue : bounds.size.x + this.scalerValue;
            var sizeY = this.scaler ? bounds.size.y * this.scalerValue : bounds.size.y + this.scalerValue;
            var sizeZ = this.scaler ? bounds.size.z * this.scalerValue : bounds.size.z + this.scalerValue;

            var maxX = bounds.max.x;
            var maxY = bounds.max.y;
            var maxZ = bounds.max.z;
            var minX = bounds.min.x;
            var minY = bounds.min.y;
            var minZ = bounds.min.z;
            var lengthX = sizeX * this.lineLengthPercentage;
            var lengthY = sizeY * this.lineLengthPercentage;
            var lengthZ = sizeZ * this.lineLengthPercentage;

            // set color and push a matrix on to the stack
            GL.Color(this.color);
            GL.PushMatrix();
            if (this.alignToObject)
            {
                // TODO: Not yet implemented for Open GL yet
                //var transformReference = this.origin ?? this.transform;
                //var matrix = Matrix4x4.TRS(transformReference.position, transformReference.rotation, transformReference.localScale);
                //GL.MultMatrix(matrix);
            }

            // start drawing lines
            GL.Begin(GL.LINES);
            var lines = this.GetLines(minX, minY, minZ, maxX, maxY, maxZ, lengthX, lengthY, lengthZ);
            for (var i = 0; i < lines.Length; i++)
            {
                var vector3 = lines[i];
                GL.Vertex(vector3);
            }

            // done drawing lines
            GL.End();

            // pop the previously pushed matrix
            GL.PopMatrix();
        }

        private Vector3[] GetLines(float minX, float minY, float minZ, float maxX, float maxY, float maxZ, float lengthX, float lengthY, float lengthZ)
        {
            var vertexes = new Vector3[]
            {
                new Vector3(maxX, maxY, maxZ),
                new Vector3(maxX - lengthX, maxY, maxZ),
           
                new Vector3(maxX, maxY, maxZ),
                new Vector3(maxX, maxY - lengthY, maxZ),
             
                new Vector3(maxX, maxY, maxZ),
                new Vector3(maxX, maxY, maxZ - lengthZ),
             
                new Vector3(minX, maxY, maxZ),
                new Vector3(minX + lengthX, maxY, maxZ),
            
                new Vector3(minX, maxY, maxZ),
                new Vector3(minX, maxY - lengthY, maxZ),
            
                new Vector3(minX, maxY, maxZ),
                new Vector3(minX, maxY, maxZ - lengthZ),
            
                new Vector3(maxX, minY, maxZ),
                new Vector3(maxX - lengthX, minY, maxZ),
            
                new Vector3(maxX, minY, maxZ),
                new Vector3(maxX, minY + lengthY, maxZ),
            
                new Vector3(maxX, minY, maxZ),
                new Vector3(maxX, minY, maxZ - lengthZ),
            
                new Vector3(minX, minY, maxZ),
                new Vector3(minX + lengthX, minY, maxZ),
            
                new Vector3(minX, minY, maxZ),
                new Vector3(minX, minY + lengthY, maxZ),
            
                new Vector3(minX, minY, maxZ),
                new Vector3(minX, minY, maxZ - lengthZ),
            
                new Vector3(minX, minY, minZ),
                new Vector3(minX + lengthX, minY, minZ),
             
                new Vector3(minX, minY, minZ),
                new Vector3(minX, minY + lengthY, minZ),
            
                new Vector3(minX, minY, minZ),
                new Vector3(minX, minY, minZ + lengthZ),
            
                new Vector3(minX, maxY, minZ),
                new Vector3(minX + lengthX, maxY, minZ),
            
                new Vector3(minX, maxY, minZ),
                new Vector3(minX, maxY - lengthY, minZ),
            
                new Vector3(minX, maxY, minZ),
                new Vector3(minX, maxY, minZ + lengthZ),
            
                new Vector3(maxX, maxY, minZ),
                new Vector3(maxX - lengthX, maxY, minZ),
            
                new Vector3(maxX, maxY, minZ),
                new Vector3(maxX, maxY - lengthY, minZ),
            
                new Vector3(maxX, maxY, minZ),
                new Vector3(maxX, maxY, minZ + lengthZ),
            
                new Vector3(maxX, minY, minZ),
                new Vector3(maxX - lengthX, minY, minZ),
            
                new Vector3(maxX, minY, minZ),
                new Vector3(maxX, minY + lengthY, minZ),
            
                new Vector3(maxX, minY, minZ),
                new Vector3(maxX, minY, minZ + lengthZ)
            };

            return vertexes;
        }
        #endregion
    }
}