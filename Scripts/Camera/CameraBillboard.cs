
namespace Codefarts.GeneralTools.Scripts.Camera
{
    using UnityEngine;

    public class CameraBillboard : MonoBehaviour
    {
        // the camera the the game object will be bill boarded to.
        public Camera cameraReference;

        public bool Invert;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
            // if no camera has been specified just use main camera
            if (this.cameraReference == null)
            {
                this.cameraReference = Camera.main;
            }
        }

        /// <summary>                         
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void Update()
        {
            var reference = this.cameraReference.transform;
            if (this.Invert)
            {
                // orient the game object to look away from the camera
                this.transform.LookAt(this.transform.position + (reference.rotation * Vector3.forward), reference.rotation * Vector3.up);
                // this.transform.rotation = Quaternion.LookRotation(-reference.forward, Vector3.up);
            }
            else
            {
                // orient the game object to look at the camera
                this.transform.LookAt(this.transform.position + (reference.rotation * Vector3.back), reference.rotation * Vector3.up);
            }
        }
    }
}