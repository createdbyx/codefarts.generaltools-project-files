﻿namespace Codefarts.GeneralTools.Scripts.Camera
{
    using UnityEngine;

    public class CameraMousePanning : MonoBehaviour
    {
        public Camera CameraObject
        {
            get
            {
                return this.cameraObject;
            }

            set
            {
                this.cameraObject = value;

                // cache the camera transform if a camera reference was specified
                this.cameraTransform = value != null ? value.transform : null;
            }
        }

        [Tooltip("The speed that panning will occur.")]
        public float PanningSpeed = 1;

        [Tooltip("Distance from edge of cameras screen rect inward that defines the area where panning will occur.")]
        public float EdgeDistance = 50;

        [SerializeField]
        [HideInInspector]
        private Camera cameraObject;

        [SerializeField]
        [HideInInspector]
        private Transform cameraTransform;

        [Tooltip("If true the camera will continue pan even if the mouse has moved beyond the cameras screen rect.")]
        public bool moveIfBeyondEdge;

        [Tooltip("If true panning direction will pan relative to the center of the caneras screen rect rather then what edge the mouse is closest to.")]
        public bool smoothPanning;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.enabled = this.cameraObject != null;
        }

        // Update is called once per frame
        private void Update()
        {
            if (this.cameraObject == null)
            {
                return;
            }

            var outsideRect = this.cameraObject.pixelRect;
            var innerRect = outsideRect;
            innerRect.min += Vector2.one * this.EdgeDistance;
            innerRect.max -= Vector2.one * this.EdgeDistance;

            var mousePosition = Input.mousePosition;

            // check if mouse is within edges
            if ((!this.moveIfBeyondEdge || innerRect.Contains(mousePosition)) && (!outsideRect.Contains(mousePosition) || innerRect.Contains(mousePosition)))
            {
                return;
            }

            var mouseVector = new Vector2(mousePosition.x, mousePosition.y) - outsideRect.center;
            var moveVector = Vector2.zero;

            if (this.smoothPanning)
            {
                moveVector = (mouseVector.normalized * this.PanningSpeed) * Time.deltaTime;
            }
            else
            {
                // horizontal
                if ((mousePosition.x < innerRect.xMin || mousePosition.x > innerRect.xMax) && (mousePosition.y >= innerRect.yMin && mousePosition.y <= innerRect.yMax))
                {
                    moveVector = (mouseVector.normalized * this.PanningSpeed) * Time.deltaTime;
                    moveVector.y = 0;
                }

                // vertical
                if ((mousePosition.y < innerRect.yMin || mousePosition.y > innerRect.yMax) && (mousePosition.x >= innerRect.xMin && mousePosition.x <= innerRect.xMax))
                {
                    moveVector = (mouseVector.normalized * this.PanningSpeed) * Time.deltaTime;
                    moveVector.x = 0;
                }

                // top left
                if (mousePosition.x < innerRect.xMin && mousePosition.y < innerRect.yMin)
                {
                    moveVector = (-Vector2.one * this.PanningSpeed) * Time.deltaTime;
                }

                // top right
                if (mousePosition.x > innerRect.xMax && mousePosition.y < innerRect.yMin)
                {
                    moveVector = (new Vector2(1, -1) * this.PanningSpeed) * Time.deltaTime;
                }

                // bottom left
                if (mousePosition.x < innerRect.xMin && mousePosition.y > innerRect.yMax)
                {
                    moveVector = (new Vector2(-1, 1) * this.PanningSpeed) * Time.deltaTime;
                }

                // bottom right
                if (mousePosition.x > innerRect.xMax && mousePosition.y > innerRect.yMax)
                {
                    moveVector = (Vector2.one * this.PanningSpeed) * Time.deltaTime;
                }
            }

            this.cameraTransform.position += (this.cameraTransform.right * moveVector.x) + (this.cameraTransform.up * moveVector.y);
        }
    }
}