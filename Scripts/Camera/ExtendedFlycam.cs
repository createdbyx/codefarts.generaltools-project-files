/*
<copyright>
 Copyright (c) 2012 Codefarts
 All rights reserved.
 contact@codefarts.com
 http://www.codefarts.com
 * MODIFIED VERSION OF - > Desi Quintans (CowfaceGames.com), 17 August 2012. changes
</copyright> 
   
/*
    EXTENDED FLYCAM
        Desi Quintans (CowfaceGames.com), 17 August 2012.
        Based on FlyThrough.js by Slin (http://wiki.unity3d.com/index.php/FlyThrough), 17 May 2011.
 
    LICENSE
        Free as in speech, and free as in beer.
 
    FEATURES
        WASD/Arrows:    Movement
                  Q:    Climb
                  E:    Drop
                      Shift:    Move faster
                    Control:    Move slower
                        End:    Toggle cursor locking to screen (you can also press Ctrl+P to toggle play mode on and off).
    */
namespace Codefarts.GeneralTools.Scripts.Camera
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    using UnityEngine;

    public class ExtendedFlycam : MonoBehaviour, INotifyPropertyChanged
    {
        /// <summary>
        /// The cached property arguments.
        /// </summary>
        private IDictionary<string, PropertyChangedEventArgs> propertyArgs = new Dictionary<string, PropertyChangedEventArgs>();

        [SerializeField]
        private float cameraSensitivity = 90;
        [SerializeField]
        private float climbSpeed = 4;
        [SerializeField]
        private float normalMoveSpeed = 10;
        [SerializeField]
        private float slowMoveMultiplier = 0.25f;
        [SerializeField]
        private float fastMoveMultiplier = 3;

        [SerializeField]
        private float pitch;

        [SerializeField]
        private float yaw;

        [SerializeField]
        private float roll;

        [SerializeField]
        private float minYaw;
        [SerializeField]
        private float maxYaw = 360;
        [SerializeField]
        private float minPitch = -90;
        [SerializeField]
        private float maxPitch = 90;
        [SerializeField]
        private float minRoll;
        [SerializeField]
        private float maxRoll;

        [SerializeField]
        private bool restrictYaw;
        [SerializeField]
        private bool restrictPitch = true;
        [SerializeField]
        private bool restrictRoll = true;
        [SerializeField]
        private bool wrapYaw = true;
        [SerializeField]
        private bool wrapPitch;
        [SerializeField]
        private bool wrapRoll;

        [SerializeField]
        private bool canMove = true;
        [SerializeField]
        private bool canRotate = true;
        [SerializeField]
        private bool canRoll;

        [SerializeField]
        private int mouseButton = 1;

        /// <summary>
        /// Gets or sets a value indicating whether the camera can be rotated by holding a mouse button and dragging.
        /// </summary>
        /// <seealso cref="MouseButton"/>
        public virtual bool UseMouseButtonToRotate
        {
            get
            {
                return this.useMouseButtonToRotate;
            }

            set
            {
                if (value == this.useMouseButtonToRotate)
                {
                    return;
                }

                this.useMouseButtonToRotate = value;
                this.OnPropertyChanged("UseMouseButtonToRotate");
            }
        }

        /// <summary>
        /// Gets or sets the mouse button that is used to rotate the camera.
        /// </summary>                                                      
        /// <seealso cref="UseMouseButtonToRotate"/>
        public virtual int MouseButton
        {
            get
            {
                return this.mouseButton;
            }

            set
            {
                if (value == this.mouseButton)
                {
                    return;
                }

                this.mouseButton = value;
                this.OnPropertyChanged("MouseButton");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the camera is allowed to roll.
        /// </summary>                                                   
        public virtual bool CanRoll
        {
            get
            {
                return this.canRoll;
            }

            set
            {
                if (value == this.canRoll)
                {
                    return;
                }

                this.canRoll = value;
                this.OnPropertyChanged("CanRoll");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the camera is allowed to rotate.
        /// </summary>
        public virtual bool CanRotate
        {
            get
            {
                return this.canRotate;
            }

            set
            {
                if (value == this.canRotate)
                {
                    return;
                }

                this.canRotate = value;
                this.OnPropertyChanged("CanRotate");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the camera can move.
        /// </summary>
        public virtual bool CanMove
        {
            get
            {
                return this.canMove;
            }

            set
            {
                if (value == this.canMove)
                {
                    return;
                }

                this.canMove = value;
                this.OnPropertyChanged("CanMove");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the camera roll will wrap. See remarks.
        /// </summary>                               
        /// <remarks>
        /// <p>This property is designed to work along side the <see cref="RestrictRoll"/>, <see cref="MinRoll"/> & <see cref="MaxRoll"/> properties.</p>
        /// <p>When true it will allow the camera roll to wrap from min to max or max to min roll boundaries depending on the direction of the roll.</p>
        /// </remarks>
        public virtual bool WrapRoll
        {
            get
            {
                return this.wrapRoll;
            }

            set
            {
                if (value == this.wrapRoll)
                {
                    return;
                }

                this.wrapRoll = value;
                this.OnPropertyChanged("WrapRoll");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the camera pitch will wrap. See remarks.
        /// </summary>                               
        /// <remarks>
        /// <p>This property is designed to work along side the <see cref="RestrictPitch"/>, <see cref="MinPitch"/> & <see cref="MaxPitch"/> properties.</p>
        /// <p>When true it will allow the camera pitch to wrap from min to max or max to min pitch boundaries depending on the direction of the pitch.</p>
        /// </remarks>
        public virtual bool WrapPitch
        {
            get
            {
                return this.wrapPitch;
            }

            set
            {
                if (value == this.wrapPitch)
                {
                    return;
                }

                this.wrapPitch = value;
                this.OnPropertyChanged("WrapPitch");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the camera yaw will wrap. See remarks.
        /// </summary>                               
        /// <remarks>
        /// <p>This property is designed to work along side the <see cref="RestrictYaw"/>, <see cref="MinYaw"/> & <see cref="MaxYaw"/> properties.</p>
        /// <p>When true it will allow the camera yaw to wrap from min to max or max to min pitch boundaries depending on the direction of the yaw.</p>
        /// </remarks>
        public virtual bool WrapYaw
        {
            get
            {
                return this.wrapYaw;
            }

            set
            {
                if (value == this.wrapYaw)
                {
                    return;
                }

                this.wrapYaw = value;
                this.OnPropertyChanged("WrapYaw");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether camera <see cref="Roll"/> should be restricted to <see cref="MinRoll"/> & <see cref="MaxRoll"/>.
        /// </summary>                                                                                                                           
        public virtual bool RestrictRoll
        {
            get
            {
                return this.restrictRoll;
            }

            set
            {
                if (value == this.restrictRoll)
                {
                    return;
                }

                this.restrictRoll = value;
                this.OnPropertyChanged("RestrictRoll");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether camera <see cref="Pitch"/> should be restricted to <see cref="MinPitch"/> & <see cref="MaxPitch"/>.
        /// </summary>                                                                                                                           
        public virtual bool RestrictPitch
        {
            get
            {
                return this.restrictPitch;
            }

            set
            {
                if (value == this.restrictPitch)
                {
                    return;
                }

                this.restrictPitch = value;
                this.OnPropertyChanged("RestrictPitch");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether camera <see cref="Yaw"/> should be restricted to <see cref="MinYaw"/> & <see cref="MaxYaw"/>.
        /// </summary>                                                                                                                           
        public virtual bool RestrictYaw
        {
            get
            {
                return this.restrictYaw;
            }

            set
            {
                if (value == this.restrictYaw)
                {
                    return;
                }

                this.restrictYaw = value;
                this.OnPropertyChanged("RestrictYaw");
            }
        }

        /// <summary>
        /// Gets or sets the maximum roll value for the camera if <see cref="RestrictRoll"/> is true.
        /// </summary>                                         
        public virtual float MaxRoll
        {
            get
            {
                return this.maxRoll;
            }

            set
            {
                if (Math.Abs(value - this.maxRoll) < Mathf.Epsilon)
                {
                    return;
                }

                this.maxRoll = value;
                this.OnPropertyChanged("MaxRoll");
            }
        }

        /// <summary>
        /// Gets or sets the minimum roll value for the camera if <see cref="RestrictRoll"/> is true.
        /// </summary>                                         
        public virtual float MinRoll
        {
            get
            {
                return this.minRoll;
            }

            set
            {
                if (Math.Abs(value - this.minRoll) < Mathf.Epsilon)
                {
                    return;
                }

                this.minRoll = value;
                this.OnPropertyChanged("MinRoll");
            }
        }

        /// <summary>
        /// Gets or sets the maximum pitch value for the camera if <see cref="RestrictPitch"/> is true.
        /// </summary>                                         
        public virtual float MaxPitch
        {
            get
            {
                return this.maxPitch;
            }

            set
            {
                if (Math.Abs(value - this.maxPitch) < Mathf.Epsilon)
                {
                    return;
                }

                this.maxPitch = value;
                this.OnPropertyChanged("MaxPitch");
            }
        }

        /// <summary>
        /// Gets or sets the minimum pitch value for the camera if <see cref="RestrictPitch"/> is true.
        /// </summary>                                         
        public virtual float MinPitch
        {
            get
            {
                return this.minPitch;
            }

            set
            {
                if (Math.Abs(value - this.minPitch) < Mathf.Epsilon)
                {
                    return;
                }

                this.minPitch = value;
                this.OnPropertyChanged("MinPitch");
            }
        }

        /// <summary>
        /// Gets or sets the maximum yaw value for the camera if <see cref="RestrictYaw"/> is true.
        /// </summary>                                         
        public virtual float MaxYaw
        {
            get
            {
                return this.maxYaw;
            }

            set
            {
                if (Math.Abs(value - this.maxYaw) < Mathf.Epsilon)
                {
                    return;
                }

                this.maxYaw = value;
                this.OnPropertyChanged("MaxYaw");
            }
        }

        /// <summary>
        /// Gets or sets the minimum yaw value for the camera if <see cref="RestrictYaw"/> is true.
        /// </summary>                                         
        public virtual float MinYaw
        {
            get
            {
                return this.minYaw;
            }

            set
            {
                if (Math.Abs(value - this.minYaw) < Mathf.Epsilon)
                {
                    return;
                }

                this.minYaw = value;
                this.OnPropertyChanged("MinYaw");
            }
        }

        /// <summary>
        /// Gets or sets the fast move multiplier that controls how much faster the camera will move when the user wants to move faster.
        /// </summary>                                                                                     
        public virtual float FastMoveMultiplier
        {
            get
            {
                return this.fastMoveMultiplier;
            }

            set
            {
                if (Math.Abs(value - this.fastMoveMultiplier) < Mathf.Epsilon)
                {
                    return;
                }

                this.fastMoveMultiplier = value;
                this.OnPropertyChanged("FastMoveMultiplier");
            }
        }

        /// <summary>
        /// Gets or sets the slow move multiplier that controls how much slower the camera will move when the user wants to move slower.
        /// </summary>                                                                                     
        public virtual float SlowMoveMultiplier
        {
            get
            {
                return this.slowMoveMultiplier;
            }

            set
            {
                if (Math.Abs(value - this.slowMoveMultiplier) < Mathf.Epsilon)
                {
                    return;
                }

                this.slowMoveMultiplier = value;
                this.OnPropertyChanged("SlowMoveMultiplier");
            }
        }

        /// <summary>
        /// Gets or sets the camera sensitivity when rotating.
        /// </summary> 
        public virtual float CameraSensitivity
        {
            get
            {
                return this.cameraSensitivity;
            }

            set
            {
                if (Math.Abs(value - this.cameraSensitivity) < Mathf.Epsilon)
                {
                    return;
                }

                this.cameraSensitivity = value;
                this.OnPropertyChanged("CameraSensitivity");
            }
        }

        /// <summary>
        /// Gets or sets the climb speed when the camera is increasing in elevation.
        /// </summary>
        public virtual float ClimbSpeed
        {
            get
            {
                return this.climbSpeed;
            }

            set
            {
                if (Math.Abs(value - this.climbSpeed) < Mathf.Epsilon)
                {
                    return;
                }

                this.climbSpeed = value;
                this.OnPropertyChanged("ClimbSpeed");
            }
        }

        /// <summary>
        /// Gets or sets the normal move speed when the camera is moving.
        /// </summary>                                                   
        public virtual float NormalMoveSpeed
        {
            get
            {
                return this.normalMoveSpeed;
            }

            set
            {
                if (Math.Abs(value - this.normalMoveSpeed) < Mathf.Epsilon)
                {
                    return;
                }

                this.normalMoveSpeed = value;
                this.OnPropertyChanged("NormalMoveSpeed");
            }
        }

        [SerializeField]
        private bool useMouseButtonToRotate = true;

        [SerializeField]
        private string pitchAxisName = "Mouse Y";
        [SerializeField]
        private string yawAxisName = "Mouse X";
        [SerializeField]
        private string rollAxisName = "CameraRoll";
        [SerializeField]
        private string forwardAndBackwardAxisName = "Vertical";
        [SerializeField]
        private string leftAndRightAxisName = "Horizontal";

        /// <summary>
        /// Gets or sets the input name of the pitch axis.
        /// </summary>
        public virtual string PitchAxisName
        {
            get
            {
                return this.pitchAxisName;
            }

            set
            {
                if (this.pitchAxisName == value)
                {
                    return;
                }

                this.pitchAxisName = value;
                this.OnPropertyChanged("PitchAxisName");
            }
        }

        /// <summary>
        /// Gets or sets the input name of the yaw axis.
        /// </summary>
        public virtual string YawAxisName
        {
            get
            {
                return this.yawAxisName;
            }

            set
            {
                if (this.yawAxisName == value)
                {
                    return;
                }

                this.yawAxisName = value;
                this.OnPropertyChanged("YawAxisName");
            }
        }

        /// <summary>
        /// Gets or sets the input name of the roll axis.
        /// </summary>
        public virtual string RollAxisName
        {
            get
            {
                return this.rollAxisName;
            }

            set
            {
                if (this.rollAxisName == value)
                {
                    return;
                }

                this.rollAxisName = value;
                this.OnPropertyChanged("RollAxisName");
            }
        }

        /// <summary>
        /// Gets or sets the input name of the forward and backward axis.
        /// </summary>
        public virtual string ForwardAndBackwardAxisName
        {
            get
            {
                return this.forwardAndBackwardAxisName;
            }

            set
            {
                if (this.forwardAndBackwardAxisName == value)
                {
                    return;
                }

                this.forwardAndBackwardAxisName = value;
                this.OnPropertyChanged("ForwardAndBackwardAxisName");
            }
        }

        /// <summary>
        /// Gets or sets the input name of the left and right axis.
        /// </summary>
        public virtual string LeftAndRightAxisName
        {
            get
            {
                return this.leftAndRightAxisName;
            }

            set
            {
                if (this.leftAndRightAxisName == value)
                {
                    return;
                }

                this.leftAndRightAxisName = value;
                this.OnPropertyChanged("LeftAndRightAxisName");
            }
        }

        [SerializeField]
        private bool lockPitch;

        [SerializeField]
        private bool lockYaw;

        [SerializeField]
        private bool lockRoll;

        /// <summary>
        /// The cached transform reference.
        /// </summary>
        private Transform transformReference;

        /// <summary>
        /// Gets or sets a value indicating whether camera roll is locked and cannot be changed.
        /// </summary>                                                                          
        public virtual bool LockRoll
        {
            get
            {
                return this.lockRoll;
            }

            set
            {
                if (this.lockRoll == value)
                {
                    return;
                }

                this.lockRoll = value;
                this.OnPropertyChanged("LockRoll");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether camera yaw is locked and cannot be changed.
        /// </summary>                                                                          
        public virtual bool LockYaw
        {
            get
            {
                return this.lockYaw;
            }

            set
            {
                if (this.lockYaw == value)
                {
                    return;
                }

                this.lockYaw = value;
                this.OnPropertyChanged("LockYaw");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether camera pitch is locked and cannot be changed.
        /// </summary>                                                                          
        public virtual bool LockPitch
        {
            get
            {
                return this.lockPitch;
            }

            set
            {
                if (this.lockPitch == value)
                {
                    return;
                }

                this.lockPitch = value;
                this.OnPropertyChanged("LockPitch");
            }
        }

        /// <summary>
        /// Gets or sets the camera pitch.
        /// </summary>
        public virtual float Pitch
        {
            get
            {
                return this.pitch;
            }

            set
            {
                if (Math.Abs(this.pitch - value) < Mathf.Epsilon)
                {
                    return;
                }

                this.pitch = value;
                this.OnPropertyChanged("Pitch");
            }
        }

        /// <summary>
        /// Gets or sets the camera yaw.
        /// </summary>
        public virtual float Yaw
        {
            get
            {
                return this.yaw;
            }

            set
            {
                if (Math.Abs(this.yaw - value) < Mathf.Epsilon)
                {
                    return;
                }

                this.yaw = value;
                this.OnPropertyChanged("Yaw");
            }
        }

        /// <summary>
        /// Gets or sets the camera roll.
        /// </summary>
        public virtual float Roll
        {
            get
            {
                return this.roll;
            }

            set
            {
                if (Math.Abs(this.roll - value) < Mathf.Epsilon)
                {
                    return;
                }

                this.roll = value;
                this.OnPropertyChanged("Roll");
            }
        }

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
            this.pitch = this.transform.localEulerAngles.x;
            this.yaw = this.transform.localEulerAngles.y;
            this.transformReference = this.transform;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void Update()
        {
            // if the can rotate flag is true and use mouse button flag and the associated mouse button is held down
            if (this.canRotate && (this.useMouseButtonToRotate && Input.GetMouseButton(this.mouseButton)))
            {
                // get pitch and yaw directions
                var pitchDirection = this.lockPitch ? 0 : -Input.GetAxis(this.pitchAxisName) * this.cameraSensitivity * Time.deltaTime;
                var yawDirection = this.lockYaw ? 0 : Input.GetAxis(this.yawAxisName) * this.cameraSensitivity * Time.deltaTime;

                // update pitch and yaw values
                this.pitch = this.GetAngle(this.pitch, this.restrictPitch, this.wrapPitch, this.minPitch, this.maxPitch, pitchDirection);
                this.yaw = this.GetAngle(this.yaw, this.restrictYaw, this.wrapYaw, this.minYaw, this.maxYaw, yawDirection);

                // check if we can roll
                if (this.canRoll)
                {
                    // get roll direction
                    var rollDirection = this.lockRoll ? 0 : Input.GetAxis(this.rollAxisName) * this.cameraSensitivity * Time.deltaTime;

                    // update roll value
                    this.roll = this.GetAngle(this.roll, this.restrictRoll, this.wrapRoll, this.minRoll, this.maxRoll, rollDirection);
                }
            }

            // Debug.Log(string.Format("x: {0} y: {1} dirx: {2} diry: {3}", this.pitch, this.yaw, pitchDirection, yawDirection));
            this.transformReference.localRotation = Quaternion.Euler(this.pitch, this.yaw, this.roll);
            
            // if the can move flag is false we can exit here
            if (!this.canMove)
            {
                return;
            }

            // set a default multiplier of 1
            var multiplier = 1f;

            // check if shift key is pressed and if so update multiplier to the fast move multiplier
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                multiplier = this.fastMoveMultiplier;
            }

            // check if control key is pressed and if so update multiplier to the slow move multiplier
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            {
                multiplier = this.slowMoveMultiplier;
            }

            //
            this.transformReference.position += this.transformReference.forward * (this.normalMoveSpeed * multiplier) * Input.GetAxis(this.forwardAndBackwardAxisName) * Time.deltaTime;
            this.transformReference.position += this.transformReference.right * (this.normalMoveSpeed * multiplier) * Input.GetAxis(this.leftAndRightAxisName) * Time.deltaTime;

            if (Input.GetKey(KeyCode.Space))
            {
                this.transformReference.position += this.transformReference.up * this.climbSpeed * Time.deltaTime;
            }
        }

        private float GetAngle(float value, bool restrict, bool wrap, float min, float max, float direction)
        {
            if (restrict)
            {
                if (wrap)
                {
                    if (Math.Abs(Math.Abs(max - min)) > Mathf.Epsilon)
                    {
                        value = value + direction < min && direction < 0
                                             ? max + direction
                                             : value + direction;
                        value = min + ((value - min) % (max - min));
                    }
                    else
                    {
                        value = min;
                    }
                }
                else
                {
                    value = Mathf.Clamp(value + direction, min, max);
                }
            }
            else
            {
                value += direction;
            }

            return value;
        }

        /// <summary>
        /// Occurs when a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                if (!this.propertyArgs.ContainsKey(propertyName))
                {
                    this.propertyArgs[propertyName] = new PropertyChangedEventArgs(propertyName);
                }

                handler(this, this.propertyArgs[propertyName]);
            }
        }
    }
}