// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Vector3EventArgs.cs" company="Codefarts">
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>
// <summary>
//   Provides event arguments for the <see cref="Vector3" /> type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Codefarts.GeneralTools.Scripts
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Provides event arguments for the <see cref="Vector3"/> type.
    /// </summary>
    public class Vector3EventArgs : EventArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3EventArgs"/> class.
        /// </summary>
        /// <param name="previousValue">
        /// The previous value.
        /// </param>
        /// <param name="value">
        /// The current value.
        /// </param>
        public Vector3EventArgs(Vector3 previousValue, Vector3 value)
        {
            this.Value = value;
            this.PreviousValue = previousValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3EventArgs"/> class.
        /// </summary>
        public Vector3EventArgs()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a previous <see cref="Vector3"/> value.
        /// </summary>
        public Vector3 PreviousValue { get; set; }

        /// <summary>
        /// Gets or sets a <see cref="Vector3"/> value.
        /// </summary>
        public Vector3 Value { get; set; }

        #endregion
    }
}