﻿namespace Codefarts.GeneralTools.Scripts
{
    using Codefarts.GeneralTools.Code;

    using UnityEngine;

    public class SelectionManagerScript : MonoBehaviour
    {
        public SelectionManager Manager;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            if (this.Manager == null)
            {
                this.Manager = new SelectionManager();
            }
        }
    }
}
