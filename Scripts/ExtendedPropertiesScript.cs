﻿namespace Codefarts.GeneralTools.Scripts
{
    using Codefarts.GeneralTools.Code;

    using UnityEngine;

    /// <summary>
    /// Provides a script for storing generic property values.
    /// </summary>
    public class ExtendedPropertiesScript : MonoBehaviour
    {
        /// <summary>
        /// The propertiesHolds the properties collection
        /// </summary>
        public GenericPropertyCollection<object> properties;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        public void Awake()
        {
            this.properties = new GenericPropertyCollection<object>();
        }
    }
}
