﻿namespace Codefarts.GeneralTools.Scripts.General
{
    using UnityEngine;

    [AddComponentMenu("Codefarts/General")]
    public class Floatate : MonoBehaviour
    {
        float bobSpeed = 3.0f;  //Bob speed
        float bobHeight = 0.5f; //Bob height
        float bobOffset = 0.0f;

        float PrimaryRot = 80.0f;  //First axies degrees per second
        float SecondaryRot = 40.0f; //Second axies degrees per second
        float TertiaryRot = 20.0f;  //Third axies degrees per second

        private float bottom;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        public void Awake()
        {
            if (this.bobSpeed < 0)
            {
                Debug.LogWarning("Negative object bobSpeed value! May result in undesired behavior. Continuing anyway.", this.gameObject);
            }

            if (this.bobHeight < 0)
            {
                Debug.LogWarning("Negative object bobHeight value! May result in undesired behavior. Continuing anyway.", this.gameObject);
            }

            this.bottom = this.transform.position.y;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            this.transform.Rotate(new Vector3(0, this.PrimaryRot, 0) * Time.deltaTime, Space.World);
            this.transform.Rotate(new Vector3(this.SecondaryRot, 0, 0) * Time.deltaTime, Space.Self);
            this.transform.Rotate(new Vector3(0, 0, this.TertiaryRot) * Time.deltaTime, Space.Self);

            var position = this.transform.position;
            position.y = this.bottom + (((Mathf.Cos((Time.time + this.bobOffset) * this.bobSpeed) + 1) / 2) * this.bobHeight);
            this.transform.position = position;
        }
    }
}
