﻿namespace Codefarts.GeneralTools.Scripts
{
    using System;

    using UnityEngine;

    [RequireComponent(typeof(MeshRenderer))]
    public class MouseHighlighter : MonoBehaviour
    {
        /// <summary>
        /// The material to be used as a highlighter.
        /// </summary>
        [Tooltip("The material to be used as a highlighter.")]
        public Material material;

        /// <summary>
        /// The cached mesh renderer reference for better performance.
        /// </summary>
        private Renderer meshRenderer;

        /// <summary>
        /// The material index where the material was stored.
        /// </summary>
        private int materialIndex = -1;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.meshRenderer = this.GetComponent<Renderer>();
        }

        /// <summary>
        /// OnMouseEnter is called when the mouse entered the GUIElement or Collider.
        /// </summary>
        public void OnMouseEnter()
        {
            var mat = this.material;
            if (mat == null)
            {
                return;
            }

            var materials = this.meshRenderer.sharedMaterials;
            Array.Resize(ref materials, materials.Length + 1);
            materials[materials.Length - 1] = mat;
            this.meshRenderer.sharedMaterials = materials;
            this.materialIndex = materials.Length - 1;
        }

        /// <summary>
        /// OnMouseExit is called when the mouse is not any longer over the GUIElement or Collider.
        /// </summary>
        public void OnMouseExit()
        {
            if (this.materialIndex != -1)
            {
                var materials = this.meshRenderer.sharedMaterials;
                if (this.materialIndex != materials.Length - 1)
                {
                    Array.Copy(materials, this.materialIndex + 1, materials, this.materialIndex, materials.Length - (this.materialIndex + 1));
                }

                Array.Resize(ref materials, materials.Length - 1);
                this.meshRenderer.sharedMaterials = materials;
                this.materialIndex = -1;
            }
        }
    }
}
