﻿namespace Codefarts.GeneralTools.Scripts
{
    using System;

    using Codefarts.Core;

    using UnityEngine;

    using Random = UnityEngine.Random;

    /// <summary>
    /// Provides the ability to spawn new random objects.
    /// </summary>
    [RequireComponent(typeof(BoxCollider))]
    public class RandomObjectSpawner : MonoBehaviour
    {
        /// <summary>
        /// Holds a reference to a singletion reference.
        /// </summary>
        private static RandomObjectSpawner singleton;

        /// <summary>
        /// The prefabs array from which prefabs are selected to spawn.
        /// </summary>
        public Transform[] Prefabs = new Transform[0];

        /// <summary>
        /// The object that will be used as the parent or container object for newly created prefabs.
        /// </summary>
        public Transform ParentObject;

        /// <summary>
        /// The spawn interval when objects will be created.
        /// </summary>
        public float SpawnInterval = 1;

        /// <summary>
        /// The last spawn time.
        /// </summary>
        private float lastSpawnTime;

        /// <summary>
        /// The holds a reference to the box collider for better performance.
        /// </summary>
        private BoxCollider boxCollider;

        /// <summary>
        /// Gets a singleton instance.
        /// </summary>   
        public static RandomObjectSpawner Instance
        {
            get
            {
                if (singleton == null)
                {
                    var exitsingObject = GameObject.FindObjectOfType<RandomObjectSpawner>();
                    GameObject gameObject;
                    if (exitsingObject != null)
                    {
                        //  gameObject = exitsingObject.gameObject;
                        singleton = exitsingObject;
                        gameObject = exitsingObject.gameObject;
                    }
                    else
                    {
                        gameObject = new GameObject("Random Object Spawner");
                        singleton = gameObject.AddComponent<RandomObjectSpawner>();
                    }

#if CODEFARTSCORE
                    var container = CodefartsContainerObject.Instance;
                    gameObject.transform.parent = container.GameObject.transform;
#endif
                }

                return singleton;
            }
        }

        /// <summary>
        /// Clears all prefabs from the <see cref="Prefabs"/> field.
        /// </summary>
        public void Clear()
        {
            Array.Resize(ref this.Prefabs, 0);
        }

        /// <summary>
        /// Gets the prefab from the <see cref="Prefabs"/> field using a predicate.
        /// </summary>
        /// <param name="p">The predicate reference.</param>
        /// <returns>The reference to the stored transform.</returns>
        public Transform GetPrefab(Predicate<Transform> p)
        {
            var value = Array.Find(this.Prefabs, p);
            return value;
        }

        /// <summary>
        /// Gets all prefabs from the <see cref="Prefabs"/> field using a predicate.
        /// </summary>
        /// <param name="p">The predicate reference.</param>
        /// <returns>The reference to the stored transforms.</returns>
        public Transform[] GetAllPrefabs(Predicate<Transform> p)
        {
            var value = Array.FindAll(this.Prefabs, p);
            if (value.Length == 0)
            {
                return null;
            }

            var returnValue = new Transform[value.Length];
            Array.Copy(value, returnValue, value.Length);
            return returnValue;
        }

        /// <summary>
        /// Adds a prefab to the <see cref="Prefabs"/> field.
        /// </summary>
        /// <param name="value">The transform reference to be added.</param>
        /// <exception cref="ArgumentNullException">The value of 'value' cannot be null. </exception>
        public void AddPrefab(Transform value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            Array.Resize(ref this.Prefabs, this.Prefabs.Length + 1);
            this.Prefabs[this.Prefabs.Length - 1] = value;
        }

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.boxCollider = this.GetComponent<BoxCollider>();
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            if (this.Prefabs == null || this.Prefabs.Length == 0)
            {
                return;
            }

            if (Time.time > this.lastSpawnTime + this.SpawnInterval)
            {
                // pick random item
                var index = Random.Range(0, this.Prefabs.Length);
                var spawnerModel = this.Prefabs[index];
                var halfSize = this.boxCollider.size / 2;
                var position = new Vector3(
                    Random.Range(-halfSize.x, halfSize.x),
                    Random.Range(-halfSize.y, halfSize.y),
                    Random.Range(-halfSize.z, halfSize.z));
                var newObject = Instantiate(spawnerModel) as Transform;
                newObject.parent = this.ParentObject != null ? this.ParentObject : this.transform;
                newObject.localPosition = position;
                this.lastSpawnTime = Time.time;
            }
        }
    }
}