// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.GeneralTools.Scripts
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Provides a behaviour that raises events when certan behaviour methods are called.
    /// </summary>
    public class GameEvents : MonoBehaviour
    {
        public event EventHandler StartEvent;
        public event EventHandler UpdateEvent;
        public event EventHandler OnGuiEvent;
        public event EventHandler OnEnableEvent;
        public event EventHandler OnDestroyEvent;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            var startEvent = this.StartEvent;
            if (startEvent != null)
            {
                startEvent(this, EventArgs.Empty);
            }  
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            var onGameUpdate = this.UpdateEvent;
            if (onGameUpdate != null)
            {
                onGameUpdate(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// This function is called when the MonoBehaviour will be destroyed.
        /// </summary>
        public void OnDestroy()
        {
            var gameDestroy = this.OnDestroyEvent;
            if (gameDestroy != null)
            {
                gameDestroy(this, EventArgs.Empty);
            }    
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            var gameEnable = this.OnEnableEvent;
            if (gameEnable != null)
            {
                gameEnable(this, EventArgs.Empty);
            }   
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            var gameGui = this.OnGuiEvent;
            if (gameGui != null)
            {       
                gameGui(this, EventArgs.Empty);
            }   
        }
    }
}