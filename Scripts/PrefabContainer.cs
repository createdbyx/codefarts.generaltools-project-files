﻿namespace Codefarts.GeneralTools.Scripts
{
    using System;
#if CODEFARTSCORE
    using Codefarts.Core;    
#endif

    using UnityEngine;

    using Object = UnityEngine.Object;

    public class PrefabContainer : MonoBehaviour
    {
        private static PrefabContainer singleton;

        //  private static GameObject gameObject;

        private static object lockObject = new object();

        [HideInInspector]
        public string[] Names = new string[0];

        [HideInInspector]
        public Object[] Prefabs = new Object[0];

        public static PrefabContainer Instance
        {
            get
            {
                lock (lockObject)
                {
                    if (singleton == null)
                    {
                        var exitsingObject = GameObject.FindObjectOfType<PrefabContainer>();
                        GameObject gameObject;
                        if (exitsingObject != null)
                        {
                            //  gameObject = exitsingObject.gameObject;
                            singleton = exitsingObject;
                            gameObject = exitsingObject.gameObject;
                        }
                        else
                        {
                            gameObject = new GameObject("Prefab Container");
                            singleton = gameObject.AddComponent<PrefabContainer>();
                        }

#if CODEFARTSCORE
                        var container = CodefartsContainerObject.Instance;
                        gameObject.transform.parent = container.GameObject.transform;
#endif
                        }
                }

                return singleton;
            }
        }

        public void Clear()
        {
            Array.Resize(ref this.Names, 0);
            Array.Resize(ref this.Prefabs, 0);
        }

        public Object GetPrefab(string name)
        {
            var index = Array.IndexOf(this.Names, name);
            if (index != -1)
            {
                return this.Prefabs[index];
            }

            return null;
        }

        public bool PrefabExists(string name)
        {
            var index = Array.IndexOf(this.Names, name);
            return index != -1;
        }

        public Object GetPrefab(Predicate<string> p)
        {
            var value = Array.Find(this.Names, p);
            return this.GetPrefab(value);
        }

        public Object GetPrefab(Predicate<Object> p)
        {
            var value = Array.Find(this.Prefabs, p);
            return value;
        }

        public Object[] GetAllPrefabs(Predicate<Object> p)
        {
            var value = Array.FindAll(this.Prefabs, p);
            if (value.Length == 0)
            {
                return null;
            }

            var returnValue = new Object[value.Length];
            Array.Copy(value, returnValue, value.Length);
            return returnValue;
        }

        public void AddPrefab(string name, Object value)
        {
            Array.Resize(ref this.Names, this.Names.Length + 1);
            Array.Resize(ref this.Prefabs, this.Prefabs.Length + 1);
            this.Names[this.Names.Length - 1] = name;
            this.Prefabs[this.Prefabs.Length - 1] = value;
        }
    }
}
