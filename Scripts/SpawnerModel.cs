﻿namespace Codefarts.GeneralTools.Scripts
{
    using UnityEngine;

    public class SpawnerModel : ScriptableObject
    {
        public Transform Object;

        public float Percentage;
    }
}